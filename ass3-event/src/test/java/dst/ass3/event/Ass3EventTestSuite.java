package dst.ass3.event;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import dst.ass3.event.test.EventProcessingTest;
import dst.ass3.event.test.EventSourceFunctionTest;

/**
 * Ass3EventTestSuite.
 */
@RunWith(Suite.class)
@SuiteClasses({
        EventSourceFunctionTest.class,
        EventProcessingTest.class
})
public class Ass3EventTestSuite {
}
