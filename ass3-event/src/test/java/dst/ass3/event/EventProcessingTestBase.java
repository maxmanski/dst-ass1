package dst.ass3.event;

import static org.junit.Assert.assertNotNull;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dst.ass3.event.model.Alert;
import dst.ass3.event.model.AverageStreamDuration;
import dst.ass3.event.model.LifecycleEvent;
import dst.ass3.event.model.StreamDuration;
import dst.ass3.event.model.StreamFailedWarning;
import dst.ass3.event.model.StreamTimeoutWarning;

/**
 * EventProcessingTestBase.
 */
public class EventProcessingTestBase extends Ass3EventTestBase {

    private static final Logger LOG = LoggerFactory.getLogger(EventProcessingTestBase.class);

    @Rule
    public Timeout timeout = new Timeout(15, TimeUnit.SECONDS);

    private IEventProcessingEnvironment epe;

    protected StaticQueueSink<LifecycleEvent> lifecycleEvents;
    protected StaticQueueSink<StreamDuration> streamDurations;
    protected StaticQueueSink<AverageStreamDuration> averageStreamDurations;
    protected StaticQueueSink<StreamTimeoutWarning> streamTimeoutWarnings;
    protected StaticQueueSink<StreamFailedWarning> streamFailedWarnings;
    protected StaticQueueSink<Alert> alerts;

    @Before
    public void setUpEnvironment() throws Exception {
        epe = EventProcessingFactory.createEventProcessingEnvironment();

        assertNotNull("#createEventProcessingEnvironment() not implemented", epe);

        lifecycleEvents = new StaticQueueSink<>("lifecycleEvents");
        streamDurations = new StaticQueueSink<>("streamDurations");
        averageStreamDurations = new StaticQueueSink<>("averageStreamDurations");
        streamTimeoutWarnings = new StaticQueueSink<>("streamTimeoutWarnings");
        streamFailedWarnings = new StaticQueueSink<>("streamFailedWarnings");
        alerts = new StaticQueueSink<>("alerts");

        epe.setLifecycleEventStreamSink(lifecycleEvents);
        epe.setStreamDurationStreamSink(streamDurations);
        epe.setAverageStreamDurationStreamSink(averageStreamDurations);
        epe.setStreamTimeoutWarningStreamSink(streamTimeoutWarnings);
        epe.setStreamFailedWarningStreamSink(streamFailedWarnings);
        epe.setAlertStreamSink(alerts);
        epe.setStreamDurationTimeout(Time.seconds(15));
    }

    public JobExecutionResult initAndExecute() throws Exception {
        return initAndExecute(null);
    }

    public JobExecutionResult initAndExecute(Consumer<IEventProcessingEnvironment> initializer) throws Exception {
        if (initializer != null) {
            initializer.accept(epe);
        }
        LOG.info("Initializing StreamExecutionEnvironment with {}", epe);
        epe.initialize(flink);
        LOG.info("Executing flink {}", flink);
        return flink.execute();
    }

    public Future<JobExecutionResult> initAndExecuteAsync(Consumer<IEventProcessingEnvironment> initializer) {
        return executor.submit(() -> initAndExecute(initializer));
    }

    public Future<JobExecutionResult> initAndExecuteAsync() {
        return executor.submit(() -> initAndExecute());
    }

    @After
    public void tearDownCollectors() throws Exception {
        StaticQueueSink.clearAll();
    }

}
