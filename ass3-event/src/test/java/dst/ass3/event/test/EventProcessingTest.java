package dst.ass3.event.test;

import static dst.ass3.model.EventType.INTERACTIVE;
import static dst.ass3.model.EventType.UNCLASSIFIED;
import static dst.ass3.model.LifecycleState.ASSIGNED;
import static dst.ass3.model.LifecycleState.READY_FOR_STREAMING;
import static dst.ass3.model.LifecycleState.STREAMED;
import static dst.ass3.model.LifecycleState.STREAMING_NOT_POSSIBLE;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.apache.flink.api.common.JobExecutionResult;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dst.ass3.dto.EventInfoDTO;
import dst.ass3.event.EventProcessingTestBase;
import dst.ass3.event.model.Alert;
import dst.ass3.event.model.AverageStreamDuration;
import dst.ass3.event.model.LifecycleEvent;
import dst.ass3.event.model.StreamDuration;
import dst.ass3.event.model.StreamFailedWarning;
import dst.ass3.event.model.StreamTimeoutWarning;
import dst.ass3.event.model.Warning;

/**
 * EventProcessingTest.
 */
public class EventProcessingTest extends EventProcessingTestBase {

    private static final Logger LOG = LoggerFactory.getLogger(EventProcessingTest.class);

    @Test
    public void lifecycleEventStream() throws Exception {
        // run flink in new thread
        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync();

        // wait until the a subscriber connects
        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        LifecycleEvent event;

        long then = System.currentTimeMillis();

        // causes LifecycleEvent 0
        LOG.info("Publishing e0");
        publisher.publish(new EventInfoDTO(0L, 1L, then, ASSIGNED, "s1", INTERACTIVE));

        LOG.info("Collecting LifecycleEvent for e0");
        event = lifecycleEvents.take();
        LOG.info("Round-trip took {}ms", System.currentTimeMillis() - then);

        assertEquals("Event ID not set correctly", 1L, event.getEventId());
        assertEquals("State not set correctly", ASSIGNED, event.getState());
        assertEquals("Server not set correctly", "s1", event.getServer());
        assertThat("Timestamp not set correctly", event.getTimestamp(), Matchers.greaterThanOrEqualTo(then));
        assertThat("Timestamp not set correctly", event.getTimestamp(), Matchers.lessThanOrEqualTo(System.currentTimeMillis()));


        // should be filtered
        LOG.info("Publishing e1, should be filtered");
        publisher.publish(new EventInfoDTO(1L, 2L, now(), ASSIGNED, "s2", UNCLASSIFIED));

        assertNull("Events of type UNCLASSIFIED should be filtered", lifecycleEvents.poll(500));

        // causes LifecycleEvent 1
        LOG.info("Publishing e2");
        publisher.publish(new EventInfoDTO(2L, 1L, now(), READY_FOR_STREAMING, "s1", INTERACTIVE));

        LOG.info("Collecting LifecycleEvent for e2");
        event = lifecycleEvents.take();
        assertEquals(1L, event.getEventId());
        assertEquals(READY_FOR_STREAMING, event.getState());

        // should be filtered
        LOG.info("Publishing e3, should be filtered");
        publisher.publish(new EventInfoDTO(3L, 2L, now(), READY_FOR_STREAMING, "s2", UNCLASSIFIED));
        assertNull("Events of type UNCLASSIFIED should be filtered", lifecycleEvents.poll(500));

        // causes LifecycleEvent 2
        LOG.info("Publishing e4");
        publisher.publish(new EventInfoDTO(4L, 1L, now(), STREAMED, "s1", INTERACTIVE));

        LOG.info("Collecting LifecycleEvent for e4");
        event = lifecycleEvents.take();
        assertEquals(1L, event.getEventId());
        assertEquals(STREAMED, event.getState());

        // disconnect subscribers
        publisher.dropClients();

        // wait for execution to end
        flinkExecution.get();
    }

    @Test
    public void streamDurationStream_01() throws Exception {
        // tests whether duration calculation and stream keying works correctly
        // expects LifecycleEvent stream to work correctly

        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync();

        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        LifecycleEvent e1Start;
        LifecycleEvent e2Start;
        LifecycleEvent e1End;
        LifecycleEvent e2End;

        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, ASSIGNED, "s1", INTERACTIVE)); // 1 starts before 2
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 3L, t, ASSIGNED, "s1", INTERACTIVE)); // 3 never finishes

        LOG.info("Waiting for LifecycleEvent for event 1 being ASSIGNED");
        e1Start = lifecycleEvents.take();
        LOG.info("Waiting for LifecycleEvent for event 2 being ASSIGNED");
        e2Start = lifecycleEvents.take();
        LOG.info("Waiting for LifecycleEvent for event 3 being ASSIGNED");
        lifecycleEvents.take();

        sleep(500);
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));

        LOG.info("Waiting for LifecycleEvent for event 1 being READY_FOR_STREAMING");
        lifecycleEvents.take();
        LOG.info("Waiting for LifecycleEvent for event 2 being READY_FOR_STREAMING");
        lifecycleEvents.take();

        sleep(500); // event 2 took about ~1000ms
        publisher.publish(new EventInfoDTO(0L, 2L, now(), STREAMED, "s1", INTERACTIVE)); // 2 finishes before 1

        LOG.info("Waiting for LifecycleEvent for event 2 being STREAMED");
        e2End = lifecycleEvents.take();

        sleep(500); // event 1 took about ~1500ms
        publisher.publish(new EventInfoDTO(0L, 1L, now(), STREAMED, "s1", INTERACTIVE));

        LOG.info("Waiting for LifecycleEvent for event 1 being STREAMED");
        e1End = lifecycleEvents.take();

        LOG.info("Collecting StreamDuration event for event 2");
        StreamDuration d0 = streamDurations.take();

        LOG.info("Collecting StreamDuration event for event 1");
        StreamDuration d1 = streamDurations.take();

        assertEquals("Expected event 2 to finish first", 2L, d0.getEventId()); // event 2 finished before 1
        assertEquals("Expected event 1 to finish last", 1L, d1.getEventId());

        assertThat("Expected StreamDuration to be >= 0", d0.getDuration(), greaterThan(0L));
        assertThat("Expected StreamDuration to be >= 0", d1.getDuration(), greaterThan(0L));

        assertEquals("StreamDuartion was not calculated from LifecycleEvents correctly",
                e2End.getTimestamp() - e2Start.getTimestamp(), d0.getDuration(), 100);

        assertEquals("StreamDuartion was not calculated from LifecycleEvents correctly",
                e1End.getTimestamp() - e1Start.getTimestamp(), d1.getDuration(), 100);

        publisher.dropClients();
        flinkExecution.get();
    }

    @Test
    public void streamDurationStream_02() throws Exception {
        // tests whether CEP rule is tolerant towards multiple state changes between ASSIGNED and STREAMED

        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync(e ->
                e.setStreamDurationTimeout(Time.seconds(1))
        );

        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, ASSIGNED, "s1", INTERACTIVE)); // never finishes

        // change state several times (tests another aspect of the CEP rule)
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));

        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, STREAMED, "s1", INTERACTIVE));

        publisher.dropClients();
        flinkExecution.get();

        List<StreamDuration> result = new ArrayList<>(streamDurations.get());
        assertEquals("Expected one event to have finished", 1, result.size());

        StreamDuration d0 = result.get(0);
        assertEquals("Expected event 1 to have finished", 1L, d0.getEventId());
    }

    @Test
    public void streamTimeoutWarningStream() throws Exception {
        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync(e -> {
            e.setStreamDurationTimeout(Time.seconds(1));
        });

        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        publisher.publish(new EventInfoDTO(0L, 1L, now(), ASSIGNED, "s1", INTERACTIVE)); // never finishes

        sleep(100);
        publisher.publish(new EventInfoDTO(0L, 2L, now(), ASSIGNED, "s1", INTERACTIVE)); // never finishes

        // confounding event
        sleep(100);
        publisher.publish(new EventInfoDTO(0L, 1L, now(), READY_FOR_STREAMING, "s1", INTERACTIVE));

        sleep(1500); // wait for event to time out

        publisher.dropClients();

        LOG.info("Waiting for Flink execution to end");
        flinkExecution.get();

        LOG.info("Collecting timeout warning for event 1");
        StreamTimeoutWarning w1 = streamTimeoutWarnings.take();

        LOG.info("Collecting timeout warning for event 2");
        StreamTimeoutWarning w2 = streamTimeoutWarnings.take();

        assertEquals("Expected event 1 to time out first", 1L, w1.getEventId());
        assertEquals("Expected event 2 to time out second", 2L, w2.getEventId());
    }

    @Test
    public void averageStreamDurationStream_01() throws Exception {
        // makes sure the event is triggered at the correct instant

        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync();

        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        sleep(250);
        publisher.publish(new EventInfoDTO(0L, 1L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 2L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 3L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 4L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 5L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 6L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 7L, now(), ASSIGNED, "s2", INTERACTIVE));

        sleep(100);
        publisher.publish(new EventInfoDTO(0L, 1L, now(), READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 2L, now(), READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 3L, now(), READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 4L, now(), READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 5L, now(), READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 6L, now(), READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 7L, now(), READY_FOR_STREAMING, "s2", INTERACTIVE));

        sleep(100);
        publisher.publish(new EventInfoDTO(0L, 1L, now(), STREAMED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 2L, now(), STREAMED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 3L, now(), STREAMED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 4L, now(), STREAMED, "s1", INTERACTIVE));

        // the first four events should not trigger anything
        LOG.info("Checking AverageStreamDuration events after the first four STREAMED events of s1");
        assertNull(averageStreamDurations.poll(500));

        // this event is from a different server
        publisher.publish(new EventInfoDTO(0L, 7L, now(), STREAMED, "s2", INTERACTIVE));
        LOG.info("Checking AverageStreamDuration events after the first STREAMED event of s2");
        assertNull(averageStreamDurations.poll(500));

        // fifth event in s1 triggers the window operation
        publisher.publish(new EventInfoDTO(0L, 5L, now(), STREAMED, "s1", INTERACTIVE));
        LOG.info("Collecting AverageStreamDuration event for s1");
        AverageStreamDuration event = averageStreamDurations.take();
        assertNotNull(event);
        assertEquals("s1", event.getServer());

        // should be in a new window and therefore not trigger
        publisher.publish(new EventInfoDTO(0L, 6L, now(), STREAMED, "s1", INTERACTIVE));
        LOG.info("Checking AverageStreamDuration events after the sixth STREAMED event of s1");
        assertNull(averageStreamDurations.poll(500));

        publisher.dropClients();

        flinkExecution.get();
    }

    @Test
    public void averageStreamDurationStream_02() throws Exception {
        // makes sure the keying works properly and that the calculation is done from StreamDuration events
        // requires StreamDuration events to be calculated correctly

        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync();

        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        List<StreamDuration> s1Durations = new ArrayList<>(5);
        List<StreamDuration> s2Durations = new ArrayList<>(5);

        sleep(250);
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 3L, t, ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 4L, t, ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 5L, t, ASSIGNED, "s1", INTERACTIVE));

        sleep(250);
        publisher.publish(5, t -> new EventInfoDTO(0L, 6L, t, ASSIGNED, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 7L, t, ASSIGNED, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 8L, t, ASSIGNED, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 9L, t, ASSIGNED, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 10L, t, ASSIGNED, "s2", INTERACTIVE));

        sleep(125);
        publisher.publish(5, t -> new EventInfoDTO(0L, 6L, t, READY_FOR_STREAMING, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 7L, t, READY_FOR_STREAMING, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 8L, t, READY_FOR_STREAMING, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 9L, t, READY_FOR_STREAMING, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 10L, t, READY_FOR_STREAMING, "s2", INTERACTIVE));

        sleep(125);
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 3L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 4L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 5L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));

        publisher.publish(5, t -> new EventInfoDTO(0L, 6L, t, STREAMED, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 7L, t, STREAMED, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 8L, t, STREAMED, "s2", INTERACTIVE));

        LOG.info("Collecting StreamDuration events 1,2,3 for s2");
        s2Durations.addAll(streamDurations.take(3));

        sleep(500);
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, STREAMED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, STREAMED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 3L, t, STREAMED, "s1", INTERACTIVE));

        LOG.info("Collecting StreamDuration events 1,2,3 for s1");
        s1Durations.addAll(streamDurations.take(3));

        publisher.publish(5, t -> new EventInfoDTO(0L, 9L, t, STREAMED, "s2", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 10L, t, STREAMED, "s2", INTERACTIVE));

        LOG.info("Collecting StreamDuration events 4,5 for s2");
        s2Durations.addAll(streamDurations.take(2));

        sleep(500);
        publisher.publish(5, t -> new EventInfoDTO(0L, 4L, t, STREAMED, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 5L, t, STREAMED, "s1", INTERACTIVE));

        LOG.info("Collecting StreamDuration events 4,5 for s1");
        s1Durations.addAll(streamDurations.take(2));

        LOG.info("Collecting AverageStreamDuration event for s2");
        AverageStreamDuration e0 = averageStreamDurations.take(); // s2

        LOG.info("Collecting AverageStreamDuration event for s1");
        AverageStreamDuration e1 = averageStreamDurations.take(); // s1

        assertEquals("Expected calculation for s2 to have been triggered first", e0.getServer(), "s2");
        assertEquals("Expected calculation for s1 to have been triggered second", e1.getServer(), "s1");

        assertEquals("Wrong number of StreamDuration events for s1", 5, s1Durations.size());
        assertEquals("Wrong number of StreamDuration events for s2", 5, s2Durations.size());

        double s1Avg = s1Durations.stream().mapToLong(StreamDuration::getDuration).average().orElse(-1);
        double s2Avg = s2Durations.stream().mapToLong(StreamDuration::getDuration).average().orElse(-1);

        assertEquals("Average duration was not calculated from StreamDuration events correctly", e0.getDuration(), s2Avg, 100);
        assertEquals("Average duration was not calculated from StreamDuration events correctly", e1.getDuration(), s1Avg, 100);

        publisher.dropClients();
        flinkExecution.get();
    }

    @Test
    public void streamFailedWarningsStream() throws Exception {
        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync();

        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        publisher.publish(0, t -> new EventInfoDTO(0L, 1L, t, ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(0, t -> new EventInfoDTO(0L, 2L, t, ASSIGNED, "s1", INTERACTIVE));

        // event 1 fail #1
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));

        // event 2 fail #1
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));

        // event 1 fail #2
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));

        // event 2 fail #2 and then success
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 2L, t, STREAMED, "s1", INTERACTIVE));

        LOG.info("Checking that no StreamFailedWarning was issued yet");
        assertNull(streamFailedWarnings.poll(500));

        LOG.info("Triggering third failure");
        // event 1 fail #3
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
        publisher.publish(5, t -> new EventInfoDTO(0L, 1L, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));

        LOG.info("Waiting for StreamFailedWarning for event 1");
        StreamFailedWarning warning = streamFailedWarnings.take();
        assertEquals(1L, warning.getEventId());
        assertEquals("s1", warning.getServer());

        publisher.dropClients();
        flinkExecution.get();
    }

    @Test
    public void alertStream_01() throws Exception {
        // checks that the window works correctly
        // expects StreamFailedWarning stream to work correctly

        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync();

        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        Consumer<Long> causeWarning = (eventId) -> {
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, READY_FOR_STREAMING, "s1", INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, STREAMING_NOT_POSSIBLE, "s1", INTERACTIVE));
        };

        // all of these events will fail
        publisher.publish(new EventInfoDTO(0L, 1L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 2L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 3L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 4L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 5L, now(), ASSIGNED, "s1", INTERACTIVE));
        publisher.publish(new EventInfoDTO(0L, 6L, now(), ASSIGNED, "s1", INTERACTIVE));

        // warning 3 warnings
        causeWarning.accept(1L);
        causeWarning.accept(2L);
        causeWarning.accept(3L);

        LOG.info("Collecting alert for first three warnings");
        assertNotNull(alerts.take());

        LOG.info("Checking that the fourth warning does not trigger an alert");
        causeWarning.accept(4L);
        assertNull(alerts.poll(500));

        LOG.info("Checking that the fifth warning does not trigger an alert");
        causeWarning.accept(5L);
        assertNull(alerts.poll(500));

        LOG.info("Checking that the sixth warning triggered an alert");
        causeWarning.accept(6L);
        assertNotNull(alerts.take());

        publisher.dropClients();
        flinkExecution.get();
    }

    @Test
    public void alertStream_02() throws Exception {
        // checks whether keying works correctly, and whether Warning streams are unioned correctly
        // expects both warning streams to work correctly

        Future<JobExecutionResult> flinkExecution = initAndExecuteAsync(e ->
                e.setStreamDurationTimeout(Time.seconds(3))
        );

        LOG.info("Waiting for subscribers to connect");
        publisher.waitForClients();

        BiConsumer<Long, String> causeWarning = (eventId, server) -> {
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, READY_FOR_STREAMING, server, INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, STREAMING_NOT_POSSIBLE, server, INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, READY_FOR_STREAMING, server, INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, STREAMING_NOT_POSSIBLE, server, INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, READY_FOR_STREAMING, server, INTERACTIVE));
            publisher.publish(5, t -> new EventInfoDTO(0L, eventId, t, STREAMING_NOT_POSSIBLE, server, INTERACTIVE));
        };

        publisher.publish(new EventInfoDTO(0L, 1L, now(), ASSIGNED, "s1", INTERACTIVE)); // s1 e1 will fail
        publisher.publish(new EventInfoDTO(0L, 2L, now(), ASSIGNED, "s1", INTERACTIVE)); // s1 e2 will fail
        publisher.publish(new EventInfoDTO(0L, 3L, now(), ASSIGNED, "s1", INTERACTIVE)); // s1 e3 will time out
        publisher.publish(new EventInfoDTO(0L, 4L, now(), ASSIGNED, "s2", INTERACTIVE)); // s2 e4 will fail

        // s1 warning #1
        causeWarning.accept(1L, "s1");

        // s1 warning #2
        causeWarning.accept(2L, "s1");

        // s2 warning #1
        causeWarning.accept(4L, "s2");


        LOG.info("Checking that no alert has been issued yet");
        assertNull(alerts.poll(500));

        // make sure the other events don't time out
        publisher.publish(new EventInfoDTO(0L, 1L, now(), STREAMED, "s1", INTERACTIVE)); // s1 e1 will fail
        publisher.publish(new EventInfoDTO(0L, 2L, now(), STREAMED, "s1", INTERACTIVE)); // s1 e2 will fail
        publisher.publish(new EventInfoDTO(0L, 4L, now(), STREAMED, "s2", INTERACTIVE)); // s2 e4 will fail

        sleep(4000); // waiting for e3 to time out

        publisher.dropClients();
        flinkExecution.get();

        LOG.info("Collecting Alert event");
        Alert alert = alerts.take();
        assertEquals("Expected only a single alert", 0, alerts.get().size());

        assertEquals("s1", alert.getServer());
        assertEquals("An alert should comprise three warnings", 3, alert.getWarnings().size());

        Warning w0 = alert.getWarnings().get(0);
        Warning w1 = alert.getWarnings().get(1);
        Warning w2 = alert.getWarnings().get(2);

        assertThat(w0, instanceOf(StreamFailedWarning.class));
        assertThat(w1, instanceOf(StreamFailedWarning.class));
        assertThat(w2, instanceOf(StreamTimeoutWarning.class));

        assertEquals("s1", w0.getServer());
        assertEquals("s1", w1.getServer());
        assertEquals("s1", w2.getServer());
    }

}