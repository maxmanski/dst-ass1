package dst.ass3.event.impl;

import dst.ass3.event.Constants;
import dst.ass3.event.EventSubscriber;
import dst.ass3.event.IEventSourceFunction;
import dst.ass3.model.IEventInfo;
import org.apache.flink.api.common.functions.IterationRuntimeContext;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.watermark.Watermark;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.util.Date;

/**
 * Created by Max on 05.06.2017.
 */
public class EventSourceFunction implements IEventSourceFunction, Serializable {

    transient private EventSubscriber eventSubscriber;
    transient private RuntimeContext runtimeContext;
    private boolean isRunning = true;

    @Override
    public void open(Configuration parameters) throws Exception {
        this.eventSubscriber = EventSubscriber.subscribe(new InetSocketAddress(Constants.EVENT_PUBLISHER_PORT));
        this.isRunning = true;
    }

    @Override
    public void close() throws Exception {
        if (this.eventSubscriber != null) {
            this.eventSubscriber.close();
        }
        this.eventSubscriber = null;
    }

    @Override
    public RuntimeContext getRuntimeContext() {
        return this.runtimeContext;
    }

    @Override
    public IterationRuntimeContext getIterationRuntimeContext() {
        return (IterationRuntimeContext) this.runtimeContext;
    }

    @Override
    public void setRuntimeContext(RuntimeContext runtimeContext) {
        this.runtimeContext = runtimeContext;
    }

    @Override
    public void run(SourceContext<IEventInfo> ctx) throws Exception {
        while (this.isRunning) {
            IEventInfo info = this.eventSubscriber.receive();

            if (info != null) {

                ctx.collect(info);
                /*
                if(info.getTimestamp() != null){
                    ctx.emitWatermark(new Watermark(info.getTimestamp()));
                }
                else{
                    ctx.emitWatermark(new Watermark(new Date().getTime()));
                }
                */
            }else{
                this.isRunning = false;
                break;
            }
        }
    }

    @Override
    public void cancel() {
        this.isRunning = false;
    }
}
