package dst.ass3.event.impl;

import dst.ass3.event.EventProcessingFactory;
import dst.ass3.event.IEventProcessingEnvironment;
import dst.ass3.event.IEventSourceFunction;
import dst.ass3.event.model.*;
import dst.ass3.model.EventType;
import dst.ass3.model.IEventInfo;
import dst.ass3.model.LifecycleState;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FoldFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.cep.CEP;
import org.apache.flink.cep.PatternSelectFunction;
import org.apache.flink.cep.PatternStream;
import org.apache.flink.cep.PatternTimeoutFunction;
import org.apache.flink.cep.pattern.Pattern;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.collector.selector.OutputSelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SplitStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.types.Either;
import org.apache.flink.util.Collector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Max on 05.06.2017.
 */
public class EventProcessingEnvironment implements IEventProcessingEnvironment, Serializable{

    private IEventSourceFunction sourceFunction;
    private StreamExecutionEnvironment env;
    private Time streamDurationTimeout;
    private SinkFunction<LifecycleEvent> lifecycleEventStreamSink;
    private SinkFunction<StreamDuration> streamDurationStreamSink;
    private SinkFunction<AverageStreamDuration> averageStreamDurationStreamSink;
    private SinkFunction<StreamTimeoutWarning> streamTimeoutWarningStreamSink;
    private SinkFunction<StreamFailedWarning> streamFailedWarningStreamSink;
    private SinkFunction<Alert> alertStreamSink;

    @Override
    public void initialize(StreamExecutionEnvironment env) {
        this.env = env;

        this.sourceFunction = EventProcessingFactory.createEventSourceFunction();
        DataStream<IEventInfo> source = env.addSource(sourceFunction);

        DataStream<LifecycleEvent> events;
        DataStream<StreamDuration> streamDurations;
        DataStream<AverageStreamDuration> averageStreamDurations;
        DataStream<StreamTimeoutWarning> streamTimeoutWarnings;
        DataStream<StreamFailedWarning> streamFailedWarnings;
        DataStream<Alert> alerts;

        // ---------------------------------
        // <LIFECYCLE EVENT>
        // ---------------------------------
        events = source
                .filter(new FilterFunction<IEventInfo>() {
                    @Override
                    public boolean filter(IEventInfo iEventInfo) throws Exception {
                        return !iEventInfo.getType().equals(EventType.UNCLASSIFIED);
                    }
                })
                .map(new MapFunction<IEventInfo, LifecycleEvent>() {
                    @Override
                    public LifecycleEvent map(IEventInfo iEventInfo) throws Exception {
                        return new LifecycleEvent(iEventInfo);
                    }
                })
                .assignTimestampsAndWatermarks(new AssignerWithPunctuatedWatermarks<LifecycleEvent>() {
                    @Override
                    public Watermark checkAndGetNextWatermark(LifecycleEvent lifecycleEvent, long l) {
                        return new Watermark(l);
                    }

                    @Override
                    public long extractTimestamp(LifecycleEvent lifecycleEvent, long l) {
                        return lifecycleEvent.getTimestamp();
                    }
                });

        // duplicate a stream such that each fork can be used by separate child streams without interrupting others
        List<DataStream<LifecycleEvent>> eventStreams = this.duplicateStream(events, 3);

        // ---------------------------------
        // <DURATION> and <TIMEOUT WARNINGS>
        // ---------------------------------
        DataStream<LifecycleEvent> partitionedInput = eventStreams.get(1).keyBy(new KeySelector<LifecycleEvent, Long>() {
            @Override
            public Long getKey(LifecycleEvent lifecycleEvent) throws Exception {
                return lifecycleEvent.getEventId();
            }
        });

        // create the pattern where we want to check transitions from ASSIGNED to STREAMED
        Pattern<LifecycleEvent, ?> pattern = Pattern.<LifecycleEvent>begin("start")
                .where(new FilterFunction<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) throws Exception {
                        return lifecycleEvent.getState().equals(LifecycleState.ASSIGNED);
                    }
                })
                .followedBy("end")
                .where(new FilterFunction<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) throws Exception {
                        return lifecycleEvent.getState().equals(LifecycleState.STREAMED);
                    }
                })
                .within(this.streamDurationTimeout);

        // create pattern stream
        PatternStream<LifecycleEvent> patternStream = CEP.pattern(partitionedInput, pattern);

        // define TimeoutFunction and SelectFunction
        PatternTimeoutFunction<LifecycleEvent, StreamTimeoutWarning> timeoutFunction = new PatternTimeoutFunction<LifecycleEvent, StreamTimeoutWarning>() {
            @Override
            public StreamTimeoutWarning timeout(Map<String, LifecycleEvent> map, long l) throws Exception {
                LifecycleEvent event = map.get("start");
                return new StreamTimeoutWarning(event.getEventId(), event.getServer());
            }
        };

        PatternSelectFunction<LifecycleEvent, StreamDuration> selectFunction = new PatternSelectFunction<LifecycleEvent, StreamDuration>() {
            @Override
            public StreamDuration select(Map<String, LifecycleEvent> map) throws Exception {
                LifecycleEvent start = map.get("start");
                LifecycleEvent end = map.get("end");
                long duration = end.getTimestamp() - start.getTimestamp();
                return new StreamDuration(start.getEventId(), start.getServer(), duration);
            }
        };

        DataStream<Either<StreamTimeoutWarning, StreamDuration>> tmp = patternStream.select(timeoutFunction, selectFunction);

        // fetching the left and right parts
        // i.e. TimeoutWarnings (left)
        // and  StreamDurations (right)
        // into separate streams
        streamTimeoutWarnings = tmp.filter(new FilterFunction<Either<StreamTimeoutWarning, StreamDuration>>() {
                    @Override
                    public boolean filter(Either<StreamTimeoutWarning, StreamDuration> streamTimeoutWarningStreamDurationEither) throws Exception {
                        return streamTimeoutWarningStreamDurationEither.isLeft();
                    }
                })
                .map(new MapFunction<Either<StreamTimeoutWarning, StreamDuration>, StreamTimeoutWarning>() {
                    @Override
                    public StreamTimeoutWarning map(Either<StreamTimeoutWarning, StreamDuration> streamTimeoutWarningStreamDurationEither) throws Exception {
                        return streamTimeoutWarningStreamDurationEither.left();
                    }
                });
        streamDurations = tmp.filter(new FilterFunction<Either<StreamTimeoutWarning, StreamDuration>>() {
                    @Override
                    public boolean filter(Either<StreamTimeoutWarning, StreamDuration> streamTimeoutWarningStreamDurationEither) throws Exception {
                        return streamTimeoutWarningStreamDurationEither.isRight();
                    }
                }).map(new MapFunction<Either<StreamTimeoutWarning, StreamDuration>, StreamDuration>() {
                    @Override
                    public StreamDuration map(Either<StreamTimeoutWarning, StreamDuration> streamTimeoutWarningStreamDurationEither) throws Exception {
                        return streamTimeoutWarningStreamDurationEither.right();
                    }
                });

        List<DataStream<StreamTimeoutWarning>> timeoutWarnings = this.duplicateStream(streamTimeoutWarnings, 2);
        List<DataStream<StreamDuration>> durations = this.duplicateStream(streamDurations, 2);

        // ---------------------------------
        // <STREAM FAILED WARNING>
        // ---------------------------------
        partitionedInput = eventStreams.get(2).keyBy(new KeySelector<LifecycleEvent, Long>() {
            @Override
            public Long getKey(LifecycleEvent lifecycleEvent) throws Exception {
                return lifecycleEvent.getEventId();
            }
        });

        Pattern<LifecycleEvent, ?> pattern2 = Pattern.<LifecycleEvent>begin("firstReady")
                .where(new FilterFunction<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) throws Exception {
                        return lifecycleEvent.getState().equals(LifecycleState.READY_FOR_STREAMING);
                    }
                })
                .next("firstFail")
                .where(new FilterFunction<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) throws Exception {
                        return lifecycleEvent.getState().equals(LifecycleState.STREAMING_NOT_POSSIBLE);
                    }
                })
                .followedBy("secondReady")
                .where(new FilterFunction<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) throws Exception {
                        return lifecycleEvent.getState().equals(LifecycleState.READY_FOR_STREAMING);
                    }
                })
                .next("secondFail")
                .where(new FilterFunction<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) throws Exception {
                        return lifecycleEvent.getState().equals(LifecycleState.STREAMING_NOT_POSSIBLE);
                    }
                })
                .followedBy("thirdReady")
                .where(new FilterFunction<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) throws Exception {
                        return lifecycleEvent.getState().equals(LifecycleState.READY_FOR_STREAMING);
                    }
                })
                .next("thirdFail")
                .where(new FilterFunction<LifecycleEvent>() {
                    @Override
                    public boolean filter(LifecycleEvent lifecycleEvent) throws Exception {
                        return lifecycleEvent.getState().equals(LifecycleState.STREAMING_NOT_POSSIBLE);
                    }
                });

        PatternStream<LifecycleEvent> patternStream2 = CEP.pattern(partitionedInput, pattern2);

        streamFailedWarnings = patternStream2.select(new PatternSelectFunction<LifecycleEvent, StreamFailedWarning>() {
            @Override
            public StreamFailedWarning select(Map<String, LifecycleEvent> map) throws Exception {
                LifecycleEvent event = map.get("thirdFail");
                return new StreamFailedWarning(event.getEventId(), event.getServer());
            }
        });

        List<DataStream<StreamFailedWarning>> failedWarnings = duplicateStream(streamFailedWarnings, 2);

        // ---------------------------------
        // <ALERTS>
        // ---------------------------------

        KeyedStream<Warning, String> warnings = timeoutWarnings.get(1).map(new MapFunction<StreamTimeoutWarning, Warning>() {
            @Override
            public Warning map(StreamTimeoutWarning streamTimeoutWarning) throws Exception {
                return streamTimeoutWarning;
            }
        }).union(failedWarnings.get(1).map(new MapFunction<StreamFailedWarning, Warning>() {
            @Override
            public Warning map(StreamFailedWarning streamFailedWarning) throws Exception {
                return streamFailedWarning;
            }
        })).keyBy(new KeySelector<Warning, String>() {
            @Override
            public String getKey(Warning warning) throws Exception {
                return warning.getServer();
            }
        });

        alerts = warnings.countWindow(3).apply(new WindowFunction<Warning, Alert, String, GlobalWindow>() {
            @Override
            public void apply(String s, GlobalWindow globalWindow, Iterable<Warning> iterable, Collector<Alert> collector) throws Exception {
                List<Warning> warnings = new ArrayList<>();
                String server = null;
                for(Warning warning: iterable){
                    warnings.add(warning);
                    server = warning.getServer();
                }
                collector.collect(new Alert(server, warnings));
            }
        });

        // ---------------------------------
        // <AVERAGE STREAM DURATION>
        // ---------------------------------

        averageStreamDurations = durations.get(1).keyBy(new KeySelector<StreamDuration, String>(){
            @Override
            public String getKey(StreamDuration streamDuration) throws Exception {
                return streamDuration.getServer();
            }
        }).countWindow(5).sum("duration").map(new MapFunction<StreamDuration, AverageStreamDuration>() {
            @Override
            public AverageStreamDuration map(StreamDuration streamDuration) throws Exception {
                double dur = streamDuration.getDuration() / 5.0;
                AverageStreamDuration avg = new AverageStreamDuration(streamDuration.getServer(), dur);
                return avg;
            }
        });

        // ---------------------------------
        // SINKS
        // ---------------------------------
        eventStreams.get(0).addSink(lifecycleEventStreamSink);
        timeoutWarnings.get(0).addSink(streamTimeoutWarningStreamSink);
        durations.get(0).addSink(streamDurationStreamSink);
        failedWarnings.get(0).addSink(streamFailedWarningStreamSink);
        averageStreamDurations.addSink(averageStreamDurationStreamSink);
        alerts.addSink(alertStreamSink);
    }

    @Override
    public void setStreamDurationTimeout(Time time) {
        this.streamDurationTimeout = time;
    }

    @Override
    public void setLifecycleEventStreamSink(SinkFunction<LifecycleEvent> sink) {
        this.lifecycleEventStreamSink = sink;
    }

    @Override
    public void setStreamDurationStreamSink(SinkFunction<StreamDuration> sink) {
        this.streamDurationStreamSink = sink;
    }

    @Override
    public void setAverageStreamDurationStreamSink(SinkFunction<AverageStreamDuration> sink) {
        this.averageStreamDurationStreamSink = sink;
    }

    @Override
    public void setStreamTimeoutWarningStreamSink(SinkFunction<StreamTimeoutWarning> sink) {
        this.streamTimeoutWarningStreamSink = sink;
    }

    @Override
    public void setStreamFailedWarningStreamSink(SinkFunction<StreamFailedWarning> sink) {
        this.streamFailedWarningStreamSink = sink;
    }

    @Override
    public void setAlertStreamSink(SinkFunction<Alert> sink) {
        this.alertStreamSink = sink;
    }

    private <T>List<DataStream<T>> duplicateStream(DataStream<T> stream, int n){
        List<DataStream<T>> ret = new ArrayList<>();

        SplitStream<T> splitStream = stream.split(new OutputSelector<T>() {
            @Override
            public Iterable<String> select(T t) {
                List<String> output = new ArrayList<>(n);
                for(int i = 0; i < n; i ++){
                    output.add(String.valueOf(i));
                }
                return output;
            }
        });

        for(int i = 0; i < n; i++){
            ret.add(splitStream.select(String.valueOf(i)));
        }

        return ret;
    }

}
