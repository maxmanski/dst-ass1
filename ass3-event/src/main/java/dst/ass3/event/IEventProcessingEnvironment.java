package dst.ass3.event;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.SinkFunction;
import org.apache.flink.streaming.api.windowing.time.Time;

import dst.ass3.event.model.Alert;
import dst.ass3.event.model.AverageStreamDuration;
import dst.ass3.event.model.LifecycleEvent;
import dst.ass3.event.model.StreamDuration;
import dst.ass3.event.model.StreamFailedWarning;
import dst.ass3.event.model.StreamTimeoutWarning;

/**
 * This class should be used to implement the event processing steps as described in the assignment. The test classes
 * will inject SinkFunctions, create a new StreamExecutionEnvironment and then call {@link
 * #initialize(StreamExecutionEnvironment)}.
 */
public interface IEventProcessingEnvironment {

    /**
     * Initializes the event processing graph on the {@link StreamExecutionEnvironment}. This function is called
     * after all sinks have been set.
     */
    void initialize(StreamExecutionEnvironment env);

    /**
     * Sets the timeout limit of a streaming event.
     *
     * @param time the timeout limit
     */
    void setStreamDurationTimeout(Time time);

    void setLifecycleEventStreamSink(SinkFunction<LifecycleEvent> sink);

    void setStreamDurationStreamSink(SinkFunction<StreamDuration> sink);

    void setAverageStreamDurationStreamSink(SinkFunction<AverageStreamDuration> sink);

    void setStreamTimeoutWarningStreamSink(SinkFunction<StreamTimeoutWarning> sink);

    void setStreamFailedWarningStreamSink(SinkFunction<StreamFailedWarning> sink);

    void setAlertStreamSink(SinkFunction<Alert> sink);

}
