package dst.ass3.event.model;

import java.io.Serializable;

/**
 * Indicates the amount of time an IEventInfo took to get from ASSIGNED to STREAMED.
 */
public class StreamDuration implements Serializable {

    private static final long serialVersionUID = -6976972381929291369L;

    private long eventId;
    private String server;
    private long duration;

    public StreamDuration() {
    }

    public StreamDuration(long eventId, String server, long duration) {
        this.eventId = eventId;
        this.server = server;
        this.duration = duration;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "StreamDuration{" +
                "eventId=" + eventId +
                ", server='" + server + '\'' +
                ", duration=" + duration +
                '}';
    }
}
