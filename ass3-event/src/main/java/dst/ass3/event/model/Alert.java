package dst.ass3.event.model;

import java.io.Serializable;
import java.util.List;

/**
 * A system alert that aggregates several warnings.
 */
public class Alert implements Serializable {

    private static final long serialVersionUID = -4561132671849230635L;

    private String server;
    private List<Warning> warnings;

    public Alert() {
    }

    public Alert(String server, List<Warning> warnings) {
        this.server = server;
        this.warnings = warnings;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public List<Warning> getWarnings() {
        return warnings;
    }

    public void setWarnings(List<Warning> warnings) {
        this.warnings = warnings;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "server='" + server + '\'' +
                ", warnings=" + warnings +
                '}';
    }
}
