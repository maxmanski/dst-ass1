package dst.ass3.event.model;

/**
 * Warning that indicates that a streaming event has not reached the lifecycle state STREAMED within a given time frame.
 */
public class StreamTimeoutWarning extends Warning {

    private static final long serialVersionUID = 7955599732178947649L;

    private long eventId;

    public StreamTimeoutWarning() {
    }

    public StreamTimeoutWarning(long eventId, String server) {
        super(server);
        this.eventId = eventId;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return "StreamTimeoutWarning{" +
                "eventId=" + eventId +
                '}';
    }
}
