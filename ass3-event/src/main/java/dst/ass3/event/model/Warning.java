package dst.ass3.event.model;

import java.io.Serializable;

/**
 * Base class for server warnings.
 */
public abstract class Warning implements Serializable {

    private static final long serialVersionUID = 273266717303711974L;

    private String server;

    public Warning() {
    }

    public Warning(String server) {
        this.server = server;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

}
