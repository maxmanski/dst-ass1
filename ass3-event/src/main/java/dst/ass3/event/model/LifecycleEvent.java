package dst.ass3.event.model;

import java.io.Serializable;

import dst.ass3.model.IEventInfo;
import dst.ass3.model.LifecycleState;

/**
 * Indicates a change in the lifecycle state of an IEventInfo.
 */
public class LifecycleEvent implements Serializable {

    private static final long serialVersionUID = 8665269919851487210L;

    /**
     * The id of the Event (the MOS business concept), as returned by {@link IEventInfo#getEventId()}.
     */
    private long eventId;

    /**
     * The server the event was classified by.
     */
    private String server;

    private LifecycleState state;

    /**
     * The instant the event was recorded (unix epoch in milliseconds)
     */
    private long timestamp;

    public LifecycleEvent() {
    }

    public LifecycleEvent(IEventInfo eventInfo) {
        this(eventInfo.getEventId(), eventInfo.getClassifiedBy(), eventInfo.getState(), eventInfo.getTimestamp());
    }

    public LifecycleEvent(long eventId, String server, LifecycleState state, long timestamp) {
        this.eventId = eventId;
        this.server = server;
        this.state = state;
        this.timestamp = timestamp;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public LifecycleState getState() {
        return state;
    }

    public void setState(LifecycleState state) {
        this.state = state;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "LifecycleEvent{" +
                "eventId=" + eventId +
                ", server='" + server + '\'' +
                ", state=" + state +
                ", timestamp=" + timestamp +
                '}';
    }
}
