package dst.ass3.event.model;

/**
 * Indicates that a stream has probably failed.
 */
public class StreamFailedWarning extends Warning {

    private static final long serialVersionUID = -9120187311385112769L;

    private long eventId;

    public StreamFailedWarning() {
    }

    public StreamFailedWarning(long eventId, String server) {
        super(server);
        this.eventId = eventId;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return "StreamFailedWarning{" +
                "eventId=" + eventId +
                '}';
    }
}
