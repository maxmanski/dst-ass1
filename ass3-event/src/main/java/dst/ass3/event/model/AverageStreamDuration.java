package dst.ass3.event.model;

import java.io.Serializable;

/**
 * The average of several {@link StreamDuration} values.
 */
public class AverageStreamDuration implements Serializable {

    private static final long serialVersionUID = -3767582104941550250L;

    private String server;
    private double duration;

    public AverageStreamDuration() {
    }

    public AverageStreamDuration(String server, double duration) {
        this.server = server;
        this.duration = duration;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "AverageStreamDuration{" +
                "server='" + server + '\'' +
                ", duration=" + duration +
                '}';
    }
}
