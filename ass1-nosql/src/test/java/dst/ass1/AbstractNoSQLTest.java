package dst.ass1;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.model.EventStatus;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.util.Constants;
import dst.ass1.jpa.util.DatabaseHelper;
import dst.ass1.jpa.util.PersistenceUtil;
import dst.ass1.nosql.IMongoDbDataLoader;
import dst.ass1.nosql.MongoDbFactory;

public abstract class AbstractNoSQLTest {

    @ClassRule
    public static EmbeddedMongo embeddedMongo = new EmbeddedMongo();

    protected MongoDbFactory mongoDbFactory;
    protected DAOFactory daoFactory;

    protected MongoClient mongoClient;
    protected MongoDatabase db;

    private EntityManagerFactory emf;
    private EntityManager em;
    private IMongoDbDataLoader mongoDbDataLoader;

    @Before
    public void setUp() throws Exception {
        setUpEntityManager();

        new NoSQLTestData(em).insertTestData();
        dropCollection(Constants.COLL_EVENTDATA);

        updateTestData();

        mongoDbFactory = new MongoDbFactory();
        mongoDbDataLoader = mongoDbFactory.createDataLoader(em);
        mongoDbDataLoader.loadData();

        setUpMongoClient();
    }

    @After
    public void tearDown() throws Exception {
        tearDownEntityManager();

        if (mongoDbDataLoader != null) {
            // avoids a timeout exception if dao factory is not implemented
            dropCollection(Constants.COLL_EVENTDATA);
        }

        tearDownMongoClient();
    }

    private void updateTestData() throws InterruptedException {
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        List<IEvent> list = daoFactory.getEventDAO().findAll();
        for (IEvent ent5 : list) {
            ent5.getEventStreaming().setEnd(new Date());
            Thread.sleep(1000);
            ent5.getEventStreaming().setStatus(EventStatus.FINISHED);
            em.persist(ent5);
        }
        tx.commit();
    }

    private void setUpEntityManager() throws Exception {
        emf = Persistence.createEntityManagerFactory(PersistenceUtil.PERSISTENCE_UNIT_NAME);
        em = emf.createEntityManager();
        daoFactory = new DAOFactory(em);
        Objects.requireNonNull(daoFactory.getEventDAO(), "NoSQL tests require an implementation of EventDAO");
        DatabaseHelper.cleanTables(em);
    }

    private void setUpMongoClient() {
        mongoClient = new MongoClient();
        db = mongoClient.getDatabase(Constants.MONGO_DB_NAME);
    }

    private void tearDownEntityManager() throws Exception {
        DatabaseHelper.cleanTables(em);

        if (em.getTransaction().isActive()) {
            em.getTransaction().rollback();
        }
        em.clear();
        em.close();
        emf.close();
    }

    private void tearDownMongoClient() {
        if (mongoClient != null) {
            mongoClient.close();
        }
    }

    private void dropCollection(String collectionName) {
        try (MongoClient mongo = new MongoClient()) {
            MongoDatabase db = mongo.getDatabase(Constants.MONGO_DB_NAME);
            db.getCollection(collectionName).drop();
        }
    }

    protected static Stream<Document> stream(MongoCollection<Document> collection) {
        return StreamSupport.stream(collection.find().spliterator(), false);
    }

    protected static <T> Map<T, Document> idMap(MongoCollection<Document> collection, Function<Document, T> idFunction) {
        return stream(collection).collect(Collectors.toMap(idFunction, Function.identity()));
    }

    @SuppressWarnings("unchecked")
    protected static <T> Map<T, Document> idMap(MongoCollection<Document> collection) {
        return idMap(collection, d -> (T) d.get("_id"));
    }

}
