package dst.ass1;

import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.dao.IEventDAO;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.util.test.TestData;

public class NoSQLTestData extends TestData {

    public NoSQLTestData(EntityManager em) {
        super(em);
    }

    @Override
    public void insertTestData() throws NoSuchAlgorithmException {
        super.insertTestData();

        EntityTransaction tx = em.getTransaction();
        tx.begin();

        IEventDAO ent5DAO = (new DAOFactory(em)).getEventDAO();
        IEvent ent5_4 = ent5DAO.findById(entity5_4Id);
        if (ent5_4 != null) {
            em.remove(ent5_4);
            em.flush();
        }

        tx.commit();
    }

}
