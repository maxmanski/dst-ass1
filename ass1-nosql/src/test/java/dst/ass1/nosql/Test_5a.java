package dst.ass1.nosql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.junit.Test;

import com.mongodb.client.MongoCollection;

import dst.ass1.AbstractNoSQLTest;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.util.Constants;

public class Test_5a extends AbstractNoSQLTest {

    @Test
    public void testMongoDataLoader() throws Exception {
        MongoCollection<Document> collection = db.getCollection(Constants.COLL_EVENTDATA);
        Map<Long, Document> docs = idMap(collection, d -> d.getLong(Constants.I_EVENT));
        List<IEvent> list = daoFactory.getEventDAO().findAll();

        assertFalse(list.isEmpty());
        assertEquals(list.size(), docs.size());

        for (IEvent relObj : list) {
            assertTrue(docs.containsKey(relObj.getId()));
            assertDataEquals(docs.get(relObj.getId()), relObj);
        }
    }

    protected void assertDataEquals(Document document, IEvent relObj) {
        MongoTestData mongoTestData = new MongoTestData();

        assertEquals(
                document.getLong(Constants.PROP_EVENTFINISHED).longValue(),
                relObj.getEventStreaming().getEnd().getTime()
        );
        assertEquals(
                document.get(mongoTestData.getDataDescription(relObj.getId())),
                Document.parse(mongoTestData.getData(relObj.getId()))
        );
    }

}
