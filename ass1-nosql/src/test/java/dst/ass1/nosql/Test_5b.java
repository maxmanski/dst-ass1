package dst.ass1.nosql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Test;

import com.mongodb.MongoException;

import dst.ass1.AbstractNoSQLTest;
import dst.ass1.jpa.model.IEvent;

public class Test_5b extends AbstractNoSQLTest {

    @Test
    public void testFindFinishedQuery() throws MongoException {
        IMongoDbQuery mongoQuery = mongoDbFactory.createQuery(db);

        List<IEvent> list = daoFactory.getEventDAO().findAll();
        for (IEvent relObj : list) {
            assertEquals(
                    relObj.getEventStreaming().getEnd().getTime(),
                    mongoQuery.findFinishedByEventId(relObj.getId()).longValue()
            );
        }
    }

    @Test
    public void testFinishedGt() throws MongoException {
        long time = 1325397600;

        List<IEvent> relObjs = daoFactory.getEventDAO().findAll();
        assertNotNull(relObjs);
        assertFalse(relObjs.isEmpty());

        IMongoDbQuery mongoQuery = mongoDbFactory.createQuery(db);
        List<Long> actual = mongoQuery.findFinishedGt(time);
        assertFalse(actual.isEmpty());

        List<Long> expected = relObjs.stream()
                .filter(o -> o.getEventStreaming().getEnd().getTime() > time)
                .map(IEvent::getId)
                .collect(Collectors.toList());

        assertThat(actual, IsIterableContainingInAnyOrder.containsInAnyOrder(expected.toArray()));
    }
}
