package dst.ass1.nosql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bson.Document;
import org.junit.Test;

import com.mongodb.MongoException;

import dst.ass1.AbstractNoSQLTest;

public class Test_5c extends AbstractNoSQLTest {

    @Test
    public void testMapReduceStreaming() throws MongoException {
        IMongoDbQuery mongoQuery = mongoDbFactory.createQuery(db);

        Map<String, Document> documents = mongoQuery.mapReduceStreaming()
                .stream()
                .collect(Collectors.toMap(d -> d.getString("_id"), Function.identity()));

        assertEquals(3, documents.size());

        Document doc1 = documents.get(MongoTestData.DATA_DESC_LOGS);
        assertNotNull(doc1);
        assertEquals(1.0, doc1.getDouble("value"), 0);

        Document doc2 = documents.get(MongoTestData.DATA_DESC_LOGS);
        assertNotNull(doc2);
        assertEquals(1.0, doc2.getDouble("value"), 0);

        Document doc3 = documents.get(MongoTestData.DATA_DESC_MATRIX);
        assertNotNull(doc3);
        assertEquals(1.0, doc3.getDouble("value"), 0);
    }
}