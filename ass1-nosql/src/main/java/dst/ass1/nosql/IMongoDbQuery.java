package dst.ass1.nosql;

import java.util.List;

import org.bson.Document;

public interface IMongoDbQuery {

    Long findFinishedByEventId(Long id);

    List<Long> findFinishedGt(Long time);

    List<Document> mapReduceStreaming();

}
