package dst.ass1.nosql.impl;

import com.mongodb.DBCollection;
import com.mongodb.MapReduceCommand;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MapReduceIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.util.Constants;
import dst.ass1.nosql.IMongoDbQuery;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.hibernate.criterion.Projection;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Max on 19.03.2017.
 */
public class MongoDbQuery implements IMongoDbQuery{

    private static final Logger LOGGER = Logger.getLogger(MongoDbQuery.class);

    private MongoDatabase mongoDatabase;
    private MongoCollection<Document> collection;

    public MongoDbQuery(MongoDatabase mongoDatabase){
        this.mongoDatabase = mongoDatabase;
        this.collection = this.mongoDatabase.getCollection(Constants.COLL_EVENTDATA);
    }

    @Override
    public Long findFinishedByEventId(Long id) {
        LOGGER.info("Finding Finish Time of Event: #" + id);

        Long finished = null;
        FindIterable<Document> findIterable = this.collection.find(new Document("event_id", id));

        // since we're searching by id, there should be only one result anyways
        for(Document found: findIterable){
            LOGGER.info("Found Event: #" + found.getLong("event_id"));
            finished = found.getLong("event_finished");
        }

        return finished;
    }

    @Override
    public List<Long> findFinishedGt(Long time) {
        LOGGER.info("Finding Events finished after: " + time);

        List<Long> finishTimes = new ArrayList<>();

        // the projection helps to fetch only those parts of the answer that are interesting for us
        FindIterable<Document> findIterable = this.collection.find(Filters.gt("event_finished", time))
                .projection(Projections.fields(Projections.include("event_id")));

        for(Document found: findIterable){
            LOGGER.info("Found Event: #" + found.getLong("event_id"));
            finishTimes.add(found.getLong("event_id"));
        }

        return finishTimes;
    }

    @Override
    public List<Document> mapReduceStreaming() {
        LOGGER.info("Doing MapReduce magic");

        String n = "\n";

        // first, do the map function on every entry in the database
        String map = "function(){" + n +
                "   var prop;" + n +
                "   for(prop in this){" + n +
                "       /* ignore the specified attributes */" + n +
                "       if(prop !== 'event_id' && prop !== 'event_finished' && prop !== '_id'){" + n +
                "           emit(prop, 1);" + n +
                "       }" + n +
                "   }" + n +
                "}";

        // do the reduce function on every result from the map function
        String reduce = "function(key, values){" + n +
                "   var value = 0;" + n +
                "   for(var i = 0; i < values.length; i++){" + n +
                "       value += values[i].value;" + n +
                "   }" + n +
                "   return value;" + n +
                "}";

        MapReduceIterable<Document> result = this.collection.mapReduce(map, reduce);
        List<Document> ret = new ArrayList<>();

        for(Document res: result){
            LOGGER.info("MapReduced Document: " + res);
            ret.add(res);
        }
        return ret;
    }
}
