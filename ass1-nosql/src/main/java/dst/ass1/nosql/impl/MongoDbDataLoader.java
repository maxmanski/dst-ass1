package dst.ass1.nosql.impl;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.util.Constants;
import dst.ass1.nosql.IMongoDbDataLoader;
import dst.ass1.nosql.MongoTestData;
import org.apache.log4j.Logger;
import org.bson.Document;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 19.03.2017.
 */
public class MongoDbDataLoader implements IMongoDbDataLoader{

    private static final Logger LOGGER = Logger.getLogger(MongoDbDataLoader.class);

    private EntityManager em;
    private MongoTestData testData;
    private MongoDatabase mongoDatabase;

    public MongoDbDataLoader(EntityManager em){
        this.em = em;
        this.testData = new MongoTestData();
        this.mongoDatabase = new MongoClient().getDatabase(Constants.MONGO_DB_NAME);
    }

    @Override
    public void loadData() throws Exception {
        LOGGER.info("Loading Data");

        MongoCollection<Document> collection = this.mongoDatabase.getCollection(Constants.COLL_EVENTDATA);

        List<IEvent> finishedEvents = em.createNamedQuery(Constants.Q_ALLFINISHEDEVENTS, IEvent.class).getResultList();

        List<Document> docsToInsert = new ArrayList<>(finishedEvents.size());
        for(IEvent event: finishedEvents){
            long id = event.getId();
            long timestamp = event.getEventStreaming().getEnd().getTime();
            String dataKey = this.testData.getDataDescription(id);
            String dataValue = this.testData.getData(id);

            Document doc = new Document("event_id", id);
            doc.append("event_finished", timestamp);
            doc.append(dataKey, Document.parse(dataValue));

            LOGGER.info("Adding Event: #" + id + " with Data key: " + dataKey);
            docsToInsert.add(doc);
        }

        // add index if it does not exist
        addIndexIfNotExists(collection);
        collection.insertMany(docsToInsert);
    }

    private void addIndexIfNotExists(MongoCollection<Document> collection){

        boolean indexExists = false;
        for(Document index: collection.listIndexes()){
            if (index.containsKey("key")) {
                if (index.get("key") instanceof Document) {
                    Document keyDoc = (Document) index.get("key");
                    if (keyDoc.containsKey("event_id")) {
                        indexExists = true;
                        break;
                    }
                }
            }

            if (index.containsKey("event_id")) {
                indexExists = true;
                break;
            }
        }

        // if the index has not yet been added, add it
        if (!indexExists) {
            LOGGER.info("Adding index on event_id");
            collection.createIndex(new Document("event_id", 1));
        }else{
            LOGGER.info("Index on event_id already exists");
        }
    }
}
