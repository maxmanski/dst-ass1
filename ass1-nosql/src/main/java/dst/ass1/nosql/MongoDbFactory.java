package dst.ass1.nosql;

import javax.persistence.EntityManager;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import dst.ass1.jpa.util.Constants;
import dst.ass1.nosql.impl.MongoDbDataLoader;
import dst.ass1.nosql.impl.MongoDbQuery;
import org.apache.log4j.Logger;

public class MongoDbFactory {

    public MongoDbFactory(){
    }

    public IMongoDbDataLoader createDataLoader(EntityManager em) {
        return new MongoDbDataLoader(em);
    }

    public IMongoDbQuery createQuery(MongoDatabase db) {
        return new MongoDbQuery(db);
    }

}
