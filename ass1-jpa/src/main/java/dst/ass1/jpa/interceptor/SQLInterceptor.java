package dst.ass1.jpa.interceptor;

import dst.ass1.jpa.util.Constants;
import org.apache.log4j.Logger;
import org.hibernate.EmptyInterceptor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SQLInterceptor extends EmptyInterceptor {

    private static final Logger LOGGER = Logger.getLogger(SQLInterceptor.class);
    private static final long serialVersionUID = -3082243834965597947L;

    private static final Object counterLock = new Object();
    private static final Object verboseLock = new Object();
    private static boolean isVerbose = false;
    private static int selectCounter = 0;

    public String onPrepareStatement(String sql) {

        boolean verbose = false;
        synchronized (verboseLock){
            verbose = isVerbose;
        }

        if(verbose){
            System.out.println(sql);
        }

        // make sure there's always only a single whitespace occurring at a time
        // ... but not at the beginning
        String temp = sql.replaceAll("\\s+", " ").trim().toLowerCase();

        String modTable = Constants.T_MODERATOR.toLowerCase();
        String srvTable = Constants.T_STREAMINGSERVER.toLowerCase();

        // note: .+? (or .*?) is the un-greedy version of .+ (or .*)
        //
        // this pattern looks for "SELECT ... FROM Moderator ..." or "SELECT ... FROM StreamingServer ..."
        Pattern moderatorServerSelectPattern = Pattern.compile(
                "^.*?select.+?\\sfrom\\s+((" + modTable + ")|(" + srvTable + ")).*"
        );

        Matcher matcher = moderatorServerSelectPattern.matcher(temp);

        if (matcher.find()) {
            synchronized (counterLock){
                selectCounter++;
            }
        }

        return sql;
    }

    public static void resetCounter() {
        synchronized (counterLock){
            selectCounter = 0;
        }
    }

    public static int getSelectCount() {
        int res = 0;

        synchronized (counterLock){
            res = selectCounter;
        }

        return selectCounter;
    }

    /**
     * If the verbose argument is set, the interceptor prints the intercepted SQL statements to System.out.
     *
     * @param verbose whether or not to be verbose
     */
    public static void setVerbose(boolean verbose) {
        LOGGER.info("Set verbosity of SQLInterceptor: " + (verbose ? "VERBOSE" : "SILENT"));

        synchronized (verboseLock){
            isVerbose = verbose;
        }
    }

}
