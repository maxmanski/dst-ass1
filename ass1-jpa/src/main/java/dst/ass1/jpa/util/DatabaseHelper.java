package dst.ass1.jpa.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;

/**
 * ##########################
 *
 * DO NOT CHANGE THIS CLASS!
 *
 * ##########################
 *
 * Contains various convenience methods for database access.
 * <p/>
 * <b>Note that the caller is responsible for dealing with possible exceptions
 * as well as doing the connection handling.</b><br/>
 * In other words, a connection will not be closed even if a fatal error occurs.
 * However, other SQL resources i.e., {@link Statement Statements} and
 * {@link ResultSet ResultSets} created within the methods, which are not
 * returned to the caller, are closed before the method returns.
 */
public final class DatabaseHelper {
    private DatabaseHelper() {
    }

    /**
     * Checks if the named table can be accessed via the given EntityManager.
     *
     * @param tableName the name of the table to find
     * @param em the EntityManager to use
     * @return {@code true} if the database schema contains a table with the given name, {@code false} otherwise
     * @throws SQLException if a database access error occurs
     */
    public static boolean isTable(final String tableName, EntityManager em) throws SQLException {

        return getSession(em).doReturningWork(new QueryWork<Boolean>("show tables") {
            @Override
            public Boolean execute(ResultSet rs) throws SQLException {
                while (rs.next()) {
                    String tbl = rs.getString(1);
                    if (tbl.equalsIgnoreCase(tableName)) {
                        return true;
                    }
                }
                return false;
            }

        });
    }

    /**
     * Detects the type of table inheritance.
     *
     * @param em the EntityManager to use
     * @param tableName the name of the table
     * @return {@code 0} if the given table exists, {@code 1} otherwise.
     * @throws SQLException
     */
    public static int getInheritanceType(EntityManager em, String tableName) throws SQLException {
        return isTable(tableName, em) ? 0 : 1;
    }

    /**
     * Checks whether a certain database table contains a column with the given
     * name.
     *
     * @param tableName the name of the table to check
     * @param column the name of the column to find
     * @param em the EntityManager to use
     * @return {@code true} if the table contains the column, {@code false} otherwise
     * @throws SQLException if a database access error occurs
     */
    public static boolean isColumnInTable(String tableName, String column, EntityManager em) throws SQLException {
        String sql = String.format(
                "SELECT * FROM information_schema.columns WHERE table_name='%s' and column_name='%s'",
                tableName.toUpperCase(), column.toUpperCase()
        );

        return getSession(em).doReturningWork(new HasAtLeastOneEntry(sql));
    }

    public static boolean isColumnInTableWithType(String tableName, String column, String type, String length,
            EntityManager em) throws SQLException {
        String sql = String.format("SELECT * FROM information_schema.columns "
                        + "WHERE table_name='%s' and column_name='%s' and "
                        + "type_name='%s' and character_maximum_length='%s'",
                tableName.toUpperCase(), column.toUpperCase(), type.toUpperCase(), length);

        return getSession(em).doReturningWork(new HasAtLeastOneEntry(sql));
    }

    /**
     * Checks whether a certain table contains an index for the given column
     * name.
     *
     * @param tableName the name of the table to check
     * @param indexName the name of the column the index is created for
     * @param nonUnique {@code true} if the index is non unique, {@code false} otherwise
     * @param em the EntityManager to use
     * @return {@code true} if the index exists, {@code false} otherwise
     * @throws SQLException if a database access error occurs
     */
    public static boolean isIndex(String tableName, String indexName, boolean nonUnique, EntityManager em)
            throws SQLException {

        String sql = String.format(
                "SELECT * FROM information_schema.indexes WHERE table_name='%s' and column_name='%s' and non_unique='%s'",
                tableName.toUpperCase(), indexName.toUpperCase(), nonUnique ? "1" : "0"
        );

        return getSession(em).doReturningWork(new HasAtLeastOneEntry(sql));
    }

    public static boolean isComposedIndex(String tableName, String columnName1, String columnName2, EntityManager em)
            throws SQLException {
        String indexName1 = getIndexName(tableName, columnName1, em);
        String indexName2 = getIndexName(tableName, columnName2, em);

        return Objects.nonNull(indexName1) && Objects.equals(indexName1, indexName2);
    }

    private static String getIndexName(String tableName, String columnName, EntityManager em) throws SQLException {
        String sql = String.format(
                "SELECT index_name FROM information_schema.indexes WHERE table_name='%s' and column_name='%s'",
                tableName.toUpperCase(), columnName.toUpperCase()
        );

        return getSession(em).doReturningWork(new QueryWork<String>(sql) {
            @Override
            protected String execute(ResultSet rs) throws SQLException {
                return (rs.next()) ? rs.getString(1) : null;
            }
        });
    }

    /**
     * Checks whether the given column of a certain table can contain {@code NULL} values.
     *
     * @param tableName the name of the table to check
     * @param columnName the name of the column to check
     * @param em the EntityManager to use
     * @return {@code true} if the column is nullable, {@code false} otherwise
     * @throws SQLException if a database access error occurs
     */
    public static boolean isNullable(String tableName, String columnName, EntityManager em) throws SQLException {
        String sql = String.format(
                "SELECT * FROM information_schema.columns WHERE table_name='%s' and column_name='%s' and IS_NULLABLE=true",
                tableName.toUpperCase(), columnName.toUpperCase()
        );

        return getSession(em).doReturningWork(new HasAtLeastOneEntry(sql));
    }

    /**
     * Deletes all data from all tables that can be accessed via the given EntityManager.
     *
     * @param em the EntityManager to use
     * @throws Exception if a database access error occurs
     */
    public static void cleanTables(EntityManager em) throws Exception {
        List<String> tables = getTables(em);
        tables.removeIf(t -> t.toLowerCase().startsWith("hibernate"));

        getSession(em).doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                try (Statement stmt = connection.createStatement()) {
                    stmt.addBatch("SET FOREIGN_KEY_CHECKS=0");
                    for (String table : tables) {
                        stmt.addBatch("TRUNCATE TABLE " + table);
                    }
                    stmt.addBatch("SET FOREIGN_KEY_CHECKS=1");
                    stmt.executeBatch();
                }
            }

        });
    }

    /**
     * Returns a list of all table-names for the given database/connection.
     *
     * @param em the EntityManager to use
     * @return List of table names
     * @throws Exception if a database access error occurs
     */
    public static List<String> getTables(EntityManager em) throws Exception {
        return getSession(em).doReturningWork(new CollectionWork<>("show tables", rs -> rs.getString(1)));
    }

    /**
     * Returns the amount of viewer capacity currently available.
     *
     * @param em the EntityManager to use
     * @param id the identifier of the requested MOSPlatform
     * @throws SQLException if a database access error occurs
     */
    public static int getAvailableViewerCapacity(EntityManager em, final String id) throws SQLException {
        final String sql = "SELECT SUM(" + Constants.M_VIEWERCAPACITY + ") "
                + "FROM " + Constants.T_UPLINK + " " + "WHERE "
                + Constants.I_STREAMINGSERVER + "=(" + "SELECT id FROM "
                + Constants.T_STREAMINGSERVER + " " + "WHERE "
                + Constants.I_MOSPLATFORM + "=" + id + " LIMIT 0,1)";

        return getSession(em).doReturningWork(new QueryWork<Integer>(sql) {
            @Override
            protected Integer execute(ResultSet rs) throws SQLException {
                return (rs.next()) ? rs.getInt(1) : null;
            }
        });
    }

    /**
     * Returns the identifiers of existing MOSPlatforms.
     *
     * @param em the EntityManager to use
     * @throws SQLException if a database access error occurs
     */
    public static List<Long> getMOSPlatformIds(EntityManager em) throws SQLException {
        final String sql = "SELECT id FROM " + Constants.T_MOSPLATFORM;
        return getSession(em).doReturningWork(new CollectionWork<>(sql, rs -> rs.getLong(1)));
    }

    public static Session getSession(EntityManager em) {
        return em.unwrap(Session.class);
    }

    interface StatementWork<T> extends ReturningWork<T> {

        default T execute(Connection connection) throws SQLException {
            try (Statement stmt = connection.createStatement()) {
                return execute(stmt);
            }
        }

        T execute(Statement stmt) throws SQLException;
    }

    public static abstract class QueryWork<T> implements StatementWork<T> {
        private final String sql;

        public QueryWork(String sql) {
            this.sql = sql;
        }

        @Override
        public T execute(Statement stmt) throws SQLException {
            try (ResultSet rs = stmt.executeQuery(sql)) {
                return execute(rs);
            }
        }

        protected abstract T execute(ResultSet rs) throws SQLException;
    }

    public static class HasAtLeastOneEntry extends QueryWork<Boolean> {

        public HasAtLeastOneEntry(String sql) {
            super(sql);
        }

        @Override
        protected Boolean execute(ResultSet rs) throws SQLException {
            return rs.next();
        }
    }

    private static class CollectionWork<T> extends QueryWork<List<T>> {

        private final CheckedFunction<ResultSet, T, SQLException> extractor;

        public CollectionWork(String sql, CheckedFunction<ResultSet, T, SQLException> extractor) {
            super(sql);
            this.extractor = extractor;
        }

        @Override
        protected List<T> execute(ResultSet rs) throws SQLException {
            List<T> list = new ArrayList<>();
            while (rs.next()) {
                list.add(extractor.apply(rs));
            }
            return list;
        }
    }

    @FunctionalInterface
    public interface CheckedFunction<T, R, E extends Exception> {
        R apply(T t) throws E;
    }
}
