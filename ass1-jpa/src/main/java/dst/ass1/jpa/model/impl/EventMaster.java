package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 15.03.2017.
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"accountNo", "bankCode"})})
@NamedQueries({
        @NamedQuery(name = Constants.Q_EVENTMASTERSSWITHACTIVEMEMBERSHIP,
                query = "SELECT em FROM EventMaster em JOIN em.memberships m JOIN m.MOSPlatform p WHERE p.name LIKE :name AND" +
                        " :minNr <= (SELECT COUNT(e.id) FROM EventMaster em2 " +
                        " JOIN em2.events e JOIN e.eventStreaming es JOIN es.uplinks u JOIN u.streamingServer srv JOIN srv.MOSPlatform mp" +
                        " WHERE em2.id = em.id AND mp.id = p.id)"),

        @NamedQuery(name = Constants.Q_MOSTACTIVEEVENTMASTER,
                query = "SELECT DISTINCT(em) FROM EventMaster em JOIN em.events GROUP BY em HAVING em.events.size >= (SELECT MAX(em2.events.size) FROM EventMaster em2 JOIN em2.events)")
})
public class EventMaster extends Person implements IEventMaster {

    @Column(nullable = false, unique = true)
    private String eventMasterName;
    private String accountNo;
    private String bankCode;

    @Column(columnDefinition = "BINARY(16)")
    private byte[] password;

    @OneToMany(targetEntity = Event.class, mappedBy = "eventMaster")
    private List<IEvent> events;

    @OneToMany(targetEntity = Membership.class, mappedBy = "eventMaster")
    private List<IMembership> memberships;


    public EventMaster(){
        this.events = new ArrayList<>();
        this.memberships = new ArrayList<>();
    }

    @Override
    public String getEventMasterName() {
        return this.eventMasterName;
    }

    @Override
    public void setEventMasterName(String eventMasterName) {
        this.eventMasterName = eventMasterName;
    }

    @Override
    public byte[] getPassword() {
        return this.password;
    }

    @Override
    public void setPassword(byte[] password) {
        this.password = password;
    }

    @Override
    public String getAccountNo() {
        return this.accountNo;
    }

    @Override
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Override
    public String getBankCode() {
        return this.bankCode;
    }

    @Override
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public List<IEvent> getEvents() {
        return this.events;
    }

    @Override
    public void setEvents(List<IEvent> events) {
        this.events = events;
    }

    @Override
    public void addEvent(IEvent event) {
        this.events.add(event);
    }

    @Override
    public List<IMembership> getMemberships() {
        return this.memberships;
    }

    @Override
    public void setMemberships(List<IMembership> memberships) {
        this.memberships = memberships;
    }

    @Override
    public void addMembership(IMembership membership) {
        this.memberships.add(membership);
    }
}
