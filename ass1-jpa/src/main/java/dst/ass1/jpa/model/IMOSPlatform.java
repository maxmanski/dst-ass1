package dst.ass1.jpa.model;

import java.math.BigDecimal;
import java.util.List;

public interface IMOSPlatform {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    String getUrl();

    void setUrl(String url);

    BigDecimal getCostsPerStreamingMinute();

    void setCostsPerStreamingMinute(BigDecimal costsPerStreamingMinute);

    List<IMembership> getMemberships();

    void setMemberships(List<IMembership> memberships);

    void addMembership(IMembership membership);

    List<IStreamingServer> getStreamingServers();

    void setStreamingServers(List<IStreamingServer> streamingServers);

    void addStreamingServer(IStreamingServer streamingServer);

}