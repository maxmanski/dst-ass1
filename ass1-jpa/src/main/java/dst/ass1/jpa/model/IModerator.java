package dst.ass1.jpa.model;

import java.util.List;

public interface IModerator extends IPerson {

    List<IStreamingServer> getAdvisedStreamingServers();

    void setAdvisedStreamingServers(List<IStreamingServer> streamingServers);

    void addAdvisedStreamingServers(IStreamingServer streamingServer);

}
