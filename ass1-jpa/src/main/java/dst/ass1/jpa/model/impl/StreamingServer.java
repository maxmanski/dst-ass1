package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMOSPlatform;
import dst.ass1.jpa.model.IModerator;
import dst.ass1.jpa.model.IStreamingServer;
import dst.ass1.jpa.model.IUplink;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Max on 15.03.2017.
 */
@Entity
public class StreamingServer implements IStreamingServer{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastMaintenance;

    @Temporal(TemporalType.TIMESTAMP)
    private Date nextMaintenance;

    @ManyToMany(targetEntity = StreamingServer.class)
    @JoinTable(name = "composed_of", joinColumns = {@JoinColumn(name = "partOf_id")}, inverseJoinColumns = {@JoinColumn(name = "composedOf_id")})
    private List<IStreamingServer> composedOf;

    @ManyToMany(targetEntity = StreamingServer.class, mappedBy = "composedOf")
    private List<IStreamingServer> partOf;

    @OneToMany(targetEntity = Uplink.class, mappedBy = "streamingServer")
    private List<IUplink> uplinks;

    @ManyToOne(targetEntity = Moderator.class)
    private IModerator moderator;

    @ManyToOne(targetEntity = MOSPlatform.class)
    private IMOSPlatform MOSPlatform;


    public StreamingServer(){
        this.composedOf = new ArrayList<>();
        this.partOf = new ArrayList<>();
        this.uplinks = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Date getLastMaintenance() {
        return this.lastMaintenance;
    }

    @Override
    public void setLastMaintenance(Date lastMaintenance) {
        this.lastMaintenance = lastMaintenance;
    }

    @Override
    public Date getNextMaintenance() {
        return this.nextMaintenance;
    }

    @Override
    public void setNextMaintenance(Date nextMaintenance) {
        this.nextMaintenance = nextMaintenance;
    }

    @Override
    public List<IStreamingServer> getComposedOf() {
        return this.composedOf;
    }

    @Override
    public void setComposedOf(List<IStreamingServer> composedOf) {
        this.composedOf = composedOf;
    }

    @Override
    public void addComposedOf(IStreamingServer streamingServer) {
        if (!this.composedOf.contains(streamingServer)) {
            this.composedOf.add(streamingServer);
        }
    }

    @Override
    public List<IStreamingServer> getPartOf() {
        return this.partOf;
    }

    @Override
    public void setPartOf(List<IStreamingServer> partOf) {
        this.partOf = partOf;
    }

    @Override
    public void addPartOf(IStreamingServer streamingServer) {
        if (!this.partOf.contains(streamingServer)) {
            this.partOf.add(streamingServer);
        }
    }

    @Override
    public List<IUplink> getUplinks() {
        return this.uplinks;
    }

    @Override
    public void setUplinks(List<IUplink> uplinks) {
        this.uplinks = uplinks;
    }

    @Override
    public void addUplink(IUplink uplink) {
        this.uplinks.add(uplink);
    }

    @Override
    public IModerator getModerator() {
        return this.moderator;
    }

    @Override
    public void setModerator(IModerator moderator) {
        this.moderator = moderator;
    }

    @Override
    public IMOSPlatform getMOSPlatform() {
        return this.MOSPlatform;
    }

    @Override
    public void setMOSPlatform(IMOSPlatform platform) {
        this.MOSPlatform = platform;
    }
}
