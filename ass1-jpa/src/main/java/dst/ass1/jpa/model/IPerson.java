package dst.ass1.jpa.model;

public interface IPerson {

    Long getId();

    void setId(Long id);

    String getLastName();

    void setLastName(String lastName);

    String getFirstName();

    void setFirstName(String firstName);

    IAddress getAddress();

    void setAddress(IAddress address);

}