package dst.ass1.jpa.model;

import java.util.Date;
import java.util.List;

public interface IUplink {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    Integer getViewerCapacity();

    void setViewerCapacity(Integer viewerCapacity);

    String getRegion();

    void setRegion(String region);

    Date getActivated();

    void setActivated(Date activated);

    Date getLastUpdate();

    void setLastUpdate(Date lastUpdate);

    IStreamingServer getStreamingServer();

    void setStreamingServer(IStreamingServer streamingServer);

    List<IEventStreaming> getEventStreamings();

    void setEventStreamings(List<IEventStreaming> streamings);

    void addEventStreaming(IEventStreaming streaming);

}