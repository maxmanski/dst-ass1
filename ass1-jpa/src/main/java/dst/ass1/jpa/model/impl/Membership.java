package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.IMOSPlatform;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.IMembershipKey;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Max on 15.03.2017.
 */
@IdClass(MembershipKey.class)
@Entity
public class Membership implements IMembership{

    @Id
    @ManyToOne(targetEntity = MOSPlatform.class)
    private IMOSPlatform MOSPlatform;
    @Id
    @ManyToOne(targetEntity = EventMaster.class)
    private IEventMaster eventMaster;

    @Temporal(TemporalType.TIMESTAMP)
    private Date registration;
    private Double discount;


    @Override
    public IMembershipKey getId() {
        return new MembershipKey(this.eventMaster, this.MOSPlatform);
    }

    @Override
    public void setId(IMembershipKey id) {
        this.eventMaster = id.getEventMaster();
        this.MOSPlatform = id.getMOSPlatform();
    }

    @Override
    public Date getRegistration() {
        return this.registration;
    }

    @Override
    public void setRegistration(Date registration) {
        this.registration = registration;
    }

    @Override
    public Double getDiscount() {
        return this.discount;
    }

    @Override
    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
