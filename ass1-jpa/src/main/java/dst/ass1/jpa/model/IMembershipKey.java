package dst.ass1.jpa.model;

public interface IMembershipKey {

    IEventMaster getEventMaster();

    void setEventMaster(IEventMaster eventMaster);

    IMOSPlatform getMOSPlatform();

    void setMOSPlatform(IMOSPlatform platform);

}
