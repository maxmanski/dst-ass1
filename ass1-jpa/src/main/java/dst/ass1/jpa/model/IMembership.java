package dst.ass1.jpa.model;

import java.util.Date;

public interface IMembership {

    IMembershipKey getId();

    void setId(IMembershipKey id);

    Date getRegistration();

    void setRegistration(Date registration);

    Double getDiscount();

    void setDiscount(Double discount);

}