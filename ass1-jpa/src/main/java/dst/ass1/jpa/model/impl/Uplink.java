package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IEventStreaming;
import dst.ass1.jpa.model.IStreamingServer;
import dst.ass1.jpa.model.IUplink;
import dst.ass1.jpa.validator.ViewerCapacity;

import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Max on 15.03.2017.
 */
@ViewerCapacity(min = 40, max = 80)
public class Uplink implements IUplink{

    private Long id;

    @Pattern(regexp = ".{5,25}", message = "Name must not be shorter than 5 or longer than 25 characters")
    private String name;
    private Integer viewerCapacity;

    @Pattern(regexp = "[A-Z]{3,3}\\-[A-Z]{3,3}\\@\\d\\d\\d\\d+", message = "Region must follow a format like ABC-DEF@zipcode")
    private String region;

    @Past
    private Date activated;

    @Past
    private Date lastUpdate;
    private IStreamingServer streamingServer;
    private List<IEventStreaming> eventStreamings;


    public Uplink(){
        this.eventStreamings = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getViewerCapacity() {
        return this.viewerCapacity;
    }

    @Override
    public void setViewerCapacity(Integer viewerCapacity) {
        this.viewerCapacity = viewerCapacity;
    }

    @Override
    public String getRegion() {
        return this.region;
    }

    @Override
    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public Date getActivated() {
        return this.activated;
    }

    @Override
    public void setActivated(Date activated) {
        this.activated = activated;
    }

    @Override
    public Date getLastUpdate() {
        return this.lastUpdate;
    }

    @Override
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public IStreamingServer getStreamingServer() {
        return this.streamingServer;
    }

    @Override
    public void setStreamingServer(IStreamingServer streamingServer) {
        this.streamingServer = streamingServer;
    }

    @Override
    public List<IEventStreaming> getEventStreamings() {
        return this.eventStreamings;
    }

    @Override
    public void setEventStreamings(List<IEventStreaming> streamings) {
        this.eventStreamings = streamings;
    }

    @Override
    public void addEventStreaming(IEventStreaming streaming) {
        this.eventStreamings.add(streaming);
    }
}
