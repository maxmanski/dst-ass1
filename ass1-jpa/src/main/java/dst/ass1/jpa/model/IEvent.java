package dst.ass1.jpa.model;

public interface IEvent {

    Long getId();

    void setId(Long id);

    Integer getStreamingTime();

    void setStreamingTime(Integer streamingTime);

    Integer getAttendingViewers();

    void setAttendingViewers(Integer attendingViewers);

    boolean isPaid();

    void setPaid(boolean isPaid);

    IMetadata getMetadata();

    void setMetadata(IMetadata metadata);

    IEventMaster getEventMaster();

    void setEventMaster(IEventMaster eventMaster);

	/*
     * Please make sure that the Event entity is the owning side of the relation
	 * with EventStreaming and therefore also propagates (cascades) EntityManager
	 * operations to the relating entity.
	 */

    IEventStreaming getEventStreaming();

    void setEventStreaming(IEventStreaming eventStreaming);

}