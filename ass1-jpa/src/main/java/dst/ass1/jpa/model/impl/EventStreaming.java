package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.EventStatus;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.model.IEventStreaming;
import dst.ass1.jpa.model.IUplink;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Max on 15.03.2017.
 */
@Entity
public class EventStreaming implements IEventStreaming{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date start;

    @Temporal(TemporalType.TIMESTAMP)
    private Date end;

    private EventStatus status;

    @ManyToMany(targetEntity = Uplink.class)
    @JoinTable(name = "streaming_uplink", joinColumns = {@JoinColumn(name = "uplinks_id")}, inverseJoinColumns = {@JoinColumn(name = "eventStreamings_id")})
    private List<IUplink> uplinks;

    // mappedBy here makes sure that it's not the owning side
    // "The inverse side of a bidirectional relationship must refer to its owning side by using the mappedBy element ..."
    @OneToOne(targetEntity = Event.class, mappedBy = "eventStreaming")
    private IEvent event;


    public EventStreaming(){
        this.uplinks = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getStart() {
        return this.start;
    }

    @Override
    public void setStart(Date start) {
        this.start = start;
    }

    @Override
    public Date getEnd() {
        return this.end;
    }

    @Override
    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public EventStatus getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(EventStatus eventStatus) {
        this.status = eventStatus;
    }

    @Override
    public List<IUplink> getUplinks() {
        return this.uplinks;
    }

    @Override
    public void setUplinks(List<IUplink> uplinks) {
        this.uplinks = uplinks;
    }

    @Override
    public void addUplinks(IUplink uplink) {
        this.uplinks.add(uplink);
    }

    @Override
    public IEvent getEvent() {
        return this.event;
    }

    @Override
    public void setEvent(IEvent event) {
        this.event = event;
    }
}
