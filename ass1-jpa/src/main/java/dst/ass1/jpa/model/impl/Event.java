package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.IEventStreaming;
import dst.ass1.jpa.model.IMetadata;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;

/**
 * Created by Max on 15.03.2017.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = Constants.Q_ALLFINISHEDEVENTS, query = "SELECT e FROM Event e JOIN e.eventStreaming es WHERE es.status = dst.ass1.jpa.model.EventStatus.FINISHED")
})
@Table(indexes = {@Index(name = "Metadata_id", columnList = "metadata_id", unique = true)})
public class Event implements IEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Transient
    private Integer streamingTime;

    @Transient
    private Integer attendingViewers;
    private boolean isPaid;

    @OneToOne(targetEntity = Metadata.class)
    private IMetadata metadata;

    @ManyToOne(targetEntity = EventMaster.class)
    private IEventMaster eventMaster;

    @OneToOne(targetEntity = EventStreaming.class, cascade = CascadeType.ALL, optional = false)
    private IEventStreaming eventStreaming;


    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getStreamingTime() {
        return this.streamingTime;
    }

    @Override
    public void setStreamingTime(Integer streamingTime) {
        this.streamingTime = streamingTime;
    }

    @Override
    public Integer getAttendingViewers() {
        return this.attendingViewers;
    }

    @Override
    public void setAttendingViewers(Integer attendingViewers) {
        this.attendingViewers = attendingViewers;
    }

    @Override
    public boolean isPaid() {
        return this.isPaid;
    }

    @Override
    public void setPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    @Override
    public IMetadata getMetadata() {
        return this.metadata;
    }

    @Override
    public void setMetadata(IMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public IEventMaster getEventMaster() {
        return this.eventMaster;
    }

    @Override
    public void setEventMaster(IEventMaster eventMaster) {
        this.eventMaster = eventMaster;
    }

    @Override
    public IEventStreaming getEventStreaming() {
        return this.eventStreaming;
    }

    @Override
    public void setEventStreaming(IEventStreaming eventStreaming) {
        this.eventStreaming = eventStreaming;
    }
}
