package dst.ass1.jpa.model;

import java.util.Date;
import java.util.List;

public interface IEventStreaming {

    Long getId();

    void setId(Long id);

    Date getStart();

    void setStart(Date start);

    Date getEnd();

    void setEnd(Date end);

    EventStatus getStatus();

    void setStatus(EventStatus eventStatus);

    List<IUplink> getUplinks();

    void setUplinks(List<IUplink> uplinks);

    void addUplinks(IUplink uplink);

    IEvent getEvent();

    void setEvent(IEvent event);

}