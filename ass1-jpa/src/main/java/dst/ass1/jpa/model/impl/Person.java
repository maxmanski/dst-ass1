package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IAddress;
import dst.ass1.jpa.model.IPerson;

import javax.persistence.*;

/**
 * Created by Max on 15.03.2017.
 *
 * Why the JOINED strategy?
 * it offers relatively good support for shallow inheritance hierarchies
 * also i think it's conceptually the closest to the java inheritance
 * negative about it is that it might require a large quantity of joins if the hierarchy gets too deep
 * (better cpu, anyone?)
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Person implements IPerson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String lastName;
    private String firstName;

    @Embedded
    private IAddress address;


    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getLastName() {
        return this.lastName;
    }

    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String getFirstName() {
        return this.firstName;
    }

    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public IAddress getAddress() {
        return this.address;
    }

    @Override
    public void setAddress(IAddress address) {
        this.address = address;
    }
}
