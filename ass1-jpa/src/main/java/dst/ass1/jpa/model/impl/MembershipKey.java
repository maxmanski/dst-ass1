package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.IMOSPlatform;
import dst.ass1.jpa.model.IMembershipKey;

import java.io.Serializable;

/**
 * Created by Max on 15.03.2017.
 */
public class MembershipKey implements IMembershipKey, Serializable {

    private IEventMaster eventMaster;
    private IMOSPlatform MOSPlatform;

    public MembershipKey(){}

    public MembershipKey(IEventMaster eventMaster, IMOSPlatform MOSPlatform){
        this.eventMaster = eventMaster;
        this.MOSPlatform = MOSPlatform;
    }

    @Override
    public IEventMaster getEventMaster() {
        return this.eventMaster;
    }

    @Override
    public void setEventMaster(IEventMaster eventMaster) {
        this.eventMaster = eventMaster;
    }

    @Override
    public IMOSPlatform getMOSPlatform() {
        return this.MOSPlatform;
    }

    @Override
    public void setMOSPlatform(IMOSPlatform platform) {
        this.MOSPlatform = platform;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj){
            return true;
        }

        if(!(obj instanceof MembershipKey)){
            return false;
        }

        MembershipKey keyObj = (MembershipKey)obj;
        if((this.eventMaster == null || keyObj.eventMaster == null) && (this.eventMaster != keyObj.eventMaster)){
            return false;
        }
        else if((this.MOSPlatform == null || keyObj.MOSPlatform == null) && (this.MOSPlatform != keyObj.MOSPlatform)){
            return false;
        }
        else if((this.eventMaster != null) && !(this.eventMaster.equals(keyObj.eventMaster))){
            return false;
        }
        else if((this.MOSPlatform != null) && !(this.MOSPlatform.equals(keyObj.MOSPlatform))){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int masterHash = this.eventMaster == null ? 0 : this.eventMaster.hashCode();
        int platformHash = this.MOSPlatform == null ? 0 : this.MOSPlatform.hashCode();
        return masterHash ^ platformHash;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
