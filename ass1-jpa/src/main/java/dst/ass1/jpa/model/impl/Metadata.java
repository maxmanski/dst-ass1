package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMetadata;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 15.03.2017.
 */
@Entity
public class Metadata implements IMetadata{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String game;

    @ElementCollection(fetch = FetchType.LAZY)
    @OrderColumn
    private List<String> settings;

    public Metadata(){
        this.settings = new ArrayList<>();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getGame() {
        return this.game;
    }

    @Override
    public void setGame(String game) {
        this.game = game;
    }

    @Override
    public List<String> getSettings() {
        return this.settings;
    }

    @Override
    public void setSettings(List<String> settings) {
        this.settings = settings;
    }

    @Override
    public void addSetting(String setting) {
        this.settings.add(setting);
    }
}
