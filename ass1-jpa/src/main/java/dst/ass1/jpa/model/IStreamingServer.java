package dst.ass1.jpa.model;

import java.util.Date;
import java.util.List;

public interface IStreamingServer {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    Date getLastMaintenance();

    void setLastMaintenance(Date lastMaintenance);

    Date getNextMaintenance();

    void setNextMaintenance(Date nextMaintenance);

    List<IStreamingServer> getComposedOf();

    void setComposedOf(List<IStreamingServer> composedOf);

    void addComposedOf(IStreamingServer streamingServer);

    List<IStreamingServer> getPartOf();

    void setPartOf(List<IStreamingServer> partOf);

    void addPartOf(IStreamingServer streamingServer);

    List<IUplink> getUplinks();

    void setUplinks(List<IUplink> uplinks);

    void addUplink(IUplink uplink);

    IModerator getModerator();

    void setModerator(IModerator moderator);

    IMOSPlatform getMOSPlatform();

    void setMOSPlatform(IMOSPlatform platform);

}