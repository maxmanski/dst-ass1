package dst.ass1.jpa.model;

public interface IAddress {

    String getStreet();

    void setStreet(String street);

    String getCity();

    void setCity(String city);

    String getZipCode();

    void setZipCode(String zipCode);

}
