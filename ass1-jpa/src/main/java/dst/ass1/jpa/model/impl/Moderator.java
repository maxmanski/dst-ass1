package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IModerator;
import dst.ass1.jpa.model.IStreamingServer;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 15.03.2017.
 */
@Entity
@NamedQueries(
        @NamedQuery(
                name = Constants.Q_STREAMINGSERVERSOFMODERATOR,
                query = "SELECT m, srv FROM Moderator m JOIN m.advisedStreamingServers srv WHERE m.firstName LIKE 'Alex%'"
        ))
public class Moderator extends Person implements IModerator{

    @OneToMany(targetEntity = StreamingServer.class, mappedBy = "moderator")
    private List<IStreamingServer> advisedStreamingServers;


    public Moderator(){
        this.advisedStreamingServers = new ArrayList<>();
    }

    @Override
    public List<IStreamingServer> getAdvisedStreamingServers() {
        return this.advisedStreamingServers;
    }

    @Override
    public void setAdvisedStreamingServers(List<IStreamingServer> streamingServers) {
        this.advisedStreamingServers = streamingServers;
    }

    @Override
    public void addAdvisedStreamingServers(IStreamingServer streamingServer) {
        this.advisedStreamingServers.add(streamingServer);
    }
}
