package dst.ass1.jpa.model;

import java.util.List;

public interface IMetadata {

    Long getId();

    void setId(Long id);

    String getGame();

    void setGame(String game);

    List<String> getSettings();

    void setSettings(List<String> settings);

    void addSetting(String setting);

}
