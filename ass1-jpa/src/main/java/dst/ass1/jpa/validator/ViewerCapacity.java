package dst.ass1.jpa.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// the target should be a class
@Target({ElementType.TYPE})
// it should be checked at runtime
@Retention(RetentionPolicy.RUNTIME)
// and the validator to use is the self-made ViewerCapacityValidator
@Constraint(validatedBy = ViewerCapacityValidator.class)
public @interface ViewerCapacity{

    // the default error message
    String message() default "ViewerCapacity must be within bounds specified by the annotation!";

    // this can be used for specifying validation groups
    // (can be used to check only subsets of the annotated constraints later)
    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    int min();
    int max();
}
