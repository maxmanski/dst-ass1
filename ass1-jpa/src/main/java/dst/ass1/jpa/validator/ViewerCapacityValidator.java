package dst.ass1.jpa.validator;

import dst.ass1.jpa.model.IUplink;
import org.apache.log4j.Logger;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Max on 18.03.2017.
 */
public class ViewerCapacityValidator implements ConstraintValidator<ViewerCapacity, IUplink>{

    private static final Logger LOGGER = Logger.getLogger(ViewerCapacityValidator.class);

    private int min;
    private int max;

    @Override
    public void initialize(ViewerCapacity viewerCapacity) {
        this.min = viewerCapacity.min();
        this.max = viewerCapacity.max();
    }

    @Override
    public boolean isValid(IUplink iUplink, ConstraintValidatorContext constraintValidatorContext) {
        Integer capacity = iUplink.getViewerCapacity();

        boolean isValid = false;

        if(capacity != null){
            isValid = ((min <= capacity) && (capacity <= max));

            if(!isValid){
                constraintValidatorContext.disableDefaultConstraintViolation();
                constraintValidatorContext.buildConstraintViolationWithTemplate(
                        "Uplink: ViewerCapacity must be within [" + min + ", " + max + "] but is: " + capacity
                ).addConstraintViolation();
            }
        }

        LOGGER.info("Validation of Uplink #" +
                ((iUplink.getId() != null) ? iUplink.getId() : "N/A") +
                " resulted in: " + (isValid ? "VALID" : "INVALID"));

        return isValid;
    }
}
