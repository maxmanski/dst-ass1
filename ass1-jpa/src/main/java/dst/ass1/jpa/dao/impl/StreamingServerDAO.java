package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IStreamingServerDAO;
import dst.ass1.jpa.model.IMOSPlatform;
import dst.ass1.jpa.model.IStreamingServer;
import dst.ass1.jpa.model.impl.MOSPlatform;
import dst.ass1.jpa.model.impl.StreamingServer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class StreamingServerDAO implements IStreamingServerDAO {

    private static final Logger LOGGER = Logger.getLogger(StreamingServerDAO.class);

    private EntityManager em;

    public StreamingServerDAO(EntityManager em){
        this.em = em;
    }


    @Override
    public IStreamingServer findById(Long id) {
        LOGGER.info("Finding StreamingServer by ID" + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IStreamingServer> cq = cb.createQuery(IStreamingServer.class);
        Root<StreamingServer> streamingServer = cq.from(StreamingServer.class);
        cq.select(streamingServer);
        cq.where(cb.equal(streamingServer.get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IStreamingServer> findAll() {
        LOGGER.info("Finding all StreamingServers");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IStreamingServer> cq = cb.createQuery(IStreamingServer.class);
        Root<StreamingServer> streamingServer = cq.from(StreamingServer.class);
        cq.select(streamingServer);
        return em.createQuery(cq).getResultList();
    }
}
