package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IEventStreamingDAO;
import dst.ass1.jpa.model.EventStatus;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.IEventStreaming;
import dst.ass1.jpa.model.impl.EventMaster;
import dst.ass1.jpa.model.impl.EventStreaming;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class EventStreamingDAO implements IEventStreamingDAO{

    private static final Logger LOGGER = Logger.getLogger(EventStreamingDAO.class);
    private EntityManager em;

    public EventStreamingDAO(EntityManager em){
        this.em = em;
    }

    @Override
    public IEventStreaming findById(Long id) {
        LOGGER.info("Finding EventStreaming by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventStreaming> cq = cb.createQuery(IEventStreaming.class);
        Root<EventStreaming> eventStreaming = cq.from(EventStreaming.class);
        cq.select(eventStreaming);
        cq.where(cb.equal(eventStreaming.<Long>get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IEventStreaming> findAll() {
        LOGGER.info("Finding all EventStreamings");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventStreaming> cq = cb.createQuery(IEventStreaming.class);
        Root<EventStreaming> eventStreaming = cq.from(EventStreaming.class);
        cq.select(eventStreaming);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<IEventStreaming> findByStatus(EventStatus status) {
        LOGGER.info("Finding EventStreamings by Status: " + status);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventStreaming> cq = cb.createQuery(IEventStreaming.class);
        Root<EventStreaming> eventStreaming = cq.from(EventStreaming.class);
        cq.select(eventStreaming);
        cq.where(cb.equal(eventStreaming.<EventStatus>get("status"), status));
        return em.createQuery(cq).getResultList();
    }
}
