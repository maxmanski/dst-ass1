package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IEventMasterDAO;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.impl.Event;
import dst.ass1.jpa.model.impl.EventMaster;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class EventMasterDAO implements IEventMasterDAO {

    private static final Logger LOGGER = Logger.getLogger(EventMasterDAO.class);
    private EntityManager em;

    public EventMasterDAO(EntityManager em){
        this.em = em;
    }

    @Override
    public IEventMaster findById(Long id) {
        LOGGER.info("Finding EventMaster by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventMaster> cq = cb.createQuery(IEventMaster.class);
        Root<EventMaster> eventMaster = cq.from(EventMaster.class);
        cq.select(eventMaster);
        cq.where(cb.equal(eventMaster.<Long>get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IEventMaster> findAll() {
        LOGGER.info("Finding all EventMasters");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventMaster> cq = cb.createQuery(IEventMaster.class);
        Root<EventMaster> eventMaster = cq.from(EventMaster.class);
        cq.select(eventMaster);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<IEventMaster> findByName(String eventMasterName) {
        LOGGER.info("Finding EventMasters by Name: " + eventMasterName);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventMaster> cq = cb.createQuery(IEventMaster.class);
        Root<EventMaster> eventMaster = cq.from(EventMaster.class);
        cq.select(eventMaster);
        cq.where(cb.like(eventMaster.get("eventMasterName"), eventMasterName));
        return em.createQuery(cq).getResultList();
    }
}
