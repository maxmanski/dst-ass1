package dst.ass1.jpa.dao;

import java.util.Date;
import java.util.List;

import dst.ass1.jpa.model.IEvent;

public interface IEventDAO extends GenericDAO<IEvent> {
    List<IEvent> findEventsForEventMasterAndGame(String eventMasterName, String game);

    List<IEvent> findEventsForStatusFinishedAndStartAndEnd(Date start, Date end);
}
