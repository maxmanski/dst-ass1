package dst.ass1.jpa.dao;

import javax.persistence.EntityManager;

import dst.ass1.jpa.dao.impl.*;
import dst.ass2.ejb.dao.IAuditLogDAO;
import dst.ass2.ejb.dao.IPriceDAO;
import dst.ass2.ejb.dao.impl.AuditLogDAO;
import dst.ass2.ejb.dao.impl.PriceDAO;
import org.apache.log4j.Logger;
import sun.rmi.runtime.Log;

public class DAOFactory {

    /*
     * HINT: When using the org.hibernate.Session in your DAOs you can extract it from the EntityManager reference with
     * e.g., em.unwrap(org.hibernate.Session.class). Do not store this org.hibernate.Session in your DAOs, but unwrap it
     * every time you actually need it.
     */

    private EntityManager em;

    public DAOFactory(EntityManager em) {
        this.em = em;
    }

    public IMOSPlatformDAO getPlatformDAO() {
        return new MOSPlatformDAO(this.em);
    }

    public IModeratorDAO getModeratorDAO() {
        return new ModeratorDAO(this.em);
    }

    public IStreamingServerDAO getStreamingServerDAO() {
        return new StreamingServerDAO(this.em);
    }

    public IUplinkDAO getUplinkDAO() {
        return new UplinkDAO(this.em);
    }

    public IMetadataDAO getMetadataDAO() {
        return new MetadataDAO(this.em);
    }

    public IEventStreamingDAO getEventStreamingDAO() {
        return new EventStreamingDAO(this.em);
    }

    public IEventDAO getEventDAO() {
        return new EventDAO(this.em);
    }

    public IMembershipDAO getMembershipDAO() {
        return new MembershipDAO(this.em);
    }

    public IEventMasterDAO getEventMasterDAO() {
        return new EventMasterDAO(this.em);
    }

	/*
     * Please note that the following methods are not needed for assignment 1,
	 * but will later be used in assignment 2. Hence, you do not have to
	 * implement it for the first submission.
	 */

    public IAuditLogDAO getAuditLogDAO() {
        return new AuditLogDAO(em);
    }

    public IPriceDAO getPriceDAO() {
        return new PriceDAO(em);
    }

}
