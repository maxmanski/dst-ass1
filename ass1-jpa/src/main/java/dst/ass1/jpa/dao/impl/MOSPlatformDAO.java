package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IMOSPlatformDAO;
import dst.ass1.jpa.model.IMOSPlatform;
import dst.ass1.jpa.model.IModerator;
import dst.ass1.jpa.model.impl.MOSPlatform;
import dst.ass1.jpa.model.impl.Moderator;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class MOSPlatformDAO implements IMOSPlatformDAO{

    private static final Logger LOGGER = Logger.getLogger(MOSPlatformDAO.class);
    private EntityManager em;

    public MOSPlatformDAO(EntityManager em){
        this.em = em;
    }


    @Override
    public IMOSPlatform findById(Long id) {
        LOGGER.info("Finding MOSPlatform by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IMOSPlatform> cq = cb.createQuery(IMOSPlatform.class);
        Root<MOSPlatform> mosPlatform = cq.from(MOSPlatform.class);
        cq.select(mosPlatform);
        cq.where(cb.equal(mosPlatform.get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IMOSPlatform> findAll() {
        LOGGER.info("Finding all MOSPlatforms");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IMOSPlatform> cq = cb.createQuery(IMOSPlatform.class);
        Root<MOSPlatform> mosPlatform = cq.from(MOSPlatform.class);
        cq.select(mosPlatform);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<IMOSPlatform> findByName(String name) {
        LOGGER.info("Finding MOSPlatforms by Name: " + name);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IMOSPlatform> cq = cb.createQuery(IMOSPlatform.class);
        Root<MOSPlatform> mosPlatform = cq.from(MOSPlatform.class);
        cq.select(mosPlatform);
        cq.where(cb.like(mosPlatform.get("name"), name));
        return em.createQuery(cq).getResultList();
    }
}
