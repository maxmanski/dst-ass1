package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IModeratorDAO;
import dst.ass1.jpa.model.IMetadata;
import dst.ass1.jpa.model.IModerator;
import dst.ass1.jpa.model.IStreamingServer;
import dst.ass1.jpa.model.impl.Metadata;
import dst.ass1.jpa.model.impl.Moderator;
import dst.ass1.jpa.model.impl.StreamingServer;
import dst.ass1.jpa.util.Constants;
import org.apache.log4j.Logger;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class ModeratorDAO implements IModeratorDAO {

    private static final Logger LOGGER = Logger.getLogger(ModeratorDAO.class);

    private EntityManager em;

    public ModeratorDAO(EntityManager em){
        this.em = em;
    }

    @Override
    public IModerator findById(Long id) {
        LOGGER.info("Finding Moderator by ID: " + id);

        TypedQuery<IModerator> q = em.unwrap(org.hibernate.Session.class)
                .createQuery("FROM Moderator WHERE id = :id", IModerator.class)
                .setParameter("id", id);

        return q.getSingleResult();
    }

    @Override
    public List<IModerator> findAll() {
        LOGGER.info("Finding all Moderators");

        TypedQuery<IModerator> q = em.unwrap(org.hibernate.Session.class).createQuery("FROM Moderator", IModerator.class);
        return q.getResultList();
    }

    @Override
    public HashMap<IModerator, Date> findNextStreamingServerMaintenanceByModerators() {
        LOGGER.info("Finding Moderators named Alex-something and the dates of their next scheduled maintenance");

        /*
        // OLD SOLUTION, USING CRITERIA API

        // thanks a lot to
        // https://stackoverflow.com/questions/4653070/how-can-i-retrieve-multiple-objects-with-jpa
        // for the hint with tuples
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq = cb.createQuery(Tuple.class);
        Root<Moderator> moderator = cq.from(Moderator.class);
        Join<Moderator, StreamingServer> modSrvJoin = moderator.join("advisedStreamingServers");
        cq.select(cb.tuple(moderator, modSrvJoin)).where(cb.like(moderator.get("firstName"), "Alex%"));

        // get a list of tuples <Moderator, Server> back
        HashMap<IModerator, Date> modMaintenanceMap = new HashMap<>();

        for(Tuple t: em.createQuery(cq).getResultList()){
            IModerator mod = (IModerator) t.get(0);
            IStreamingServer srv = (IStreamingServer) t.get(1);

            // if the server has a next maintenance scheduled...
            if(srv.getNextMaintenance() != null) {

                // if we have no next date yet, any will do
                if (!modMaintenanceMap.containsKey(mod)) {
                    modMaintenanceMap.put(mod, srv.getNextMaintenance());

                    // else, we only take the date if it's in the nearer future than the saved one
                } else if (modMaintenanceMap.get(mod).after(srv.getNextMaintenance())) {
                    modMaintenanceMap.put(mod, srv.getNextMaintenance());
                }
            }
        }
        */

        List<Tuple> modSrvs = em.createNamedQuery(Constants.Q_STREAMINGSERVERSOFMODERATOR, Tuple.class).getResultList();
        HashMap<IModerator, Date> modMaintenanceMap = new HashMap<>();
        for(Tuple t: modSrvs){
            IModerator mod = (IModerator) t.get(0);
            IStreamingServer srv = (IStreamingServer) t.get(1);

            if (srv.getNextMaintenance() != null) {
                if(!modMaintenanceMap.containsKey(mod)){
                    modMaintenanceMap.put(mod, srv.getNextMaintenance());

                } else if (modMaintenanceMap.get(mod).after(srv.getNextMaintenance())) {
                    modMaintenanceMap.put(mod, srv.getNextMaintenance());
                }
            }
        }

        return modMaintenanceMap;
    }
}
