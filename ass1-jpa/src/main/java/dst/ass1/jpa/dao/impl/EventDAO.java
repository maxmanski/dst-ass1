package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IEventDAO;
import dst.ass1.jpa.model.EventStatus;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.model.impl.Event;
import dst.ass1.jpa.model.impl.EventMaster;
import dst.ass1.jpa.model.impl.EventStreaming;
import dst.ass1.jpa.model.impl.Metadata;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class EventDAO implements IEventDAO {

    private static final Logger LOGGER = Logger.getLogger(EventDAO.class);

    private EntityManager em;

    public EventDAO(EntityManager em) {
        this.em = em;
    }

    @Override
    public IEvent findById(Long id) {
        LOGGER.info("Finding Event by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEvent> cq = cb.createQuery(IEvent.class);
        Root<Event> event = cq.from(Event.class);
        cq.select(event);
        cq.where(cb.equal(event.<Long>get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IEvent> findAll() {
        LOGGER.info("Finding all Events");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEvent> cq = cb.createQuery(IEvent.class);
        Root<Event> event = cq.from(Event.class);
        cq.select(event);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<IEvent> findEventsForEventMasterAndGame(String eventMasterName, String game) {

        String logMsg;
        if ((eventMasterName != null) && (game != null)) {
            logMsg = "Finding Events for EventMaster: " + eventMasterName + " and Game: " + game;

        } else if (eventMasterName != null) {
            logMsg = "Finding Events for EventMaster: " + eventMasterName + " (Game N/A)";

        } else if (game != null) {
            logMsg = "Finding Events for Game: " + game + " (EventMaster N/A)";

        } else {
            logMsg = "Finding Events (EventMaster N/A and Game N/A)";

        }
        LOGGER.info(logMsg);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEvent> cq = cb.createQuery(IEvent.class);
        Root<Event> event = cq.from(Event.class);

        // same procedure as in next function
        List<Predicate> predicates = new ArrayList<>(3);

        if (eventMasterName != null) {
            Join<Event, EventMaster> eventMasterJoin = event.join("eventMaster");
            predicates.add(cb.like(eventMasterJoin.get("eventMasterName"), eventMasterName));
        }

        if (game != null) {
            Join<Event, Metadata> eventMetadataJoin = event.join("metadata");
            predicates.add(cb.like(eventMetadataJoin.get("game"), game));
        }

        cq.select(event).where(predicates.toArray(new Predicate[]{}));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<IEvent> findEventsForStatusFinishedAndStartAndEnd(Date start, Date end) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String logMsg;
        if ((start != null) && (end != null)) {
            logMsg = "Finding finished Events for Start: " +
                    formatter.format(start) +
                    " and End: " +
                    formatter.format(end);

        } else if (start != null) {
            logMsg = "Finding finished Events for Start: " +
                    formatter.format(start) +
                    " (End N/A)";

        } else if (end != null) {
            logMsg = "Finding finished Events for End: " +
                    formatter.format(end) +
                    " (Start N/A)";

        } else {
            logMsg = "Finding finished Events (Start N/A and End N/A)";

        }
        LOGGER.info(logMsg);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEvent> cq = cb.createQuery(IEvent.class);
        Root<Event> event = cq.from(Event.class);
        Join<Event, EventStreaming> eventStreamingJoin = event.join("eventStreaming");

        // initialise the predicates for the WHERE clause only with the
        // status == FINISHED
        // thx jassi <3
        List<Predicate> predicates = new ArrayList<>(3);
        predicates.add(cb.equal(eventStreamingJoin.get("status"), EventStatus.FINISHED));

        // if the parameters are not NULL, add them to the WHERE clause
        if (start != null) {
            predicates.add(cb.equal(eventStreamingJoin.<Date>get("start"), start));
        }

        if (end != null) {
            predicates.add(cb.equal(eventStreamingJoin.<Date>get("end"), end));
        }

        cq.select(event).where(predicates.toArray(new Predicate[]{}));

        return em.createQuery(cq).getResultList();
    }
}
