package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IUplinkDAO;
import dst.ass1.jpa.model.IMOSPlatform;
import dst.ass1.jpa.model.IStreamingServer;
import dst.ass1.jpa.model.IUplink;
import dst.ass1.jpa.model.impl.MOSPlatform;
import dst.ass1.jpa.model.impl.StreamingServer;
import dst.ass1.jpa.model.impl.Uplink;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class UplinkDAO implements IUplinkDAO {

    private static final Logger LOGGER = Logger.getLogger(UplinkDAO.class);

    private EntityManager em;

    public UplinkDAO(EntityManager em){
        this.em = em;
    }

    @Override
    public IUplink findById(Long id) {
        LOGGER.info("Finding Uplink by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IUplink> cq = cb.createQuery(IUplink.class);
        Root<Uplink> uplink = cq.from(Uplink.class);
        cq.select(uplink);
        cq.where(cb.equal(uplink.get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IUplink> findAll() {
        LOGGER.info("Finding all Uplinks");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IUplink> cq = cb.createQuery(IUplink.class);
        Root<Uplink> uplink = cq.from(Uplink.class);
        cq.select(uplink);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<IUplink> findByPlatform(IMOSPlatform platform) {
        LOGGER.info("Finding Uplinks by Platform: #" + platform.getId());

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IUplink> cq = cb.createQuery(IUplink.class);
        Root<Uplink> uplink = cq.from(Uplink.class);
        Join<StreamingServer, MOSPlatform> serverPlatformJoin = uplink.join("streamingServer").join("MOSPlatform");
        cq.select(uplink);
        cq.where(cb.equal(serverPlatformJoin.get("id"), platform.getId()));
        return em.createQuery(cq).getResultList();
    }
}
