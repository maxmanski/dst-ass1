package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IMetadataDAO;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.IMetadata;
import dst.ass1.jpa.model.impl.Membership;
import dst.ass1.jpa.model.impl.Metadata;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class MetadataDAO implements IMetadataDAO {

    private static final Logger LOGGER = Logger.getLogger(MetadataDAO.class);

    private EntityManager em;

    public MetadataDAO(EntityManager em){
        this.em = em;
    }


    @Override
    public IMetadata findById(Long id) {
        LOGGER.info("Finding Metadata by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IMetadata> cq = cb.createQuery(IMetadata.class);
        Root<Metadata> metadata = cq.from(Metadata.class);
        cq.select(metadata);
        cq.where(cb.equal(metadata.get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IMetadata> findAll() {
        LOGGER.info("Finding all Metadata");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IMetadata> cq = cb.createQuery(IMetadata.class);
        Root<Metadata> metadata = cq.from(Metadata.class);
        cq.select(metadata);
        return em.createQuery(cq).getResultList();
    }
}
