package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IMembershipDAO;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.IMOSPlatform;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.impl.EventMaster;
import dst.ass1.jpa.model.impl.MOSPlatform;
import dst.ass1.jpa.model.impl.Membership;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Max on 16.03.2017.
 */
public class MembershipDAO implements IMembershipDAO{

    private static final Logger LOGGER = Logger.getLogger(MembershipDAO.class);

    private EntityManager em;

    public MembershipDAO(EntityManager em){
        this.em = em;
    }


    @Override
    public IMembership findById(Long id) {
        LOGGER.info("Finding Membership by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IMembership> cq = cb.createQuery(IMembership.class);
        Root<Membership> eventStreaming = cq.from(Membership.class);
        cq.select(eventStreaming);
        cq.where(cb.equal(eventStreaming.get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IMembership> findAll() {
        LOGGER.info("Finding all Memberships");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IMembership> cq = cb.createQuery(IMembership.class);
        Root<Membership> eventStreaming = cq.from(Membership.class);
        cq.select(eventStreaming);
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<IMembership> findByEventMasterAndPlatform(IEventMaster eventMaster, IMOSPlatform platform) {
        String logMsg;
        if ((eventMaster != null) && (platform != null)) {
            logMsg = "Finding Memberships by EventMaster: #" + eventMaster.getId() + " and MOSPlatform: #" + platform.getId();

        } else if (eventMaster != null) {
            logMsg = "Finding Memberships by EventMaster: #" + eventMaster.getId() + " (MOSPlatform N/A)";

        } else if (platform != null) {
            logMsg = "Finding Memberships by MOSPlatform: #" + platform.getId() + " (EventMaster N/A)";

        } else {
            logMsg = "Finding Memberships (EventMaster N/A and MOSPlatform N/A";

        }

        LOGGER.info(logMsg);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IMembership> cq = cb.createQuery(IMembership.class);
        Root<Membership> eventStreaming = cq.from(Membership.class);
        Join<Membership, EventMaster> membershipMasterJoin = eventStreaming.join("eventMaster");
        Join<Membership, MOSPlatform> membershipPlatformJoin = eventStreaming.join("MOSPlatform");
        cq.select(eventStreaming);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(membershipMasterJoin.get("id"), eventMaster.getId()));
        predicates.add(cb.equal(membershipPlatformJoin.get("id"), platform.getId()));
        cq.where(predicates.toArray(new Predicate[]{}));
        List<IMembership> res = em.createQuery(cq).getResultList();

        return res;
    }
}
