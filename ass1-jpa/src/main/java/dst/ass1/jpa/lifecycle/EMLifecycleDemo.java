package dst.ass1.jpa.lifecycle;

import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import dst.ass1.jpa.model.*;
import dst.ass1.jpa.model.impl.EventMaster;
import org.apache.log4j.Logger;

public class EMLifecycleDemo {

    private static final Logger LOGGER = Logger.getLogger(EMLifecycleDemo.class);

    private EntityManager em;
    private ModelFactory modelFactory;

    public EMLifecycleDemo(EntityManager em, ModelFactory modelFactory) {
        this.em = em;
        this.modelFactory = modelFactory;
    }

    /**
     * Method to illustrate the persistence lifecycle. EntityManager is opened
     * and closed by the Test-Environment! You have to use at least the
     * following methods (listed in alphabetical order) provided by the EntityManager:
     * - clear(..)
     * - flush(..)
     * - merge(..)
     * - persist(..)
     * - remove(..)
     *
     * Keep in mind that this is not necessarily the correct order!
     *
     * @throws NoSuchAlgorithmException
     */
    public void demonstrateEntityMangerLifecycle() throws NoSuchAlgorithmException {

        /*
        * The lifecycle of an entity consists of the four states: New, Managed, Detached and Removed
        *
        * A figure describing the states and state transitions can be seen here:
        * http://www.objectdb.com/java/jpa/persistence/managed
        * */

        IAddress address = this.modelFactory.createAddress();
        IEventMaster master = this.modelFactory.createEventMaster();
        IEvent event = this.modelFactory.createEvent();
        IEventStreaming streaming = this.modelFactory.createEventStreaming();

        address.setCity("Alpbach");
        address.setStreet("Haus Bergwald 439");
        address.setZipCode("6236");

        master.setFirstName("Max");
        master.setLastName("Moser");
        master.setEventMasterName("SerMaximus");
        master.setBankCode("1234");
        master.setAccountNo("1234567890");
        master.setAddress(address);
        master.addEvent(event);

        event.setAttendingViewers(0);
        event.setPaid(true);
        event.setStreamingTime(0);
        event.setEventMaster(master);
        event.setEventStreaming(streaming);

        streaming.setEvent(event);
        streaming.setStart(Calendar.getInstance().getTime());
        streaming.setEnd(Calendar.getInstance().getTime());
        streaming.setStatus(EventStatus.FINISHED);

        LOGGER.info("New Event created, but not yet persisted: #" + event.getId());

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            // we need the EventMaster, because of hibernate...
            em.persist(master);
            em.persist(event);
            LOGGER.info("PERSISTED Event #" + event.getId());
            LOGGER.info("em.contains(event): " + em.contains(event));

            // flush it like a toilet
            em.flush();
            LOGGER.info("FLUSHED EntityManager: Event #" + event.getId());
            LOGGER.info("em.contains(event): " + em.contains(event));

            em.remove(event); // status: removed - deleted from the database
            LOGGER.info("REMOVED Event #" + event.getId());
            LOGGER.info("em.contains(event): " + em.contains(event));

            em.flush();
            LOGGER.info("FLUSHED EntityManager: Event #" + event.getId());
            LOGGER.info("em.contains(event): " + em.contains(event));

            IEvent mergedEvent = em.merge(event);
            LOGGER.info("MERGED Event #" + mergedEvent.getId());
            LOGGER.info("em.contains(mergedEvent): " + em.contains(mergedEvent));
            LOGGER.info("em.contains(event): " + em.contains(event));
            LOGGER.info("mergedEvent equals event: " + event.equals(mergedEvent));

            em.persist(mergedEvent);
            em.flush();
            LOGGER.info("PERSISTED MergedEvent #" + mergedEvent.getId());
            LOGGER.info("em.contains(event): " + em.contains(event));
            LOGGER.info("em.contains(mergedEvent): " + em.contains(mergedEvent));

            // clears the EntityManager context -- all managed Entities should become detached
            // note that we could also em.detach(event) instead of clearing the entire persistence context
            em.clear();
            LOGGER.info("CLEARED the Persistence Context");
            LOGGER.info("em.contains(event): " + em.contains(event));
            LOGGER.info("em.contains(mergedEvent): " + em.contains(mergedEvent));

        } finally {
            tx.rollback();
        }
    }

}
