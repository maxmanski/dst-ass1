package dst.ass1.jpa.listener;

import dst.ass1.jpa.model.impl.Uplink;
import org.apache.log4j.Logger;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Max on 18.03.2017.
 */
public class UplinkListener {

    private static final Logger LOGGER = Logger.getLogger(UplinkListener.class);

    @PrePersist
    public void uplinkPersisted(Uplink uplink){
        LOGGER.info("Persisted new Uplink instance");

        Date now = Calendar.getInstance().getTime();
        uplink.setActivated(now);
        uplink.setLastUpdate(now);
    }

    @PreUpdate
    public void uplinkUpdated(Uplink uplink){
        LOGGER.info("Updated existing Uplink instance: #" + uplink.getId());

        Date now = Calendar.getInstance().getTime();
        uplink.setLastUpdate(now);
    }
}
