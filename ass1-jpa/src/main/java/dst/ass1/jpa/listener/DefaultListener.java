package dst.ass1.jpa.listener;

import javax.persistence.*;
import java.util.HashMap;

public class DefaultListener {

    private static final Object loadLock = new Object();
    private static final Object updateLock = new Object();
    private static final Object removeLock = new Object();
    private static final Object persistLock = new Object();

    private static int loads = 0;
    private static int updates = 0;
    private static int removes = 0;
    private static int persists = 0;
    private static long overallPersistTime = 0;

    private static final HashMap<Object, Long> startTimes = new HashMap<>();

    @PostLoad
    public void postLoad(Object o){
        synchronized (loadLock){
            loads++;
        }
    }

    @PostUpdate
    public void postUpdate(Object o){
        synchronized (updateLock){
            updates++;
        }
    }

    @PostRemove
    public void postRemove(Object o){
        synchronized (removeLock){
            removes++;
        }
    }

    @PrePersist
    public void prePersist(Object o){
        synchronized (persistLock){
            startTimes.put(o, System.currentTimeMillis());
        }
    }

    @PostPersist
    public void postPersist(Object o){
        synchronized (persistLock){

            if (startTimes.containsKey(o)) {
                long diff = System.currentTimeMillis() - startTimes.get(o);
                overallPersistTime += diff;
                startTimes.remove(o);
            }

            persists++;
        }
    }

    public static int getLoadOperations() {
        int res = 0;

        synchronized (loadLock){
            res = loads;
        }

        return res;
    }

    public static int getUpdateOperations() {
        int res = 0;

        synchronized (updateLock){
            res = updates;
        }

        return res;
    }

    public static int getRemoveOperations() {
        int res = 0;

        synchronized (removeLock){
            res = removes;
        }

        return res;
    }

    public static int getPersistOperations() {
        int res = 0;

        synchronized (persistLock){
            res = persists;
        }

        return res;
    }

    public static long getOverallTimeToPersist() {

        long res = 0;

        synchronized (persistLock){
            res = overallPersistTime;
        }

        return res;
    }

    public static double getAverageTimeToPersist() {
        double res = 0.0;

        synchronized (persistLock){
            res = overallPersistTime / ((double) persists);
        }

        return res;
    }

    /**
     * Clears the internal data structures that are used for storing the operations.
     */
    public static void clear() {
        synchronized (loadLock){
            loads = 0;
        }
        synchronized (updateLock){
            updates = 0;
        }
        synchronized (removeLock){
            removes = 0;
        }
        synchronized (persistLock){
            persists = 0;
            overallPersistTime = 0;
        }
    }
}
