package dst.ass2.ejb.dao.impl;

import dst.ass2.ejb.dao.IPriceDAO;
import dst.ass2.ejb.model.IPrice;
import dst.ass2.ejb.model.impl.Price;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 07.05.2017.
 */
public class PriceDAO implements IPriceDAO {
    private static final Logger LOGGER = Logger.getLogger(PriceDAO.class);

    private EntityManager em;

    public PriceDAO(EntityManager em){
        this.em = em;
    }

    @Override
    public IPrice findById(Long id) {
        LOGGER.info("Finding Price by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IPrice> cq = cb.createQuery(IPrice.class);
        Root<Price> price = cq.from(Price.class);
        cq.select(price);
        cq.where(cb.equal(price.get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IPrice> findAll() {
        LOGGER.info("Finding all Prices");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IPrice> cq = cb.createQuery(IPrice.class);
        Root<Price> price = cq.from(Price.class);
        cq.select(price);
        return em.createQuery(cq).getResultList();
    }
}
