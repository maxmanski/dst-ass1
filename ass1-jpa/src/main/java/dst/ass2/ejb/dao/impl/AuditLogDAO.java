package dst.ass2.ejb.dao.impl;

import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.model.impl.Event;
import dst.ass2.ejb.dao.IAuditLogDAO;
import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.model.impl.AuditLog;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 07.05.2017.
 */
public class AuditLogDAO implements IAuditLogDAO {

    private static final Logger LOGGER = Logger.getLogger(PriceDAO.class);

    private EntityManager em;

    public AuditLogDAO(EntityManager em){
        this.em = em;
    }

    @Override
    public IAuditLog findById(Long id) {
        LOGGER.info("Finding AuditLog by ID: " + id);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IAuditLog> cq = cb.createQuery(IAuditLog.class);
        Root<AuditLog> auditLog = cq.from(AuditLog.class);
        cq.select(auditLog);
        cq.where(cb.equal(auditLog.get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    @Override
    public List<IAuditLog> findAll() {
        LOGGER.info("Finding all AuditLogs");

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IAuditLog> cq = cb.createQuery(IAuditLog.class);
        Root<AuditLog> auditLog = cq.from(AuditLog.class);
        cq.select(auditLog);
        return em.createQuery(cq).getResultList();
    }
}
