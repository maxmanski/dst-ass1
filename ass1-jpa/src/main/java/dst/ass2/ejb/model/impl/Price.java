package dst.ass2.ejb.model.impl;

import dst.ass2.ejb.dao.IPriceDAO;
import dst.ass2.ejb.model.IPrice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by Max on 07.05.2017.
 */
@Entity
public class Price implements IPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer nrOfHistoricalEvents;
    private BigDecimal price;

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getNrOfHistoricalEvents() {
        return this.nrOfHistoricalEvents;
    }

    @Override
    public void setNrOfHistoricalEvents(Integer nrOfHistoricalEvents) {
        this.nrOfHistoricalEvents = nrOfHistoricalEvents;
    }

    @Override
    public BigDecimal getPrice() {
        return this.price;
    }

    @Override
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
