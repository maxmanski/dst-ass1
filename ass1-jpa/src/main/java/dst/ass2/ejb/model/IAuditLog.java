package dst.ass2.ejb.model;

import java.util.Date;
import java.util.List;

public interface IAuditLog {

    Long getId();

    void setId(Long id);

    String getMethod();

    void setMethod(String method);

    String getResult();

    void setResult(String result);

    Date getInvocationTime();

    void setInvocationTime(Date invocationTime);

    List<IAuditParameter> getParameters();

    void setParameters(List<IAuditParameter> parameters);

}