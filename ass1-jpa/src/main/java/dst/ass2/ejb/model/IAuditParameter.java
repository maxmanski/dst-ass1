package dst.ass2.ejb.model;

public interface IAuditParameter {

    Long getId();

    void setId(Long id);

    Integer getParameterIndex();

    void setParameterIndex(Integer parameterIndex);

    String getType();

    void setType(String type);

    String getValue();

    void setValue(String value);

    IAuditLog getAuditLog();

    void setAuditLog(IAuditLog auditLog);

}