package dst.ass2.ejb.model;

import java.math.BigDecimal;

public interface IPrice {

    Long getId();

    void setId(Long id);

    Integer getNrOfHistoricalEvents();

    void setNrOfHistoricalEvents(Integer nrOfHistoricalEvents);

    BigDecimal getPrice();

    void setPrice(BigDecimal price);

}