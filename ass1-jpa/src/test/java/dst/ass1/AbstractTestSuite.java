package dst.ass1;

import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import dst.ass1.jpa.util.PersistenceUtil;

public abstract class AbstractTestSuite {

    private static EntityManagerFactory emf;

    @BeforeClass
    public static void setUpClass() {
        emf = getEmf();
    }

    @AfterClass
    public static void tearDownClass() throws SQLException {
        if (emf != null) {
            emf.close();
            emf = null;
        }
    }

    public static EntityManagerFactory getEmf() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory(PersistenceUtil.PERSISTENCE_UNIT_NAME);
        }
        return emf;
    }


}