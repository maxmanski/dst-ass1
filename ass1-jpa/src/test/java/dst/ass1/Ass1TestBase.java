package dst.ass1;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.IUplink;

/**
 * Contains convenience code used in tests for ass1-jpa.
 */
public class Ass1TestBase {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    public Date createDate(int year, int month, int day) {
        return new GregorianCalendar(year, month, day).getTime();
    }

    protected <T, R> List<R> map(List<T> list, Function<T, R> idFn) {
        return list.stream().map(idFn).collect(Collectors.toList());
    }

    protected List<Long> getEventIds(List<IEvent> list) {
        return map(list, IEvent::getId);
    }

    protected List<Long> getEventMasterIds(List<IEventMaster> list) {
        return map(list, IEventMaster::getId);
    }

    protected List<Long> getUplinkIds(List<IUplink> list) {
        return map(list, IUplink::getId);
    }
}
