package dst.ass1.jpa;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.model.IStreamingServer;
import dst.ass1.jpa.util.Constants;
import dst.ass1.jpa.util.DatabaseHelper;
import dst.ass1.jpa.util.test.TestData;

public class Test_1a03 extends AbstractTest {

    @Override
    protected void setUpDatabase() {
        tryInsertTestData();
    }

    @Test
    public void testEntity2NameConstraint() {
        expectedException.expect(PersistenceException.class);
        expectedException.expectCause(isA(ConstraintViolationException.class));

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            // Entity #2
            IStreamingServer ent2_2 = daoFactory.getStreamingServerDAO().findById(testData.entity2_1Id);
            ent2_2.setName(TestData.N_ENT2_2);
            em.persist(ent2_2);
            em.flush();
        } finally {
            tx.rollback();
        }
    }

    @Test
    public void testEntity2NameConstraintJdbc() throws ClassNotFoundException, SQLException {
        assertTrue(DatabaseHelper.isIndex(Constants.T_STREAMINGSERVER, "name", false, em));
    }

}
