package dst.ass1.jpa;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.util.Constants;
import dst.ass1.jpa.util.DatabaseHelper;

public class Test_1b01 extends AbstractTest {

    @Override
    protected void setUpDatabase() {
        tryInsertTestData();
    }

    @Test
    public void testEventMasterAccountNoBankCodeConstraint() throws SQLException, ClassNotFoundException {
        expectedException.expect(PersistenceException.class);
        expectedException.expectCause(isA(ConstraintViolationException.class));

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            IEventMaster ent8 = daoFactory.getEventMasterDAO().findById(testData.entity8_1Id);
            ent8.setAccountNo("account2");
            ent8.setBankCode("bank2");
            em.persist(ent8);
            em.flush();
        } finally {
            tx.rollback();
        }
    }

    @Test
    public void testEventMasterAccountNoBankCodeConstraintJdbc() throws ClassNotFoundException, SQLException {
        assertTrue(DatabaseHelper.isIndex(Constants.T_EVENTMASTER, Constants.M_ACCOUNT, false, em));
        assertTrue(DatabaseHelper.isIndex(Constants.T_EVENTMASTER, Constants.M_BANKCODE, false, em));
        assertTrue(DatabaseHelper.isComposedIndex(Constants.T_EVENTMASTER, Constants.M_ACCOUNT, Constants.M_BANKCODE, em));

        assertTrue(DatabaseHelper.isNullable(Constants.T_EVENTMASTER, Constants.M_ACCOUNT, em));
        assertTrue(DatabaseHelper.isNullable(Constants.T_EVENTMASTER, Constants.M_BANKCODE, em));
    }

}
