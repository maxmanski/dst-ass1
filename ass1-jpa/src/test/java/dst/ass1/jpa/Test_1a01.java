package dst.ass1.jpa;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import javax.persistence.EntityTransaction;

import org.junit.Test;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.dao.IEventDAO;
import dst.ass1.jpa.dao.IEventMasterDAO;
import dst.ass1.jpa.dao.IEventStreamingDAO;
import dst.ass1.jpa.dao.IMOSPlatformDAO;
import dst.ass1.jpa.dao.IMembershipDAO;
import dst.ass1.jpa.dao.IMetadataDAO;
import dst.ass1.jpa.dao.IModeratorDAO;
import dst.ass1.jpa.dao.IStreamingServerDAO;
import dst.ass1.jpa.dao.IUplinkDAO;
import dst.ass1.jpa.model.IAddress;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.model.IEventStreaming;
import dst.ass1.jpa.model.IMOSPlatform;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.IMetadata;
import dst.ass1.jpa.model.IModerator;
import dst.ass1.jpa.model.IStreamingServer;
import dst.ass1.jpa.model.IUplink;
import dst.ass1.jpa.util.ExceptionUtils;

public class Test_1a01 extends AbstractTest {

    @Test
    public void testModelFactory() {
        assertThat(modelFactory.createAddress(), isA(IAddress.class));
        assertThat(modelFactory.createModerator(), isA(IModerator.class));
        assertThat(modelFactory.createStreamingServer(), isA(IStreamingServer.class));
        assertThat(modelFactory.createUplink(), isA(IUplink.class));
        assertThat(modelFactory.createMetadata(), isA(IMetadata.class));
        assertThat(modelFactory.createEventStreaming(), isA(IEventStreaming.class));
        assertThat(modelFactory.createPlatform(), isA(IMOSPlatform.class));
        assertThat(modelFactory.createEvent(), isA(IEvent.class));
        assertThat(modelFactory.createMembership(), isA(IMembership.class));
        assertThat(modelFactory.createEventMaster(), isA(IEventMaster.class));
    }

    @Test
    public void testDAOFactory() {
        DAOFactory daoDummy = new DAOFactory(em);

        assertThat(daoDummy.getModeratorDAO(), isA(IModeratorDAO.class));
        assertThat(daoDummy.getStreamingServerDAO(), isA(IStreamingServerDAO.class));
        assertThat(daoDummy.getUplinkDAO(), isA(IUplinkDAO.class));
        assertThat(daoDummy.getMetadataDAO(), isA(IMetadataDAO.class));
        assertThat(daoDummy.getEventStreamingDAO(), isA(IEventStreamingDAO.class));
        assertThat(daoDummy.getPlatformDAO(), isA(IMOSPlatformDAO.class));
        assertThat(daoDummy.getEventDAO(), isA(IEventDAO.class));
        assertThat(daoDummy.getMembershipDAO(), isA(IMembershipDAO.class));
        assertThat(daoDummy.getEventMasterDAO(), isA(IEventMasterDAO.class));
    }

    @Test
    public void testInsertEntities() {
        try {
            testData.insertTestData();
        } catch (Exception e) {
            EntityTransaction tx = em.getTransaction();

            if (tx.isActive()) {
                try {
                    tx.rollback();
                } catch (Exception e1) {
                }
            }

            fail(ExceptionUtils.getMessage(e));
        }
    }
}
