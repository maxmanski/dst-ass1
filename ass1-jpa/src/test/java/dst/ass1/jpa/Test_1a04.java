package dst.ass1.jpa;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.model.IUplink;
import dst.ass1.jpa.util.Constants;
import dst.ass1.jpa.util.DatabaseHelper;
import dst.ass1.jpa.util.test.TestData;

public class Test_1a04 extends AbstractTest {

    @Override
    protected void setUpDatabase() {
        tryInsertTestData();
    }

    @Test
    public void testEntity3Constraint() {
        expectedException.expect(PersistenceException.class);
        expectedException.expectCause(isA(ConstraintViolationException.class));

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            // Entity #3
            IUplink ent3_1 = daoFactory.getUplinkDAO().findById(testData.entity3_1Id);
            ent3_1.setName(TestData.N_ENT3_2);
            em.persist(ent3_1);
            em.flush();

        } finally {
            tx.rollback();
        }
    }

    @Test
    public void testEntity3ConstraintJdbc() throws ClassNotFoundException, SQLException {
        assertTrue(DatabaseHelper.isIndex(Constants.T_UPLINK, "name", false, em));
    }

}
