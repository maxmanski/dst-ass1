package dst.ass1.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityTransaction;

import org.junit.Test;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.model.EventStatus;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.util.ExceptionUtils;

public class Test_2c02 extends AbstractTest {

    @Override
    protected void setUpDatabase() {
        tryInsertTestData();
    }

    @Test
    public void testFindEventsForStatusFinishedBetweenStartAndFinish1() {
        EntityTransaction tx = null;
        try {
            tx = em.getTransaction();
            tx.begin();

            IEvent ent5 = daoFactory.getEventDAO().findById(testData.entity5_1Id);
            ent5.getEventStreaming().setStatus(EventStatus.SCHEDULED);

            em.persist(ent5);

            List<IEvent> list = daoFactory.getEventDAO().findEventsForStatusFinishedAndStartAndEnd(null, null);
            assertTrue(list == null || list.isEmpty());
        } finally {
            if (tx != null) {
                tx.rollback();
            }
        }
    }

    @Test
    public void testFindEventsForStatusFinishedBetweenStartAndFinish2() {
        List<IEvent> list = daoFactory.getEventDAO().findEventsForStatusFinishedAndStartAndEnd(null, null);
        assertNotNull(list);
        assertEquals(1, list.size());
        assertEquals(testData.entity5_1Id, list.get(0).getId());
    }

    @Test
    public void testFindEventsForStatusFinishedBetweenStartAndFinish3() {
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();

            IEvent ent5 = daoFactory.getEventDAO().findById(testData.entity5_1Id);
            ent5.getEventStreaming().setStatus(EventStatus.FINISHED);
            ent5.getEventStreaming().setStart(createDate(2012, 1, 20));
            ent5.getEventStreaming().setEnd(createDate(2012, 11, 30));

            em.persist(ent5);

            tx.commit();

        } catch (Exception e) {
            fail(ExceptionUtils.getMessage(e));
        }

        List<IEvent> events = findRange(createDate(2012, 1, 1), createDate(2012, 12, 31));
        assertNotNull(events);
        assertEquals(0, events.size());

        events = findRange(createDate(2012, 1, 1), createDate(2012, 10, 1));
        assertNotNull(events);
        assertEquals(0, events.size());

        events = findRange(createDate(2012, 10, 1), createDate(2012, 11, 30));
        assertNotNull(events);
        assertEquals(0, events.size());

        events = findRange(createDate(2012, 1, 20), createDate(2012, 11, 30));
        assertEquals(1, events.size());
        assertEquals(testData.entity5_1Id, events.get(0).getId());

        events = findRange(createDate(2012, 1, 20), null);
        assertEquals(1, events.size());
        assertEquals(testData.entity5_1Id, events.get(0).getId());

        events = findRange(null, createDate(2012, 11, 30));
        assertEquals(1, events.size());
        assertEquals(testData.entity5_1Id, events.get(0).getId());

    }

    private List<IEvent> findRange(Date from, Date to) {
        return daoFactory.getEventDAO().findEventsForStatusFinishedAndStartAndEnd(from, to);
    }

}
