package dst.ass1.jpa;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.EntityManager;

import org.junit.Test;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.model.IModerator;
import dst.ass1.jpa.util.Constants;

public class Test_2b01_2 extends AbstractTest {

    @Test
    public void testNamedQueryExists() {
        try {
            em.createNamedQuery(Constants.Q_STREAMINGSERVERSOFMODERATOR);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testQuery() {
        // retrieve a new/fresh EntityManager, so that no entities can be cached
        EntityManager freshEM = getFreshEntityManager();

        try {
            DAOFactory daoFactory = new DAOFactory(freshEM);
            HashMap<IModerator, Date> map = daoFactory.getModeratorDAO().findNextStreamingServerMaintenanceByModerators();
            assertTrue(map.isEmpty());
        } finally {
            freshEM.clear();
            freshEM.close();
        }
    }
}
