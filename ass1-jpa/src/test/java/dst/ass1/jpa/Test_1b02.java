package dst.ass1.jpa;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;

import org.hibernate.PropertyValueException;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;

import dst.ass1.AbstractTest;
import dst.ass1.jpa.model.IEventMaster;
import dst.ass1.jpa.util.Constants;
import dst.ass1.jpa.util.DatabaseHelper;
import dst.ass1.jpa.util.test.TestData;

public class Test_1b02 extends AbstractTest {

    @Override
    protected void setUpDatabase() {
        tryInsertTestData();
    }

    @Test
    public void testEventMasterEventMasternameConstraint() throws SQLException, ClassNotFoundException {
        expectedException.expect(PersistenceException.class);
        expectedException.expectCause(isA(ConstraintViolationException.class));

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            IEventMaster ent8 = daoFactory.getEventMasterDAO().findById(testData.entity8_1Id);
            ent8.setEventMasterName(TestData.N_ENT8_2);
            em.persist(ent8);
            em.flush();
        } finally {
            tx.rollback();
        }
    }

    @Test
    public void testEventMasterEventMasternameConstraintJdbc() throws ClassNotFoundException, SQLException {
        assertTrue(DatabaseHelper.isIndex(Constants.T_EVENTMASTER, Constants.M_EVENTMASTERNAME, false, em));
    }

    @Test
    public void testEventMasterNotNullConstraint() {
        expectedException.expect(PersistenceException.class);
        expectedException.expectCause(isA(PropertyValueException.class));
        expectedException.expectMessage("not-null property");

        EntityTransaction tx = em.getTransaction();
        tx.begin();
        try {
            IEventMaster ent8 = daoFactory.getEventMasterDAO().findById(testData.entity8_1Id);
            ent8.setEventMasterName(null);
            em.persist(ent8);
            em.flush();
        } finally {
            tx.rollback();
        }
    }

    @Test
    public void testEventMasterNotNullConstraintJdbc() throws ClassNotFoundException, SQLException {
        assertFalse(DatabaseHelper.isNullable(Constants.T_EVENTMASTER, Constants.M_EVENTMASTERNAME, em));
    }
}
