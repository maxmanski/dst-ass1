package dst.ass3.aop.logging;

import dst.ass3.aop.IPluginExecutable;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

@Aspect
public class LoggingAspect {

    private Logger logger = null;

    @Before("execution(void dst.ass3.aop.IPluginExecutable.execute()) && target(executable) && !@annotation(dst.ass3.aop.logging.Invisible)")
    public synchronized void preExecute(IPluginExecutable executable) {
        StringBuilder builder = new StringBuilder();
        builder.append("[Java] Plugin ")
                .append(executable.getClass().getCanonicalName())
                .append(" started to execute");

        this.fetchLogger(executable);
        this.log(builder.toString());
    }

    @After("execution(void dst.ass3.aop.IPluginExecutable.execute()) && target(executable) && !@annotation(dst.ass3.aop.logging.Invisible)")
    public synchronized void postExecute(IPluginExecutable executable) {
        StringBuilder builder = new StringBuilder();
        builder.append("[Java] Plugin ")
                .append(executable.getClass().getCanonicalName())
                .append(" is finished");

        this.fetchLogger(executable);
        this.log(builder.toString());

    }

    private void log(String message) {
        if (this.logger == null) {
            System.out.println(message);
        } else {
            this.logger.log(Level.INFO, message);
        }
    }

    private void fetchLogger(IPluginExecutable executable) {
        this.logger = null;

        try {
            Class<?> classOfExec = executable.getClass();
            for (Field f : classOfExec.getDeclaredFields()) {
                if (this.isSubclassOfLogger(f.getType())) {
                    f.setAccessible(true);
                    this.logger = (Logger) f.get(executable);
                    break;
                }
            }
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }
    }

    private boolean isSubclassOfLogger(Class<?> toCheck) {
        return Logger.class.isAssignableFrom(toCheck);
    }

}
