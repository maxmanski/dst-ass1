package dst.ass3.aop.impl;

import dst.ass3.aop.IPluginExecutable;
import dst.ass3.aop.IPluginExecutor;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Max on 06.06.2017.
 */
public class PluginExecutor implements IPluginExecutor {

    private final List<File> dirsToMonitor;
    private final HashMap<String, Instant> lastUpdated;
    private final ExecutorService threadPool;
    private Timer timer;

    private enum FileStatus {
        NEW, UPDATED, OLD
    }

    public PluginExecutor() {
        this.threadPool = Executors.newCachedThreadPool();
        this.lastUpdated = (new HashMap<String, Instant>());
        this.dirsToMonitor = Collections.synchronizedList(new ArrayList<>());
    }

    @Override
    public synchronized void monitor(File dir) {
        this.dirsToMonitor.add(dir);
    }


    @Override
    public synchronized void stopMonitoring(File dir) {
        this.dirsToMonitor.remove(dir);
    }

    @Override
    public synchronized void start() {
        if(this.timer == null){
            this.timer = new Timer();
            this.timer.schedule(new CheckJob(this), 0);
        }
    }

    @Override
    public synchronized void stop() {
        if(this.timer != null){
            this.timer.cancel();
            this.timer = null;
        }
    }

    private void monitorDirectories(){
        System.out.println("PLUGIN EXECUTOR -- Checking Directories");
        List<File> dirsToMonitor;

        synchronized (this){
            dirsToMonitor = new ArrayList<>(this.dirsToMonitor);
        }

        for(File dir: dirsToMonitor) {
            List<String> oldEntries = new ArrayList<>(this.lastUpdated.keySet());

            // make sure there's no NullPointer
            File[] files = dir.listFiles();
            if(files == null){
                files = new File[0];
            }

            // iterate over each file in the directory and check it
            for (File f : files) {
                String filename = f.getAbsolutePath();

                if (filename.endsWith(".jar")) {
                    switch (this.checkJar(f)) {
                        case NEW:
                        case UPDATED:
                            this.runJar(f);
                            break;
                        case OLD:
                        default:
                            // the file has been there before; there's nothing really to do
                            break;
                    }
                }

                // if the file is still there, don't mark it as "old entry"
                if (oldEntries.contains(filename)) {
                    oldEntries.remove(filename);
                }

            }

            // remove outdated entries from the lastUpdated map
            for (String oldEntry : oldEntries) {
                this.lastUpdated.remove(oldEntry);
            }
        }
    }

    private synchronized FileStatus checkJar(File f) {
        System.out.println("PLUGIN EXECUTOR -- Checking JAR file: " + f.getAbsolutePath());
        String filename = f.getAbsolutePath();
        Instant lastModified = new Date(f.lastModified()).toInstant();

        if (lastUpdated.containsKey(filename)) {

            if (this.lastUpdated.get(filename).isBefore(lastModified)) {
                this.lastUpdated.put(filename, lastModified);
                return FileStatus.UPDATED;
            } else {
                return FileStatus.OLD;
            }

        } else {
            this.lastUpdated.put(filename, lastModified);
            return FileStatus.NEW;
        }
    }

    private void runJar(File f) {
        try {
            // thanks guys
            // @ https://stackoverflow.com/questions/15720822/how-to-get-names-of-classes-inside-a-jar-file

            ZipInputStream zip = new ZipInputStream(new FileInputStream(f));
            for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
                if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
                    // hit a class file!
                    String className = entry.getName().replace('/', '.');
                    className = className.substring(0, className.length() - ".class".length());

                    // further thanks to
                    // https://stackoverflow.com/questions/60764/how-should-i-load-jars-dynamically-at-runtime
                    URL jarURL = f.toURI().toURL();
                    URL[] urls = new URL[]{jarURL};

                    URLClassLoader urlClassLoader = new URLClassLoader(urls, this.getClass().getClassLoader());
                    Class<?> classToLoad = Class.forName(className, true, urlClassLoader);

                    if (this.isPluginExecutable(classToLoad)) {

                        System.out.println("PLUGIN EXECUTOR -- Running JAR file: " + f.getAbsolutePath());

                        this.threadPool.execute(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Object instance = classToLoad.newInstance();
                                    Method invokeMethod = classToLoad.getDeclaredMethod("execute");
                                    invokeMethod.invoke(instance);

                                } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }

        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    private boolean isPluginExecutable(Class<?> c) {
        boolean executable = false;
        for(Class<?> i: c.getInterfaces()){
            if (i.equals(IPluginExecutable.class)) {
                executable = true;
                break;
            }
        }

        return executable;
    }

    // Job for checking Directories
    private class CheckJob extends TimerTask{

        private PluginExecutor exec;

        public CheckJob(PluginExecutor exec){
            this.exec = exec;
        }

        @Override
        public synchronized void run() {
            this.exec.monitorDirectories();
            this.exec.timer = new Timer();
            this.exec.timer.schedule(new CheckJob(this.exec), 500);
        }

    }
}
