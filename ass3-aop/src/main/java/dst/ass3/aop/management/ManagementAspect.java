package dst.ass3.aop.management;

import dst.ass3.aop.IPluginExecutable;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

@Aspect
public class ManagementAspect {

    private HashMap<IPluginExecutable, Timer> timers = new HashMap<>();

    @Before("execution(void dst.ass3.aop.IPluginExecutable.execute()) && target(executable) && @annotation(dst.ass3.aop.management.Timeout)")
    public synchronized void beforeExecute(IPluginExecutable executable) {
        try {
            // fetch the annotation from the class
            Class<?> execClass = executable.getClass();
            Method executeMethod = execClass.getMethod("execute");
            Timeout timeoutAnnotation = executeMethod.getAnnotation(dst.ass3.aop.management.Timeout.class);

            // set up a timer whose job it is to call interrupted() on the executable after timeout ms
            // and put the newly created timer into the executable/timer map
            long timeout = timeoutAnnotation.value();
            InterruptTask interruptTask = new InterruptTask(executable);
            Timer timer = new Timer();
            timers.put(executable, timer);
            timer.schedule(interruptTask, timeout);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @After("execution(void dst.ass3.aop.IPluginExecutable.execute()) && target(executable) && @annotation(dst.ass3.aop.management.Timeout)")
    public synchronized void afterExecute(IPluginExecutable executable) {
        // if the executable finished before its timeout, its timer task is gonna get cancelled
        if(timers.containsKey(executable)){
            Timer t = timers.get(executable);
            t.cancel();
            timers.remove(executable);
        }
    }

    private class InterruptTask extends TimerTask {

        private IPluginExecutable executable;

        public InterruptTask(IPluginExecutable executable){
            this.executable = executable;
        }

        @Override
        public void run() {
            this.executable.interrupted();
        }
    }
}
