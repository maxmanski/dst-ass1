package dst.ass3.dto;

import java.io.Serializable;

public class EventInfoRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long eventInfoId;

    public EventInfoRequest() {
    }

    public EventInfoRequest(Long eventInfoId) {
        super();
        this.eventInfoId = eventInfoId;
    }

    public Long getEventInfoId() {
        return eventInfoId;
    }

    public void setEventInfoId(Long eventInfoId) {
        this.eventInfoId = eventInfoId;
    }

    @Override
    public String toString() {
        return "EventInfoRequest{" +
                "eventInfoId=" + eventInfoId +
                '}';
    }
}
