package dst.ass3.dto;

import java.io.Serializable;

public class AssignEventRequest implements Serializable {

    private static final long serialVersionUID = 843972285375484461L;
    private Long eventId;

    public AssignEventRequest() {
    }

    public AssignEventRequest(Long eventId) {
        this.eventId = eventId;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return "AssignEventRequest{" +
                "eventId=" + eventId +
                '}';
    }
}
