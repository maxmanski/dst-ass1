package dst.ass3.dto;

import java.io.Serializable;

import dst.ass3.model.EventType;
import dst.ass3.model.IEventInfo;
import dst.ass3.model.LifecycleState;

public class EventInfoDTO implements Serializable, IEventInfo {

    private static final long serialVersionUID = 4134104076758220138L;
    private Long id;
    private Long eventId;
    private Long timestamp;
    private LifecycleState state;
    private String classifiedBy;
    private EventType type;

    public EventInfoDTO() {
    }

    public EventInfoDTO(Long id, Long eventId, LifecycleState state, String classifiedBy, EventType type) {
        super();
        this.id = id;
        this.eventId = eventId;
        this.state = state;
        this.classifiedBy = classifiedBy;
        this.type = type;
    }

    public EventInfoDTO(Long id, Long eventId, Long timestamp, LifecycleState state, String classifiedBy, EventType type) {
        this.id = id;
        this.eventId = eventId;
        this.timestamp = timestamp;
        this.state = state;
        this.classifiedBy = classifiedBy;
        this.type = type;
    }

    public EventInfoDTO(IEventInfo event) {
        super();
        this.id = event.getId();
        this.eventId = event.getEventId();
        this.timestamp = event.getTimestamp();
        this.state = event.getState();
        this.classifiedBy = event.getClassifiedBy();
        this.type = event.getType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    @Override
    public Long getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public LifecycleState getState() {
        return state;
    }

    public void setState(LifecycleState state) {
        this.state = state;
    }

    public String getClassifiedBy() {
        return classifiedBy;
    }

    public void setClassifiedBy(String classifiedBy) {
        this.classifiedBy = classifiedBy;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "EventInfoDTO{" +
                "id=" + id +
                ", eventId=" + eventId +
                ", timestamp=" + timestamp +
                ", state=" + state +
                ", classifiedBy='" + classifiedBy + '\'' +
                ", type=" + type +
                '}';
    }
}
