package dst.ass2.di.impl;

import dst.ass2.di.IInjectionController;
import dst.ass2.di.InjectionException;
import dst.ass2.di.annotation.Component;
import dst.ass2.di.annotation.ComponentId;
import dst.ass2.di.annotation.Inject;
import dst.ass2.di.model.ScopeType;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Max on 08.05.2017.
 */
public class InjectionController implements IInjectionController {

    // logger not in classpath, gonna use sysouts instead...
    //private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(InjectionController.class);

    private HashMap<String, Object> singletonInstances = new HashMap<>();
    private AtomicLong id = new AtomicLong(0);

    @Override
    public void initialize(Object obj) throws InjectionException {
        if(obj == null){
            throw new InjectionException("Cannot inject to NULL");
        }

        System.out.println("Initializing Object of class: " + obj.getClass().getSimpleName());
        Component component = null;

        Class<?> objClass = obj.getClass();
        for(Annotation annotation: objClass.getAnnotations()){
            if(annotation instanceof Component){
                component = (Component) annotation;
            }
        }

        if(component == null){
            throw new InjectionException("Object is not annotated as Component!");
        }

        switch (component.scope()){
            case SINGLETON:
                // if we have a singleton, check if we already have an instance
                if(this.singletonInstances.containsKey(obj.getClass().getCanonicalName())){
                    throw new InjectionException("Cannot instantiate Singleton more than once!");
                }else{
                    this.singletonInstances.put(obj.getClass().getCanonicalName(), obj);
                }

            case PROTOTYPE:
                List<Field> idFields = new ArrayList<>();

                for(Field f: this.getAllFields(objClass)){
                    // check all fields of the object

                    for(Annotation annotation: f.getAnnotations()){
                        // for every annotation of the field, let's check if it's an ID...
                        if(annotation instanceof ComponentId){
                            if(!f.getType().equals(Long.class)){
                                throw new InjectionException("Cannot instantiate: ComponentId is not a Long!");
                            }

                            idFields.add(f);
                        }

                        // ... or an Inject
                        else if (annotation instanceof Inject) {
                            Inject inject = (Inject) annotation;

                            try {
                                // determine the required type and instantiate it
                                Class<?> classToInject = inject.specificType();
                                if(classToInject.equals(Void.class)){
                                    classToInject = f.getType();
                                }
                                System.out.println("Instantiating field of type: " + classToInject.getSimpleName());

                                Object newInstance = null;
                                try {
                                    // first off we'll try to get a SINGLETON instance from the class
                                    // (if it's not a SINGLETON, it's going to throw an InjectionException)
                                    newInstance = this.getSingletonInstance(classToInject);

                                }catch (InjectionException e){

                                    // if it wasn't a SINGLETON, we just try to create a new instance of it...
                                    newInstance = classToInject.newInstance();
                                    try {
                                        // and let's try to initialize it...
                                        // NOTE: only Components can be injected!
                                        this.initialize(newInstance);

                                    }catch (Exception ex){
                                        // if that fails, we just reset it to NULL
                                        // (or throw an InjectionException if it was a required field)
                                        if(inject.required()){
                                            throw new InjectionException(ex);
                                        }
                                        newInstance = null;
                                    }
                                }

                                // set the field for the given object to the new instance
                                f.setAccessible(true);
                                f.set(obj, newInstance);

                            } catch (IllegalArgumentException | IllegalAccessException | InstantiationException e) {
                                if(inject.required()){
                                    throw new InjectionException(e);
                                }
                            }
                        }
                    }
                }

                if(idFields.isEmpty()){
                    throw new InjectionException("No ComponentId field given!");
                }else{

                    long _id = id.getAndAdd(1);
                    System.out.println("Setting ID to: " + _id);
                    try {
                        for (Field f : idFields){
                            f.setAccessible(true);
                            f.set(obj, _id);
                        }
                    } catch (Exception e) {
                        throw new InjectionException(e);
                    }
                }
                break;


            default:
                throw new InjectionException("Unknown ScopeType!");
        }
    }

    @Override
    public <T> T getSingletonInstance(Class<T> clazz) throws InjectionException {

        System.out.println("Fetching Singleton instance for class: " + clazz.getSimpleName());
        // if we already have the Singleton instance for this class...
        if(this.singletonInstances.containsKey(clazz.getCanonicalName())){
            return (T)this.singletonInstances.get(clazz.getCanonicalName());
        }

        // if we're not handling a SINGLETON, throw an error
        for(Annotation annotation: clazz.getAnnotations()){
            if(annotation instanceof Component){
                if(!((Component)annotation).scope().equals(ScopeType.SINGLETON)){
                    throw new InjectionException("Cannot get Singleton Instance of PROTOTYPE!");
                }
            }
        }

        try {
            // try to instantiate the singleton
            T instance;

            instance = clazz.newInstance();
            this.initialize(instance);

            this.singletonInstances.put(clazz.getCanonicalName(), instance);
            return instance;

        } catch (InstantiationException | IllegalAccessException ex){
            throw new InjectionException(ex);
        }
    }

    private List<Field> getAllFields(Class<?> type){
        return this.getAllFields(new ArrayList<>(), type);
    }

    // credits to:
    // https://stackoverflow.com/questions/1042798/retrieving-the-inherited-attribute-names-values-using-java-reflection
    // for helping me find all those fields (non-public and inherited)
    private List<Field> getAllFields(List<Field> fields, Class<?> type){
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if(type.getSuperclass() != null){
            getAllFields(fields, type.getSuperclass());
        }

        return fields;
    }
}
