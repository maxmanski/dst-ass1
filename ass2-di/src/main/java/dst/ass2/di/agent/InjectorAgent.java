package dst.ass2.di.agent;

import dst.ass2.di.annotation.Component;
import javassist.*;

import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

public class InjectorAgent implements ClassFileTransformer {

    @Override
    public byte[] transform(ClassLoader loader, String className,
            Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
            byte[] classfileBuffer) throws IllegalClassFormatException {

        byte[] byteCode = classfileBuffer;

        try {
            ClassPool cp = ClassPool.getDefault();

            // since for some reason the packages/class name are separated via slash, replace them with dots
            // should throw some NotFoundExceptions for inner classes (asdf.qwer.Class.$InnerClass, ...)
            CtClass cc = cp.get(className.replaceAll("/", "."));

            // check if the current class is a component...
            boolean isComponent = false;
            for(Object o: cc.getAnnotations()){
                if(o instanceof Component){
                    isComponent = true;
                }
            }

            if(!isComponent){
                return classfileBuffer;
            }
            System.out.println("Instrumenting class: " + cc.getSimpleName());

            // if it is, add to each constructor the initialize(this) statement from the TransparentInstance
            for(CtConstructor ctor: cc.getDeclaredConstructors()){
                String functionToAdd = "dst.ass2.di.InjectionControllerFactory.getTransparentInstance().initialize(this);";
                ctor.insertAfter(functionToAdd);
            }

            byteCode = cc.toBytecode();

        } catch (CannotCompileException | NotFoundException | ClassNotFoundException | IOException ignored){
        }

        return byteCode;
    }

    public static void premain(String agentArgs, Instrumentation instrumentation){
        try {
            System.out.println("Installing our Custom Transformer");
            // before executing the main method, add our injector agent
            instrumentation.addTransformer(new InjectorAgent());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
