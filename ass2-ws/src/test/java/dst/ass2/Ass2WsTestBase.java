package dst.ass2;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.Scanner;

import javax.annotation.Resource;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.apache.openejb.junit.OpenEjbRunner;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(OpenEjbRunner.class)
public class Ass2WsTestBase {

    protected static EJBContainer container;
    protected static Context context;

    @PersistenceContext
    protected EntityManager em;

    @Resource
    protected UserTransaction userTransaction;

    public static Properties getContainerProperties() {
        Properties properties = new Properties();

        // TomEE settings
        // System.setProperty("tomee.jpa.factory.lazy", "true");
        properties.put("tomee.jpa.cdi", "false"); // http://tomee-openejb.979440.n4.nabble.com/CDI-issues-tomee-7-0-2-td4680584.html
        properties.put("openejb.validation.output.level", "VERBOSE");
        properties.put("openejb.jpa.auto-scan", "true");
        properties.put("openejb.embedded.initialcontext.close", "DESTROY");
        properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.core.OpenEJBInitialContextFactory");

        properties.put("openejb.embedded.remotable", "true");

        // new data source (will be used by hibernate persistence unit, any previously configured one will be overwritten)
        properties.put("dst_ds", "new://Resource?type=DataSource");
        properties.put("dst_ds.JdbcDriver", "org.h2.Driver");
        properties.put("dst_ds.JdbcUrl", "jdbc:h2:/tmp/database/dst;AUTO_SERVER=TRUE;MVCC=true");
        properties.put("dst_ds.UserName", "");
        properties.put("dst_ds.Password", "");
        properties.put("dst_ds.JtaManaged", "true");
        properties.put("dst_ds.transactional", "true");

        // hibernate hacks
        properties.put("hibernate.enable_lazy_load_no_trans", "true");

        return properties;
    }

    @BeforeClass
    public static void startContainer() throws Exception {
        container = EJBContainer.createEJBContainer(getContainerProperties());
        context = container.getContext();
    }

    @AfterClass
    public static void stopContainer() {
        if (context != null) {
            try {
                context.close();
            } catch (NamingException e) {
            }
        }

        if (container != null) {
            container.close();
        }
    }

    @Before
    public void inject() throws NamingException {
        if (container != null) {
            container.getContext().bind("inject", this);
        }
    }

    protected String readURL(String urlString) throws UncheckedIOException, IllegalArgumentException {
        try {
            return readURL(new URL(urlString));
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private String readURL(URL url) throws UncheckedIOException {
        try (Scanner in = new Scanner(url.openStream())) {
            StringBuilder str = new StringBuilder();
            while (in.hasNext()) {
                str.append(in.nextLine()).append("\n");
            }
            return str.toString();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
