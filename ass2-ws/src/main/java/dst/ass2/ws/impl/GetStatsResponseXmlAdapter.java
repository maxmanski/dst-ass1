package dst.ass2.ws.impl;

import dst.ass2.ws.IGetStatsResponse;
import dst.ass2.ws.dto.StatisticsDTO;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by Max on 09.05.2017.
 */
public class GetStatsResponseXmlAdapter extends XmlAdapter<StatisticsDTO, IGetStatsResponse> {

    @Override
    public IGetStatsResponse unmarshal(StatisticsDTO v) throws Exception {
        IGetStatsResponse ret = new GetStatsResponse();
        ret.setStatistics(v);
        return ret;
    }

    @Override
    public StatisticsDTO marshal(IGetStatsResponse v) throws Exception {
        return v.getStatistics();
    }
}
