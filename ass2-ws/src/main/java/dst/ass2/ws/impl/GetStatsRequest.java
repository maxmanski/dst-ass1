package dst.ass2.ws.impl;

import dst.ass2.ws.IGetStatsRequest;

/**
 * Created by Max on 08.05.2017.
 */
public class GetStatsRequest implements IGetStatsRequest {

    private int maxStreamings;

    @Override
    public int getMaxStreamings() {
        return this.maxStreamings;
    }

    @Override
    public void setMaxStreamings(int maxStreamings) {
        this.maxStreamings = maxStreamings;
    }
}
