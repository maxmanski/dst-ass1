package dst.ass2.ws.impl;

import dst.ass2.ws.IGetStatsRequest;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by Max on 09.05.2017.
 */
public class GetStatsRequestXmlAdapter extends XmlAdapter<Integer, IGetStatsRequest> {

    @Override
    public IGetStatsRequest unmarshal(Integer v) throws Exception {
        IGetStatsRequest ret = new GetStatsRequest();
        ret.setMaxStreamings(v);
        return ret;
    }

    @Override
    public Integer marshal(IGetStatsRequest v) throws Exception {
        return v.getMaxStreamings();
    }
}
