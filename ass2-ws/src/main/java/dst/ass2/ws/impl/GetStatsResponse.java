package dst.ass2.ws.impl;

import dst.ass2.ws.IGetStatsResponse;
import dst.ass2.ws.dto.StatisticsDTO;

/**
 * Created by Max on 08.05.2017.
 */
public class GetStatsResponse implements IGetStatsResponse {

    private StatisticsDTO statisticsDTO;

    @Override
    public StatisticsDTO getStatistics() {
        return this.statisticsDTO;
    }

    public void setStatistics(StatisticsDTO statisticsDTO) {
        this.statisticsDTO = statisticsDTO;
    }
}
