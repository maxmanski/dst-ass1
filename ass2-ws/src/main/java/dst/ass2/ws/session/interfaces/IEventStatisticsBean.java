package dst.ass2.ws.session.interfaces;

import dst.ass2.ws.Constants;
import dst.ass2.ws.IGetStatsRequest;
import dst.ass2.ws.IGetStatsResponse;
import dst.ass2.ws.impl.GetStatsRequestXmlAdapter;
import dst.ass2.ws.impl.GetStatsResponseXmlAdapter;
import dst.ass2.ws.session.exception.WebServiceException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.soap.Addressing;

/**
 * This is the interface of the statistics Web Service.
 */
@Addressing
@WebService(portName = Constants.PORT_NAME,
        name = Constants.NAME,
        serviceName = Constants.SERVICE_NAME,
        targetNamespace = Constants.NAMESPACE)
public interface IEventStatisticsBean {

    /**
     * Get statistics for a given platform.
     *
     * @param request The request object with parameters
     * @param platformName The name of the platform
     * @return statistics for the platform with the specified name.
     */
    @WebResult(name = "IGetStatsResponse", header = false, partName = "IGetStatsResponse")
    @Action(input = "http://localhost:4204/ws/StatisticsService/service/GetStatsRequest",
            output = "http://localhost:4204/ws/StatisticsService/service/GetStatsResponse",
            fault = @FaultAction(className = WebServiceException.class))
    @WebMethod(operationName = Constants.OP_GET_STATS)
    @XmlJavaTypeAdapter(GetStatsResponseXmlAdapter.class) IGetStatsResponse getStatisticsForPlatform(
            @WebParam @XmlJavaTypeAdapter(GetStatsRequestXmlAdapter.class)
            IGetStatsRequest request,
            @WebParam(header = true, partName = "platformName")
            String platformName) throws WebServiceException;

}
