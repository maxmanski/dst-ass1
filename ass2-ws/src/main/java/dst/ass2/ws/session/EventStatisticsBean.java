package dst.ass2.ws.session;

import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.dao.IMOSPlatformDAO;
import dst.ass1.jpa.model.*;
import dst.ass2.ws.Constants;
import dst.ass2.ws.IGetStatsRequest;
import dst.ass2.ws.IGetStatsResponse;
import dst.ass2.ws.dto.StatisticsDTO;
import dst.ass2.ws.impl.GetStatsRequestXmlAdapter;
import dst.ass2.ws.impl.GetStatsResponse;
import dst.ass2.ws.impl.GetStatsResponseXmlAdapter;
import dst.ass2.ws.session.exception.WebServiceException;
import dst.ass2.ws.session.interfaces.IEventStatisticsBean;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.soap.Addressing;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Stateless
@Addressing
@WebService(portName = Constants.PORT_NAME,
        name = Constants.NAME,
        serviceName = Constants.SERVICE_NAME,
        targetNamespace = Constants.NAMESPACE)
public class EventStatisticsBean implements IEventStatisticsBean {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(EventStatisticsBean.class);

    @PersistenceContext
    private EntityManager em;

    private IMOSPlatformDAO imosPlatformDAO;

    @PostConstruct
    public void setUp(){
        DAOFactory factory = new DAOFactory(em);
        this.imosPlatformDAO = factory.getPlatformDAO();
    }

    @WebResult(name = "IGetStatsResponse", header = false, partName = "IGetStatsResponse")
    @Action(input = "http://localhost:4204/ws/StatisticsService/service/GetStatsRequest",
            output = "http://localhost:4204/ws/StatisticsService/service/GetStatsResponse",
            fault = @FaultAction(className = WebServiceException.class))
    @WebMethod(operationName = Constants.OP_GET_STATS)
    public @XmlJavaTypeAdapter(GetStatsResponseXmlAdapter.class) IGetStatsResponse getStatisticsForPlatform(
            @WebParam @XmlJavaTypeAdapter(GetStatsRequestXmlAdapter.class)
            IGetStatsRequest request,
            @WebParam(header = true, partName = "platformName")
            String name) throws WebServiceException {

        LOGGER.info("Handling the Statistics WS call");
        IGetStatsResponse response = new GetStatsResponse();
        StatisticsDTO statistics = new StatisticsDTO();
        statistics.setStreamings(new ArrayList<>());
        int maxStreamings = request.getMaxStreamings();

        statistics.setName(name);
        IMOSPlatform platform = null;
        for(IMOSPlatform p: this.imosPlatformDAO.findByName(name)){
            platform = p;
        }

        if(platform == null){
            throw new WebServiceException("Could not find a MOSPlatform with the specified name");
        }

        List<IEventStreaming> streamings = new ArrayList<>();

        for(IStreamingServer streamingServer: platform.getStreamingServers()){
            for(IUplink uplink: streamingServer.getUplinks()){
                for(IEventStreaming streaming: uplink.getEventStreamings()){
                    if((streaming.getStatus() == null) || (streaming.getStatus().equals(EventStatus.FINISHED))){
                        if(statistics.getStreamings().size() < maxStreamings){
                            statistics.addStreaming(streaming);
                        }
                    }
                }
            }
        }

        response.setStatistics(statistics);
        return response;
    }

}
