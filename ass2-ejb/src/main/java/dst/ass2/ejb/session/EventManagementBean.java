package dst.ass2.ejb.session;

import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.dao.IEventMasterDAO;
import dst.ass1.jpa.dao.IMOSPlatformDAO;
import dst.ass1.jpa.dao.IUplinkDAO;
import dst.ass1.jpa.model.*;
import dst.ass2.ejb.dto.AssignmentDTO;
import dst.ass2.ejb.interceptor.AuditInterceptor;
import dst.ass2.ejb.session.exception.AssignmentException;
import dst.ass2.ejb.session.interfaces.IEventManagementBean;

import javax.annotation.PostConstruct;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Stateful
@Interceptors(AuditInterceptor.class)
public class EventManagementBean implements IEventManagementBean {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(EventManagementBean.class);
    private static final Object submissionLock = new Object();

    private IEventMaster loggedInMaster;
    private List<AssignmentDTO> assignmentCache;
    private DAOFactory daoFactory;
    private IEventMasterDAO eventMasterDAO;
    private IUplinkDAO uplinkDAO;
    private IMOSPlatformDAO platformDAO;
    private ModelFactory modelFactory;

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void setUp(){
        LOGGER.info("Setting up new EventManagementBean");
        this.modelFactory = new ModelFactory();
        this.loggedInMaster = null;
        this.assignmentCache = new ArrayList<>();
        this.daoFactory = new DAOFactory(em);
        this.uplinkDAO = this.daoFactory.getUplinkDAO();
        this.platformDAO = this.daoFactory.getPlatformDAO();
        this.eventMasterDAO = this.daoFactory.getEventMasterDAO();
    }

    @Override
    public void login(String eventMasterName, String password)
            throws AssignmentException {

        List<IEventMaster> eventMasters = this.eventMasterDAO.findAll();
        boolean loginSuccess = false;

        try {
            for(IEventMaster eventMaster: eventMasters){
                if (eventMaster.getEventMasterName().equals(eventMasterName)) {

                    // calculate the md5 hash of the specified password
                    MessageDigest md5 = MessageDigest.getInstance("MD5");
                    md5.update(password.getBytes());
                    byte[] pwHash = md5.digest();

                    if (Arrays.equals(eventMaster.getPassword(), pwHash)) {

                        this.loggedInMaster = eventMaster;
                        loginSuccess = true;
                        break;
                    }
                }
            }
        }catch(NoSuchAlgorithmException e){
            throw new AssignmentException(e);
        }

        LOGGER.info("Login of EventMaster: " + eventMasterName + " was " + (loginSuccess ? "successful" : "unsuccessful"));

        if (!loginSuccess) {
            throw new AssignmentException("EventMaster with name \"" + eventMasterName + "\" does not exist or password does not match!");
        }

    }

    @Override
    @Remove(retainIfException = true)
    public void submitAssignments() throws AssignmentException {
        synchronized (EventManagementBean.submissionLock) {
            if (this.loggedInMaster == null) {
                throw new AssignmentException("EventMaster must be logged in to submit Assignments!");
            }

            LOGGER.info("Trying to submit Assignments for EventMaster: " + this.loggedInMaster.getEventMasterName());
            List<IMetadata> metadataToPersist = new ArrayList<>();
            List<IEventStreaming> eventStreamingsToPersist = new ArrayList<>();
            List<IEvent> eventsToPersist = new ArrayList<>();

            // try to set up all events/streamings to persist and check if they are still valid
            for (AssignmentDTO ass : this.assignmentCache) {
                IMetadata metadata = this.modelFactory.createMetadata();
                metadata.setSettings(ass.getSettings());
                metadata.setGame(ass.getGame());

                IEventStreaming eventStreaming = this.modelFactory.createEventStreaming();
                eventStreaming.setStatus(EventStatus.SCHEDULED);
                eventStreaming.setStart(Date.from(Instant.now()));

                IEvent event = this.modelFactory.createEvent();
                event.setPaid(false);
                event.setEventMaster(this.loggedInMaster);
                event.setMetadata(metadata);
                event.setEventStreaming(eventStreaming);

                eventStreaming.setEvent(event);

                for (Long uplinkId : ass.getUplinkIds()) {
                    // for each chosen uplink, check if they're still free
                    IUplink uplink = this.uplinkDAO.findById(uplinkId);

                    boolean isFree = true;
                    for (IEventStreaming streaming : uplink.getEventStreamings()) {
                        if (streaming.getStatus().equals(EventStatus.SCHEDULED) ||
                                streaming.getStatus().equals(EventStatus.STREAMING)) {

                            isFree = false;
                            break;
                        }
                    }

                    if (isFree) {
                        eventStreaming.addUplinks(uplink);
                    } else {
                        throw new AssignmentException("Cannot create EventStreaming - one or more Uplinks are no longer available!");
                    }
                }

                // if we have come this far, all is good to go for this event:
                // add it to the list of events to persist
                eventsToPersist.add(event);
                eventStreamingsToPersist.add(eventStreaming);
                metadataToPersist.add(metadata);
            }

            // if we have come this far without raising an exception, we can be sure
            // that every Event/Streaming is still good to go
            for (int i = 0; i < eventsToPersist.size(); i++) {
                em.persist(metadataToPersist.get(i));
                em.persist(eventsToPersist.get(i));
                em.persist(eventStreamingsToPersist.get(i));
            }

            em.flush();
            LOGGER.info("Submitting Events done for EventMaster: " + this.loggedInMaster.getEventMasterName());
        }
    }

    @Override
    public void addEvent(Long platformId, Integer numViewers, String game,
            List<String> settings) throws AssignmentException {

        List<Long> uplinkIds = new ArrayList<>();
        IMOSPlatform platform;
        List<IUplink> uplinks;

        try {
            platform = this.platformDAO.findById(platformId);
            uplinks = this.uplinkDAO.findByPlatform(platform);
        }catch (NoResultException nre){
            throw new AssignmentException("Could not find MOSPlatform for ID or any uplinks for this platform", nre);
        }

        int capacity = 0;
        for(IUplink uplink: uplinks){
            if(capacity >= numViewers){
                // the allocated capacity exceeds the requirements; we can cancel the search now
                break;
            }

            boolean free = true;
            for(IEventStreaming streaming: uplink.getEventStreamings()){

                if(streaming.getStatus().equals(EventStatus.SCHEDULED) ||
                        streaming.getStatus().equals(EventStatus.STREAMING)){

                    free = false;
                    break;
                }
            }

            if(free){
                uplinkIds.add(uplink.getId());
                capacity += uplink.getViewerCapacity();
            }
        }

        if(capacity < numViewers){
            throw new AssignmentException("We require more Viewer Capacity");
        }

        // actually you should perhaps take the cache into consideration, but the
        // assignment said it's only supposed to be basic and not complex

        AssignmentDTO assignment = new AssignmentDTO(platformId, numViewers, game, settings, uplinkIds);
        LOGGER.info("Added Assignment: " + assignment.toString());
        this.assignmentCache.add(assignment);
    }

    @Override
    public List<AssignmentDTO> getCache() {
        return this.assignmentCache;
    }

    @Override
    public void removeEventsForPlatform(Long id) {
        // lambdas, boy!
        LOGGER.info("Removing Events for Platform #" + id);
        this.assignmentCache.removeIf(ass -> ass.getPlatformId().equals(id));
    }

}
