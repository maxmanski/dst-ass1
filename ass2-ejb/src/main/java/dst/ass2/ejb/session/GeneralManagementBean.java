package dst.ass2.ejb.session;

import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.model.*;
import dst.ass2.ejb.dao.IAuditLogDAO;
import dst.ass2.ejb.dto.AuditLogDTO;
import dst.ass2.ejb.dto.AuditParameterDTO;
import dst.ass2.ejb.dto.BillDTO;
import dst.ass2.ejb.management.interfaces.IPriceManagementBean;
import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.model.IAuditParameter;
import dst.ass2.ejb.session.exception.AssignmentException;
import dst.ass2.ejb.session.interfaces.IGeneralManagementBean;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@Stateless
@Remote
public class GeneralManagementBean implements IGeneralManagementBean {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(GeneralManagementBean.class);

    @EJB
    private IPriceManagementBean priceManagementBean;

    private IAuditLogDAO auditLogDAO;
    private ModelFactory modelFactory;

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void setUp(){
        LOGGER.info("Setting up new GeneralManagementBean");
        this.auditLogDAO = new DAOFactory(em).getAuditLogDAO();
        this.modelFactory = new ModelFactory();
    }

    @Override
    public void addPrice(Integer nrOfHistoricalEvents, BigDecimal price) {
        priceManagementBean.setPrice(nrOfHistoricalEvents, price);
    }

    @Override
    @Asynchronous
    public Future<BillDTO> getBillForEventMaster(String eventMasterName)
            throws Exception {

        BillDTO bill = new BillDTO();
        List<BillDTO.BillPerEventMaster> billPerEventMaster = new ArrayList<>();
        BigDecimal price = new BigDecimal(0.0);
        IEventMaster master = null;

        // find the eventMaster with specified name
        // (could be done via other eventmasterDAO method too, but whatever)
        for(IEventMaster eventMaster: new DAOFactory(em).getEventMasterDAO().findAll()){
            if(eventMaster.getEventMasterName().equals(eventMasterName)){
                master = eventMaster;
                break;
            }
        }

        if (master == null) {
            throw new AssignmentException("No such EventMaster");
        }
        LOGGER.info("Creating bill for EventMaster: " + eventMasterName);

        bill.setEventMasterName(master.getEventMasterName());

        // set up lists for paid and unpaid events
        List<IEvent> paidEvents = new ArrayList<>(master.getEvents());
        List<IEvent> unpaidEvents = new ArrayList<>(paidEvents);
        paidEvents.removeIf(event -> !event.isPaid());
        unpaidEvents.removeIf(IEvent::isPaid);

        int paidCount = paidEvents.size();

        for(IEvent event: unpaidEvents){
            IMOSPlatform platform = null;
            IMembership membership = null;
            double discount = 0.0;

            // assumption: every event is streamed on exactly one platform (not across multiple platforms)
            // and an eventMaster has exactly one membership at each required platform
            for(IUplink eventUplink: event.getEventStreaming().getUplinks()){
                platform = eventUplink.getStreamingServer().getMOSPlatform();

                for(IMembership m: new DAOFactory(em).getMembershipDAO().findByEventMasterAndPlatform(master, platform)){
                    membership = m;
                }
            }

            if (platform == null) {
                throw new AssignmentException("No MOSPlatform is streaming the Event");
            }

            if(membership != null){
                discount = membership.getDiscount();
            }

            Instant start = event.getEventStreaming().getStart().toInstant();
            Instant end = event.getEventStreaming().getEnd().toInstant();

            // calculate the setup price based on how many events were already created
            BigDecimal setupCosts = priceManagementBean.getPrice(paidCount);

            // calculate what the event itself costs based on DURATION x COSTS_PER_MINUTE
            BigDecimal streamingCosts = platform.getCostsPerStreamingMinute();
            streamingCosts = streamingCosts.multiply(new BigDecimal(Duration.between(start, end).toMinutes()));

            // calculate what this event costs: (c_se + c_st) * (1 - discount)
            BigDecimal eventCosts = new BigDecimal(1 - discount);
            eventCosts = eventCosts.multiply(streamingCosts.add(setupCosts));
            eventCosts = eventCosts.setScale(2, BigDecimal.ROUND_HALF_UP);

            price = price.add(eventCosts);

            // create the bill per event
            BillDTO.BillPerEventMaster bpem = bill.new BillPerEventMaster();
            bpem.setEventId(event.getId());
            bpem.setSetupCosts(setupCosts.setScale(2, BigDecimal.ROUND_HALF_UP));
            bpem.setStreamingCosts(streamingCosts.setScale(2, BigDecimal.ROUND_HALF_UP));
            bpem.setEventCosts(eventCosts.setScale(2, BigDecimal.ROUND_HALF_UP));
            bpem.setNumberOfUplinks(event.getEventStreaming().getUplinks().size());
            billPerEventMaster.add(bpem);

            event.setPaid(true);
            em.merge(event);

            paidCount++;
        }

        em.flush();
        bill.setTotalPrice(price.setScale(2, BigDecimal.ROUND_HALF_UP));
        bill.setBills(billPerEventMaster);

        return new AsyncResult<BillDTO>(bill);
    }

    @Override
    public List<AuditLogDTO> getAuditLogs() {
        LOGGER.info("Fetching AuditLogs");
        List<AuditLogDTO> auditLogs = new ArrayList<>();

        for(IAuditLog log: this.auditLogDAO.findAll()){
            List<AuditParameterDTO> params = new ArrayList<>();

            for(IAuditParameter param: log.getParameters()){
                AuditParameterDTO p = new AuditParameterDTO(param.getId(), param.getParameterIndex(), param.getType(), param.getValue());
                params.add(p);
            }

            AuditLogDTO auditLogDTO = new AuditLogDTO(log.getId(), log.getMethod(), log.getResult(), log.getInvocationTime(), params);
            auditLogs.add(auditLogDTO);
        }

        return auditLogs;
    }

    @Override
    public void clearPriceCache() {
        priceManagementBean.clearCache();
    }
}
