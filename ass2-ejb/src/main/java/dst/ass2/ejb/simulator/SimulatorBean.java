package dst.ass2.ejb.simulator;


import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.dao.IEventDAO;
import dst.ass1.jpa.model.EventStatus;
import dst.ass1.jpa.model.IEvent;
import dst.ass1.jpa.model.IEventStreaming;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Singleton
public class SimulatorBean {

    @PersistenceContext
    private EntityManager em;

    private DAOFactory daoFactory;
    private IEventDAO eventDAO;

    private Logger LOGGER = Logger.getLogger(SimulatorBean.class);

    @PostConstruct
    public void setUp(){
        this.daoFactory = new DAOFactory(em);
        this.eventDAO = this.daoFactory.getEventDAO();
    }

    @Schedule(second = "*/5", minute = "*", hour = "*")
    public void simulate() {

        LOGGER.info("Simulating finishing the Events");
        List<IEvent> events = this.eventDAO.findAll();

        for(IEvent event: events){
            IEventStreaming streaming = event.getEventStreaming();

            if(streaming.getEnd() == null){
                if(Instant.now().isAfter(streaming.getStart().toInstant())){
                    streaming.setEnd(Date.from(Instant.now()));
                    streaming.setStatus(EventStatus.FINISHED);
                }
            }

            em.merge(streaming);
            em.merge(event);
        }

        em.flush();
    }

}
