package dst.ass2.ejb.management;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.model.ModelFactory;
import dst.ass2.ejb.dao.IPriceDAO;
import dst.ass2.ejb.dao.impl.PriceDAO;
import dst.ass2.ejb.management.interfaces.IPriceManagementBean;
import dst.ass2.ejb.model.IPrice;
import dst.ass2.ejb.model.impl.Price;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Startup
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class PriceManagementBean implements IPriceManagementBean {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(PriceManagementBean.class);

    @PersistenceContext
    private EntityManager em;

    private ModelFactory modelFactory;
    private HashMap<Integer, BigDecimal> prices;
    private DAOFactory daoFactory;
    private IPriceDAO priceDAO;

    @PostConstruct
    private void setUp(){
        LOGGER.info("Setting up new PriceManagementBean");
        this.modelFactory = new ModelFactory();
        this.prices = new HashMap<>();
        this.daoFactory = new DAOFactory(em);
        this.priceDAO = this.daoFactory.getPriceDAO();

        for(IPrice price: this.priceDAO.findAll()){
            this.prices.put(price.getNrOfHistoricalEvents(), price.getPrice());
        }
    }

    @Override
    @Lock(LockType.READ)
    public BigDecimal getPrice(Integer nrOfHistoricalEvents) {
        BigDecimal price = null;

        Integer events = null;
        List<Integer> keys = new ArrayList<>(this.prices.keySet());
        Collections.sort(keys);

        for(Integer event: keys){
            if(event <= nrOfHistoricalEvents){
                events = event;
            }else{
                break;
            }
        }

        if (events != null) {
            price = this.prices.get(events);
        }


        BigDecimal ret = ((price == null) ? new BigDecimal(0.0) : price);
        LOGGER.info("Price for " + nrOfHistoricalEvents + " events: " + ret.toString());
        return ret;
    }

    @Override
    @Lock(LockType.WRITE)
    public void setPrice(Integer nrOfHistoricalEvents, BigDecimal price) {

        IPrice priceDto = this.modelFactory.createPrice();
        priceDto.setNrOfHistoricalEvents(nrOfHistoricalEvents);
        priceDto.setPrice(price);

        if (this.prices.containsKey(nrOfHistoricalEvents)) {
            if(this.prices.get(nrOfHistoricalEvents).compareTo(price) != 0){
                LOGGER.info("Updating price (" + price.toString() + ") for " + nrOfHistoricalEvents + " events in DB");

                // the price was already there but got updated -> update in DB, too!
                this.em.merge(priceDto);
            }
        }else{
            LOGGER.info("Adding new price (" + price.toString() + ") for " + nrOfHistoricalEvents + " events in DB");

            // the price was new -> update in DB!
            this.em.persist(priceDto);
        }

        this.prices.put(nrOfHistoricalEvents, price);
        this.em.flush();
    }


    @Override
    @Lock(LockType.WRITE)
    public void clearCache() {
        this.prices.clear();
    }
}
