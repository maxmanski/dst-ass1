package dst.ass2.ejb.interceptor;

import dst.ass1.jpa.model.ModelFactory;
import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.model.IAuditParameter;
import dst.ass2.ejb.model.impl.AuditParameter;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class AuditInterceptor {

    @PersistenceContext
    EntityManager em;

    private ModelFactory modelFactory;

    public AuditInterceptor(){
        this.modelFactory = new ModelFactory();
    }

    @AroundInvoke
    public Object invoked(InvocationContext ctx) throws Exception{
        IAuditLog auditLog = this.modelFactory.createAuditLog();
        List<IAuditParameter> params = new ArrayList<>();

        int i = 0;
        for(Object param: ctx.getParameters()){
            IAuditParameter auditParameter = this.modelFactory.createAuditParameter();
            auditParameter.setAuditLog(auditLog);
            auditParameter.setParameterIndex(i);
            auditParameter.setType(ctx.getMethod().getParameterTypes()[i].getTypeName());

            if(param != null){
                auditParameter.setValue(param.toString());
            }else{
                auditParameter.setValue(null);
            }

            params.add(auditParameter);
            i++;
        }

        auditLog.setInvocationTime(Date.from(Instant.now()));
        auditLog.setMethod(ctx.getMethod().getName());
        auditLog.setParameters(params);

        Object res = null;
        Exception occurredException = null;

        // proceed with the method invocation and wait for the result
        try {
            res = ctx.proceed();
        } catch(Exception ex){
            res = ex;
            occurredException = ex;
        }

        if(res != null){
            auditLog.setResult(res.toString());
        }else{
            auditLog.setResult(null);
        }

        // persist the Audit Log
        em.persist(auditLog);
        em.flush();

        // act like the method did (either return the value or throw an exception)
        if (occurredException != null) {
            throw occurredException;
        }else{
            return res;
        }
    }

}
