package dst.ass2.ejb;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import dst.ass1.jpa.dao.DAOFactory;
import dst.ass2.Ass2EjbTestBase;
import dst.ass2.ejb.model.IPrice;

public class GeneralManagementBeanTest extends Ass2EjbTestBase {

    private DAOFactory daoFactory;

    @Before
    public void setUp() {
        testingBean.insertTestData();
        managementBean.clearPriceCache();
        daoFactory = new DAOFactory(em);
    }

    @Test
    public void testAddPrice() {
        BigDecimal cost1 = new BigDecimal(50);
        BigDecimal cost2 = new BigDecimal(45);

        managementBean.addPrice(0, cost1);
        managementBean.addPrice(1, cost2);

        List<IPrice> prices = daoFactory.getPriceDAO().findAll();

        assertEquals("Expected two price objects", 2, prices.size());

        prices.sort(Comparator.comparing(IPrice::getPrice));

        IPrice p1 = prices.get(1); // cost 1
        IPrice p2 = prices.get(0); // cost 2

        assertEquals(0, p1.getNrOfHistoricalEvents().intValue());
        assertEquals(1, p2.getNrOfHistoricalEvents().intValue());
    }

    @Test
    public void testAddPrice_Cache() throws Exception {
        managementBean.addPrice(0, new BigDecimal(50));

        List<IPrice> prices = daoFactory.getPriceDAO().findAll();

        assertEquals("Expected one price object", 1, prices.size());

        userTransaction.begin();
        for (IPrice price : prices) {
            em.remove(em.merge(price));
        }
        userTransaction.commit();

        // add same price a second time => should be cached and therefore
        // not stored in the db
        managementBean.addPrice(0, new BigDecimal(50));

        prices = daoFactory.getPriceDAO().findAll();

        assertEquals(0, prices.size());

        // clear cache
        managementBean.clearPriceCache();

        // add same price to cleared cache => should be stored in the db
        managementBean.addPrice(0, new BigDecimal(50));
        prices = daoFactory.getPriceDAO().findAll();

        assertEquals(1, prices.size());
    }
}
