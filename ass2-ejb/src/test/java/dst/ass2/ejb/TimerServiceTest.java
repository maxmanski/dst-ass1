package dst.ass2.ejb;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import dst.ass1.jpa.dao.DAOFactory;
import dst.ass1.jpa.model.EventStatus;
import dst.ass1.jpa.model.IEventStreaming;
import dst.ass2.Ass2EjbTestBase;

public class TimerServiceTest extends Ass2EjbTestBase {

    private final long sleepingTime = TimeUnit.SECONDS.toMillis(30);

    private DAOFactory daoFactory;

    @Before
    public void setUp() {
        testingBean.insertTestData();
        daoFactory = new DAOFactory(em);
    }

    @Test
    public void testTimerService() throws Exception {
        Thread.sleep(sleepingTime);

        List<IEventStreaming> streamings = daoFactory.getEventStreamingDAO().findAll();
        assertEquals(4, streamings.stream().filter(s -> s.getStatus() == EventStatus.FINISHED).count());

    }

}
