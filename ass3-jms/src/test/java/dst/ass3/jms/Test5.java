package dst.ass3.jms;

import static dst.ass3.util.Utils.SHORT_WAIT;
import static dst.ass3.util.Utils.assure;
import static dst.ass3.util.Utils.log;
import static dst.ass3.util.Utils.logCheckpoint;
import static dst.ass3.util.Utils.logTimed;
import static dst.ass3.util.Utils.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dst.ass3.AbstractJMSTest;
import dst.ass3.dto.ClassifyEventRequest;
import dst.ass3.dto.EventInfoDTO;
import dst.ass3.dto.StreamEventRequest;
import dst.ass3.jms.scheduler.IScheduler.ISchedulerListener;
import dst.ass3.jms.streamingserver.IStreamingServer.IStreamingServerListener;
import dst.ass3.jms.uplink.IUplink.IUplinkListener;
import dst.ass3.model.EventType;
import dst.ass3.model.LifecycleState;

/**
 * This test performs the following tasks: ASSIGN, CLASSIFY as PRESENTATION and STREAM it.
 *
 * <pre>
 * Timing diagram
 *
 *    0  1  2  3  4
 *    |--|--|--|--|-->
 * E1 *******
 * I1          *
 *    ^     ^  ^  ^
 *    CP1   CP2+3 CP4
 *
 * **: Running
 *
 * E1: takes 2sec to finish
 * I1: InfoRequest for EventInfo 1
 *
 * CP1: Check-Point 1 - Assign Event 1
 * CP2: Check-Point 2 - Wait till event 1 has finished
 * CP3: Check-Point 3 - Event1 should have finished so send INFO Request
 * CP4: Check-Point 4 - Info Request completed [Scheduler = ASSIGN, STREAMED, INFO events]
 * </pre>
 */
public class Test5 extends AbstractJMSTest {

    private AtomicInteger streamingServerEvent = new AtomicInteger(0);
    private AtomicInteger schedulerEvent = new AtomicInteger(0);
    private AtomicInteger streamed = new AtomicInteger(0);

    private long eventId1 = 50;
    private long eventInfoId1 = -1;

    private String classifiedBy = null;
    private long startTime;

    private Semaphore sem;
    private Semaphore semUplink;

    private final int STREAMING_TIME = 2000;

    @Before
    public void init() {
        super.init();
    }

    @Test
    public void test_AssignTypeAndStream2() {
        sem = new Semaphore(0);
        semUplink = new Semaphore(0);
        ss1.start();
        scheduler.start();
        u2.start();

        IStreamingServerListener streamingServerListener = new IStreamingServerListener() {
            @Override
            public ClassifyEventResponse decideEvent(
                    ClassifyEventRequest request, String streamingServerName) {
                logTimed("** streamingServer " + streamingServerName + " uplink: "
                        + request, startTime);

                assertNotNull("eventWrapper.eventId = null",
                        request.getEventId());
                assertNotNull("eventWrapper.id = null", request.getId());

                assertEquals("eventId wrong", eventId1, request.getEventId()
                        .longValue());

                log("SETTING ID " + request.getId());
                eventInfoId1 = request.getId();
                classifiedBy = streamingServerName;

                streamingServerEvent.incrementAndGet();
                sem.release();

                return new ClassifyEventResponse(
                        DecisionType.ACCEPT, EventType.PRESENTATION);
            }
        };

        IUplinkListener uplinkListener = new IUplinkListener() {
            @Override
            public void waitForStreamed(StreamEventRequest event,
                    String uplinkName, EventType acceptedType,
                    String streamingServerName) {
                logTimed("** uplink " + uplinkName + " eventWrapper: "
                        + event, startTime);

                assertEquals("streamingServerName", classifiedBy, streamingServerName);
                assertEquals("uplinkName", UPLINK2_NAME, uplinkName);
                assertEquals("eventtype", EventType.PRESENTATION,
                        acceptedType);

                sleep(STREAMING_TIME); // simulate streaming

                streamed.incrementAndGet();

                semUplink.release();
                assertEquals("uplink listener - too many events", 1,
                        streamed.get());
            }
        };

        ISchedulerListener schedulerListener = new ISchedulerListener() {
            @Override
            public void notify(InfoType type, EventInfoDTO eventInfo) {
                logTimed("** scheduler: type=" + type + " eventWrapper: "
                        + eventInfo, startTime);
                sleep(SHORT_WAIT); // wait short time for updated eventId

                assertEquals("eventId in server response DTO wrong " + eventId1,
                        eventId1, eventInfo.getEventId().longValue());

                assertEquals("eventWrapperId in server response DTO wrong"
                        + schedulerEvent, eventInfoId1, eventInfo.getId()
                        .longValue());

                switch (schedulerEvent.get()) {
                    case 0:
                        // ASSIGN
                        assertEquals("1st event of wrong type", InfoType.CREATED,
                                type);
                        assertEquals("1st event != ASSIGNED",
                                LifecycleState.ASSIGNED, eventInfo.getState());
                        assertEquals("1st event Event type != UNCLASSIFIED",
                                EventType.UNCLASSIFIED, eventInfo.getType());
                        break;
                    case 1:
                        // STREAMED
                        assertEquals("2nd event of wrong type", InfoType.STREAMED,
                                type);
                        assertEquals("2nd event != ASSIGNED",
                                LifecycleState.STREAMED, eventInfo.getState());
                        assertEquals("2nd event Event type ", EventType.PRESENTATION,
                                eventInfo.getType());
                        assertNotNull("2nd classified by == null " + classifiedBy,
                                eventInfo.getClassifiedBy());
                        assertEquals("2nd classified by != " + classifiedBy, classifiedBy,
                                eventInfo.getClassifiedBy());
                        break;
                    case 2:
                        // INFO
                        assertEquals("3rd event of wrong type", InfoType.INFO, type);
                        assertEquals("3rd event != ASSIGNED",
                                LifecycleState.STREAMED, eventInfo.getState());
                        assertEquals("3rd event Event type ", EventType.PRESENTATION,
                                eventInfo.getType());
                        assertNotNull("3rd classified by == null" + classifiedBy,
                                eventInfo.getClassifiedBy());
                        assertEquals("3rd classified by != " + classifiedBy, classifiedBy,
                                eventInfo.getClassifiedBy());
                        break;
                    default:
                        fail("only 3 events expected");
                        break;
                }

                schedulerEvent.incrementAndGet();
                sem.release();
            }
        };

        sleep(SHORT_WAIT); // Wait for old messages being discarded.
        startTime = new Date().getTime();

        // ---------------- CP1 ------------------------
        logCheckpoint(1, startTime);

        ss1.setStreamingServerListener(streamingServerListener);
        scheduler.setSchedulerListener(schedulerListener);
        u2.setUplinkListener(uplinkListener);

        log("Assigning " + eventId1 + "...");
        scheduler.assign(eventId1);

        assure(sem,
                2,
                "did not get 2 events (Scheduler: create; StreamingServer: classify) in time",
                DEFAULT_CHECK_TIMEOUT);
        // ---------------- CP2 ------------------------
        logCheckpoint(2, startTime);

        assertEquals("wrong count of streamingServer events ", 1,
                streamingServerEvent.get());
        assertEquals("wrong count of scheduler events ", 1,
                schedulerEvent.get());

        assure(semUplink,
                1,
                "did not get 1 event (Uplink: finished streaming) in time",
                DEFAULT_CHECK_TIMEOUT + STREAMING_TIME / 1000);
        assure(sem, 1, "did not get 1 event (Scheduler: streamed) in time",
                DEFAULT_CHECK_TIMEOUT);

        // ---------------- CP3 ------------------------
        logCheckpoint(3, startTime);
        assertEquals("wrong count of uplink events", 1, streamed.get());
        assertEquals("wrong count of scheduler events ", 2,
                schedulerEvent.get());
        assertEquals("wrong count of streamingServer events ", 1,
                streamingServerEvent.get());

        log("Executing info " + eventInfoId1 + "...");
        scheduler.info(eventInfoId1);

        assure(sem, 1, "did not get 1 event (Scheduler: info) in time",
                DEFAULT_CHECK_TIMEOUT);

        // ---------------- CP4 ------------------------
        logCheckpoint(4, startTime);

        assertEquals("wrong count of scheduler events ", 3,
                schedulerEvent.get());
        assertEquals("wrong count of streamingServer events ", 1,
                streamingServerEvent.get());
    }

    @After
    public void shutdown() {
        // disable all listeners
        ss1.setStreamingServerListener(null);
        scheduler.setSchedulerListener(null);

        log("shutting down " + AbstractJMSTest.UPLINK2_NAME + "...");
        u2.stop();
        log("shutting down " + AbstractJMSTest.STREAMINGSERVER1_NAME + "...");
        ss1.stop();
        log("shutting down Scheduler...");
        scheduler.stop();

        super.shutdown();
    }

}
