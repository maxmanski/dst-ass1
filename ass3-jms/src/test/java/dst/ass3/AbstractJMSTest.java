package dst.ass3;

import java.util.Properties;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.openejb.junit.OpenEjbRunner;
import org.junit.runner.RunWith;

import dst.ass3.jms.JMSFactory;
import dst.ass3.jms.scheduler.IScheduler;
import dst.ass3.jms.streamingserver.IStreamingServer;
import dst.ass3.jms.uplink.IUplink;
import dst.ass3.model.EventType;


@RunWith(OpenEjbRunner.class)
public abstract class AbstractJMSTest {

    protected static final String UPLINK1_NAME = "u1";
    protected static final String UPLINK2_NAME = "u2";
    protected static final String UPLINK3_NAME = "u3";
    protected static final String UPLINK4_NAME = "u4";

    protected static final String STREAMINGSERVER1_NAME = "ss1";
    protected static final String STREAMINGSERVER2_NAME = "ss2";

    protected IScheduler scheduler;
    protected IStreamingServer ss2;
    protected IStreamingServer ss1;
    protected IUplink u4;
    protected IUplink u3;
    protected IUplink u2;
    protected IUplink u1;

    protected EJBContainer ejbContainer;
    protected Context ctx;

    /**
     * Time to wait for semaphores to reach required value
     */
    public int DEFAULT_CHECK_TIMEOUT = 5;

    public void init() {
        this.u1 = JMSFactory.createUplink(UPLINK1_NAME, STREAMINGSERVER1_NAME, EventType.INTERACTIVE);
        this.u2 = JMSFactory.createUplink(UPLINK2_NAME, STREAMINGSERVER1_NAME, EventType.PRESENTATION);
        this.u3 = JMSFactory.createUplink(UPLINK3_NAME, STREAMINGSERVER2_NAME, EventType.INTERACTIVE);
        this.u4 = JMSFactory.createUplink(UPLINK4_NAME, STREAMINGSERVER2_NAME, EventType.PRESENTATION);

        this.ss1 = JMSFactory.createStreamingServer(STREAMINGSERVER1_NAME);
        this.ss2 = JMSFactory.createStreamingServer(STREAMINGSERVER2_NAME);

        this.scheduler = JMSFactory.createScheduler();

        try {
            Properties p = getContainerProperties();

            ejbContainer = EJBContainer.createEJBContainer(p);

            ctx = ejbContainer.getContext();

            ctx.bind("inject", scheduler);

            ctx.bind("inject", ss1);
            ctx.bind("inject", ss2);

            ctx.bind("inject", u1);
            ctx.bind("inject", u2);
            ctx.bind("inject", u3);
            ctx.bind("inject", u4);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        System.out.println("******************************"
                + this.getClass().getCanonicalName()
                + "******************************");
    }

    public void shutdown() {
        if (ctx != null) {
            try {
                ctx.close();
            } catch (NamingException e) {
            }
        }

        if (ejbContainer != null) {
            ejbContainer.close();
        }
    }

    public static Properties getContainerProperties() {
        Properties properties = new Properties();

        // TomEE settings
        // System.setProperty("tomee.jpa.factory.lazy", "true");
        properties.put("tomee.jpa.cdi", "false"); // http://tomee-openejb.979440.n4.nabble.com/CDI-issues-tomee-7-0-2-td4680584.html
        properties.put("openejb.validation.output.level", "VERBOSE");
        properties.put("openejb.jpa.auto-scan", "true");
        properties.put("openejb.embedded.initialcontext.close", "DESTROY");
        properties.put("openejb.deployments.classpath.include", ".*(ass3-shared|ass3-jms).*");
        properties.put("org.apache.activemq.SERIALIZABLE_PACKAGES", "*"); // http://activemq.apache.org/objectmessage.html
        properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.core.OpenEJBInitialContextFactory");

        // hibernate hacks
        properties.put("hibernate.enable_lazy_load_no_trans", "true");

        return properties;
    }
}

