package dst.ass3.jms;

import dst.ass3.dto.*;
import dst.ass3.jms.scheduler.IScheduler;
import dst.ass3.model.EventInfo;
import dst.ass3.model.EventType;
import dst.ass3.model.IEventInfo;
import dst.ass3.model.LifecycleState;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.jms.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Max on 05.06.2017.
 */
@MessageDriven(mappedName = "centralQueue")
public class CentralServer implements MessageListener {

    @PersistenceContext
    private EntityManager em;

    @Resource(mappedName = "streamingServerQueue")
    private Queue streamingServerQueue;
    @Resource(mappedName = "schedulerQueue")
    private Queue schedulerQueue;
    @Resource(mappedName = "uplinkTopic")
    private Topic uplinkTopic;

    @Resource
    private ConnectionFactory connectionFactory;
    private Connection connection;
    private Session session;

    @PostConstruct
    public void setUp() {
        try {
            System.out.println("CENTRAL SERVER -- Setting up");
            this.connection = this.connectionFactory.createConnection();
            this.session = this.connection.createSession();
        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }

    @PreDestroy
    public void tearDown() {
        try {
            System.out.println("CENTRAL SERVER -- Tearing down");
            this.session.close();
            this.connection.close();
        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onMessage(Message message) {
        try {
            System.out.println("CENTRAL SERVER -- Received Message");
            if (message instanceof ObjectMessage) {
                Object containedObject = ((ObjectMessage) message).getObject();

                // decide what to do, based on the content of the message
                if (containedObject instanceof AssignEventRequest) {
                    AssignEventRequest req = (AssignEventRequest) containedObject;
                    this.handleAssignEvent(req);

                } else if (containedObject instanceof ClassifyEventRequest) {
                    ClassifyEventRequest req = (ClassifyEventRequest) containedObject;
                    this.handleClassifyEvent(req);

                } else if (containedObject instanceof EventInfoRequest) {
                    EventInfoRequest req = (EventInfoRequest) containedObject;
                    this.handleEventInfo(req);

                } else if (containedObject instanceof StreamEventRequest) {
                    StreamEventRequest req = (StreamEventRequest) containedObject;
                    this.handleStreamEvent(req);
                }
            }
        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }

    private void handleAssignEvent(AssignEventRequest request) throws JMSException{
        System.out.println("CENTRAL SERVER -- Handling AssignRequest");
        IEventInfo info = new EventInfo();
        info.setEventId(request.getEventId());
        info.setState(LifecycleState.ASSIGNED);
        info.setType(EventType.UNCLASSIFIED);
        em.persist(info);
        em.flush();

        ObjectMessage message = this.session.createObjectMessage(new EventInfoDTO(info));
        message.setStringProperty("type", IScheduler.ISchedulerListener.InfoType.CREATED.toString());
        this.session.createProducer(this.streamingServerQueue).send(message);
        this.session.createProducer(this.schedulerQueue).send(message);
    }

    private void handleClassifyEvent(ClassifyEventRequest request) throws JMSException{
        System.out.println("CENTRAL SERVER -- Handling ClassifyEventRequest");
        IEventInfo info = this.getEventInfoByEventId(request.getEventId());
        info.setType(request.getType());
        info.setState(request.getState());
        info.setClassifiedBy(request.getClassifiedBy());
        EventInfoDTO dto = new EventInfoDTO(info);

        ObjectMessage message;
        switch (info.getState()){
            case READY_FOR_STREAMING:
                System.out.println("CENTRAL SERVER -- EventInfo: Ready for streaming");
                // if we're ready for streaming, let's forward the EventInfoDTO to the Uplinks
                message = this.session.createObjectMessage(dto);
                message.setStringProperty("streamingServer", dto.getClassifiedBy());
                message.setStringProperty("type", dto.getType().toString());
                this.session.createProducer(uplinkTopic).send(message);
                break;

            case STREAMING_NOT_POSSIBLE:
                System.out.println("CENTRAL SERVER -- EventInfo: Streaming not possible");
                // otherwise, let's inform the Scheduler that something's not possible
                message = this.session.createObjectMessage(dto);
                message.setStringProperty("type", IScheduler.ISchedulerListener.InfoType.DENIED.toString());
                this.session.createProducer(schedulerQueue).send(message);
                break;

            default:
                break;
        }
    }

    private void handleEventInfo(EventInfoRequest request) throws JMSException{
        System.out.println("CENTRAL SERVER -- Handling EventInfoRequest");
        IEventInfo info = this.getEventInfoById(request.getEventInfoId());
        EventInfoDTO dto = new EventInfoDTO(info);

        ObjectMessage message = this.session.createObjectMessage(dto);
        message.setStringProperty("type", IScheduler.ISchedulerListener.InfoType.INFO.toString());
        this.session.createProducer(this.schedulerQueue).send(message);

    }

    private void handleStreamEvent(StreamEventRequest request) throws JMSException{
        System.out.println("CENTRAL SERVER -- Handling StreamEventRequest");
        IEventInfo info = this.getEventInfoByEventId(request.getEventId());
        info.setState(request.getState());
        info.setClassifiedBy(request.getClassifiedBy());
        EventInfoDTO dto = new EventInfoDTO(info);

        ObjectMessage message = this.session.createObjectMessage(dto);
        message.setStringProperty("type", IScheduler.ISchedulerListener.InfoType.STREAMED.toString());
        this.session.createProducer(this.schedulerQueue).send(message);

    }

    private List<IEventInfo> getEventInfos(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventInfo> cq = cb.createQuery(IEventInfo.class);
        Root<EventInfo> event = cq.from(EventInfo.class);
        cq.select(event);
        return em.createQuery(cq).getResultList();

    }

    private IEventInfo getEventInfoById(long id){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventInfo> cq = cb.createQuery(IEventInfo.class);
        Root<EventInfo> event = cq.from(EventInfo.class);
        cq.select(event);
        cq.where(cb.equal(event.<Long>get("id"), id));
        return em.createQuery(cq).getSingleResult();
    }

    private IEventInfo getEventInfoByEventId(long id){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<IEventInfo> cq = cb.createQuery(IEventInfo.class);
        Root<EventInfo> event = cq.from(EventInfo.class);
        cq.select(event);
        cq.where(cb.equal(event.<Long>get("eventId"), id));
        return em.createQuery(cq).getSingleResult();
    }
}