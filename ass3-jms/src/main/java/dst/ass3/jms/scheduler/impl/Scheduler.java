package dst.ass3.jms.scheduler.impl;

import dst.ass3.dto.AssignEventRequest;
import dst.ass3.dto.EventInfoDTO;
import dst.ass3.dto.EventInfoRequest;
import dst.ass3.jms.scheduler.IScheduler;

import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.jms.*;

/**
 * Created by Max on 04.06.2017.
 */
@ManagedBean
public class Scheduler implements IScheduler {

    @Resource(mappedName = "centralQueue")
    private Queue centralQueue;
    @Resource(mappedName = "schedulerQueue")
    private Queue schedulerQueue;
    //@Resource(lookup = "java:comp/DefaultJMSConnectionFactory")
    @Resource
    private ConnectionFactory connectionFactory;

    private Connection connection;
    private Session session;
    private MessageConsumer messageConsumer;
    private ISchedulerListener schedulerListener;
    private SchedulerMessageListener messageListener;

    public Scheduler() {
        this.messageListener = new SchedulerMessageListener(this);
    }

    @Override
    public void start() {
        try {
            System.out.println("SCHEDULER -- Starting a Scheduler");
            this.connection = this.connectionFactory.createConnection();
            this.session = this.connection.createSession();
            this.messageConsumer = this.session.createConsumer(this.schedulerQueue);
            this.messageConsumer.setMessageListener(this.messageListener);
            this.connection.start();

        } catch (JMSException ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public void stop() {
        try {
            System.out.println("SCHEDULER -- Stopping the Scheduler");
            if(this.messageConsumer != null){
                this.messageConsumer.close();
            }
            if(this.session != null){
                this.session.close();
            }
            if (this.connection != null) {
                this.connection.close();
            }
            this.connection = null;
            this.messageConsumer = null;
            this.session = null;

        } catch (JMSException ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public void assign(long eventId) {
        try {
            System.out.println("SCHEDULER -- Assign Event #" + eventId);
            AssignEventRequest request = new AssignEventRequest(eventId);

            ObjectMessage message = this.session.createObjectMessage(request);
            this.session.createProducer(this.centralQueue).send(message);

        }catch (JMSException ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void info(long eventInfoId) {
        try {
            System.out.println("SCHEDULER -- Info EventInfo #" + eventInfoId);
            EventInfoRequest request = new EventInfoRequest(eventInfoId);

            ObjectMessage message = this.session.createObjectMessage(request);
            this.session.createProducer(this.centralQueue).send(message);

        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setSchedulerListener(ISchedulerListener listener) {
        this.schedulerListener = listener;
    }

    // inner class used as message listener
    private class SchedulerMessageListener implements MessageListener {

        private Scheduler scheduler;

        public SchedulerMessageListener(Scheduler scheduler) {
            this.scheduler = scheduler;
        }

        @Override
        public void onMessage(Message message) {
            try {
                System.out.println("SCHEDULER -- Received Message");
                if(message instanceof ObjectMessage){
                    Object containedObject = ((ObjectMessage)message).getObject();

                    if(containedObject instanceof EventInfoDTO){
                        EventInfoDTO info = (EventInfoDTO) containedObject;

                        ISchedulerListener.InfoType type = ISchedulerListener.InfoType.valueOf(message.getStringProperty("type"));
                        this.scheduler.schedulerListener.notify(type, info);
                    }
                }
            }catch (JMSException ex){
                ex.printStackTrace();
            }
        }
    }
}
