package dst.ass3.jms.streamingserver;

import dst.ass3.dto.ClassifyEventRequest;
import dst.ass3.model.EventType;

public interface IStreamingServer {
    /**
     * Starts a StreamingServer
     */
    void start();

    /**
     * Stops the StreamingServer and cleans all resources (e.g.: close session,
     * connection, etc.). Keep in mind that Listeners may be sleeping when stop
     * is requested. Be sure to interrupt them and discard the results they
     * might return, because the system is stopping already.
     */
    void stop();

    /**
     * Sets the Listener. Only one Listener should be in use at any
     * time. Be sure to handle cases where messages are received but no
     * listener is yet set (discard the message). The listeners may block
     * forever, so be sure to interrupt them in stop().
     *
     * @param listener
     */
    void setStreamingServerListener(IStreamingServerListener listener);

    interface IStreamingServerListener {
        enum DecisionType {
            ACCEPT, DENY
        }

        class ClassifyEventResponse {
            public DecisionType resp;
            public EventType type;

            public ClassifyEventResponse(DecisionType resp, EventType type) {
                this.resp = resp;
                this.type = type;
            }
        }

        /**
         * Process the given ClassifyEventRequest.
         *
         * @param request the classification request
         * @param streamingServerName name of the StreamingServer executing this listener
         * @return ACCEPT + Type | DENY
         */
        ClassifyEventResponse decideEvent(ClassifyEventRequest request, String streamingServerName);
    }
}
