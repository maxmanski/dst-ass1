package dst.ass3.jms.streamingserver.impl;

import dst.ass3.dto.ClassifyEventRequest;
import dst.ass3.dto.EventInfoDTO;
import dst.ass3.jms.streamingserver.IStreamingServer;
import dst.ass3.model.EventType;
import dst.ass3.model.LifecycleState;

import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.jms.*;

/**
 * Created by Max on 04.06.2017.
 */
@ManagedBean
public class StreamingServer implements IStreamingServer {

    // the injected destination resource
    @Resource(mappedName = "centralQueue")
    private Queue centralQueue;
    @Resource(mappedName = "streamingServerQueue")
    private Queue streamingServerQueue;

    // injected connectionFactory
    @Resource
    private ConnectionFactory connectionFactory;

    // connection: virtual connection with a JMS provider (e.g. open TCP/IP socket between client and server)
    // it is used to create sessions
    private Connection conn;
    // session: single-threaded context for producing/consuming messages
    private Session session;
    private MessageConsumer consumer;
    private String name;

    private IStreamingServerListener listener;
    private StreamingServerMessageListener messageListener;

    public StreamingServer(String name) {
        this.name = name;
        this.messageListener = new StreamingServerMessageListener(this);
    }

    @Override
    public void start() {
        try {
            System.out.println("STREAMING SERVER -- Start");
            this.conn = this.connectionFactory.createConnection();
            this.conn.setClientID(this.name);

            this.session = this.conn.createSession(false, Session.AUTO_ACKNOWLEDGE);

            this.consumer = this.session.createConsumer(this.streamingServerQueue);
            this.consumer.setMessageListener(this.messageListener);

            this.conn.start();
        } catch (JMSException ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    public void stop() {
        try {
            System.out.println("STREAMING SERVER -- Stop");
            if (this.consumer != null) {
                this.consumer.close();
            }
            if(this.session != null){
                this.session.close();
            }
            if (this.conn != null) {
                this.conn.close();
            }
            this.session = null;
            this.conn = null;
            this.consumer = null;
        } catch (JMSException ignored) {

        }
    }

    @Override
    public void setStreamingServerListener(IStreamingServerListener listener) {
        this.listener = listener;
    }

    // inner class for handling messages
    private class StreamingServerMessageListener implements MessageListener {

        private StreamingServer srv;

        public StreamingServerMessageListener(StreamingServer srv) {
            this.srv = srv;
        }

        @Override
        public void onMessage(Message message) {

            System.out.println("STREAMING SERVER -- Message received");
            // before processing the event, stop the connection
            try {
                srv.conn.stop();
            } catch (JMSException ignored) {

            }

            try {
                if (message instanceof ObjectMessage) {

                    Object containedObject = ((ObjectMessage) message).getObject();

                    if(containedObject instanceof EventInfoDTO){

                        EventInfoDTO info = (EventInfoDTO) containedObject;
                        ClassifyEventRequest classifyRequest = new ClassifyEventRequest(info);

                        // decide what's the matter with the event
                        IStreamingServerListener.ClassifyEventResponse classifyResponse = this.srv.listener.decideEvent(classifyRequest, this.srv.name);

                        classifyRequest.setClassifiedBy(this.srv.name);
                        EventType typeToSet = (classifyResponse.type != null) ? classifyResponse.type : EventType.UNCLASSIFIED;
                        classifyRequest.setType(typeToSet);

                        // check if good or not
                        switch(classifyResponse.resp){
                            case ACCEPT:
                                classifyRequest.setState(LifecycleState.READY_FOR_STREAMING);
                                break;
                            case DENY:
                                classifyRequest.setState(LifecycleState.STREAMING_NOT_POSSIBLE);
                                break;
                            default:
                                System.out.println("'How did I get here?' -- Desmond, the Moon Bear");
                                break;
                        }

                        ObjectMessage reply = this.srv.session.createObjectMessage(classifyRequest);
                        this.srv.session.createProducer(this.srv.centralQueue).send(reply);
                    }
                }

            } catch (JMSException ex) {
                ex.printStackTrace();
            }


            // after processing the event, get the connection going again
            try {
                srv.conn.start();
            } catch (JMSException ignored) {

            }
        }
    }
}
