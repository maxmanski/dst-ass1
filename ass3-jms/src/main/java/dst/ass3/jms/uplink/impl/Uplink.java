package dst.ass3.jms.uplink.impl;

import dst.ass3.dto.EventInfoDTO;
import dst.ass3.dto.StreamEventRequest;
import dst.ass3.jms.uplink.IUplink;
import dst.ass3.model.EventType;
import dst.ass3.model.LifecycleState;

import javax.annotation.ManagedBean;
import javax.annotation.Resource;
import javax.jms.*;

/**
 * Created by Max on 04.06.2017.
 */
@ManagedBean
public class Uplink implements IUplink {

    @Resource(mappedName = "centralQueue")
    private Queue centralQueue;
    @Resource(mappedName = "uplinkTopic")
    private Topic uplinkTopic;
    @Resource
    private ConnectionFactory connectionFactory;

    private Connection connection;
    private Session session;
    private MessageConsumer consumer;
    private String name;
    private String streamingServer;
    private EventType eventType;
    private IUplinkListener uplinkListener;
    private String clause;
    private String id;
    private UplinkMessageListener messageListener;

    public Uplink(String name, String streamingServer, EventType eventType) {
        this.name = name;
        this.streamingServer = streamingServer;
        this.eventType = eventType;
        this.clause = "streamingServer='" + streamingServer + "' AND type='" + eventType + "'";
        this.id = this.streamingServer + "." + this.name;
        this.messageListener = new UplinkMessageListener(this);
    }

    @Override
    public void start() {
        try {
            System.out.println("UPLINK -- Start");
            this.connection = this.connectionFactory.createConnection();
            // setting a client ID is required for the durable subscriber
            this.connection.setClientID(this.id);
            this.session = this.connection.createSession();
            this.consumer = this.session.createDurableSubscriber(this.uplinkTopic, this.id, this.clause, false);
            this.consumer.setMessageListener(this.messageListener);
            this.connection.start();
        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void stop() {
        try {
            System.out.println("UPLINK -- Stop");
            if (this.consumer != null) {
                this.consumer.close();
            }
            if (this.session != null) {
                this.session.close();
            }
            if (this.connection != null) {
                this.connection.close();
            }
            this.consumer = null;
            this.session = null;
            this.connection = null;
        } catch (JMSException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setUplinkListener(IUplinkListener listener) {
        this.uplinkListener = listener;
    }

    // inner class as MessageListener
    private class UplinkMessageListener implements MessageListener {
        private Uplink uplink;

        public UplinkMessageListener(Uplink uplink) {
            this.uplink = uplink;
        }

        @Override
        public void onMessage(Message message) {
            try {
                System.out.println("UPLINK -- Message received");
                if(message instanceof ObjectMessage){
                    Object containedObject = ((ObjectMessage)message).getObject();

                    if(containedObject instanceof EventInfoDTO){
                        EventInfoDTO info = (EventInfoDTO)containedObject;

                        // wait for the EventInfoThing to be streamed
                        StreamEventRequest request = new StreamEventRequest(info);
                        this.uplink.uplinkListener.waitForStreamed(request, this.uplink.name, info.getType(), info.getClassifiedBy());
                        request.setState(LifecycleState.STREAMED);

                        // and then, notify the server
                        ObjectMessage response = this.uplink.session.createObjectMessage(request);
                        this.uplink.session.createProducer(this.uplink.centralQueue).send(response);
                    }
                }
            }catch (JMSException ex){
                ex.printStackTrace();
            }
        }
    }
}
